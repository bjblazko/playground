package de.huepattl.playground.berkeleydb;

import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.SequenceConfig;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;
import de.huepattl.playground.berkeleydb.domain.model.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.util.ArrayList;
import java.util.List;

/**
 * The focus on this showcase if on how to retrieve and search the database.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @see SimplePersistenceTest
 * @see AdvancedPersistenceTest
 * @since 2013-04-29
 */
public class SearchMethodsTest {

    Logger LOG = LogManager.getLogger(SearchMethodsTest.class.getName());


    /**
     * Takes care of allocating a temporary folder and cleaning it up after test execution.
     * If you need to have a look at the data in this location, you need to set a breakpoint
     * and run as debug.
     */
    @Rule
    public TemporaryFolder dbLocation = new TemporaryFolder();

    /**
     * Our persistence store. This is a JE specific feature that not exists in the non-JE Berkeley DB.
     */
    private EntityStore entityStore;

    /**
     * Executed before test: bootstrap our database.
     *
     * @see com.sleepycat.je.EnvironmentConfig
     * @see com.sleepycat.je.Environment
     * @see com.sleepycat.persist.StoreConfig
     */
    @Before
    public void setupDatabase() {
        /*
        Create default database environment. You might configure stuff like locking behaviour,
        caching, recovery etc.
         */
        EnvironmentConfig environmentConfig = new EnvironmentConfig();
        environmentConfig.setAllowCreate(true); // create if not exists

        /*
        The database environment itself. You can, for example, use this instance to get
        statistics on current transactions etc.
         */
        Environment environment = new Environment(dbLocation.getRoot(), environmentConfig);
        LOG.info("Using DB location: " + dbLocation.getRoot().getAbsolutePath());

        /*
        Configure and create the entity-related store. It is thread-safe and handles entity handling.
         */
        StoreConfig storeConfig = new StoreConfig();
        storeConfig.setAllowCreate(true);
        this.entityStore = new EntityStore(environment, "testDatabase", storeConfig);
        SequenceConfig sequenceConfig = new SequenceConfig();
        sequenceConfig.setAllowCreate(true);
        sequenceConfig.setInitialValue(1);
        this.entityStore.setSequenceConfig("ID", sequenceConfig);
    }

    /**
     * Helper method that generates the given amount of example persons in memory.
     *
     * @param amount Number of {@link Person} entities to return.
     * @return List of persons.
     */
    private List<Person> sampleEntities(final int amount) {
        List<Person> result = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            Person p = new Person("Person-" + i, "Example");
            result.add(p);
        }
        return result;
    }

    /**
     * Shows how to retrieve a single entity when its primary key is known.
     */
    @Test
    public void getById() {
        PrimaryIndex<Long, Person> primaryIndex = entityStore.getPrimaryIndex(Long.class, Person.class);
        long firstPersonId = 0;

        /*
        Persist 15 person entities and remind the ID of the first one...
         */

        for (final Person person : this.sampleEntities(5)) {
            primaryIndex.put(person);
            if (firstPersonId == 0) {
                firstPersonId = person.getId();
            }
        }

        /*
        Retrieve person by ID (which we reminded above).
         */
        final Person p = primaryIndex.get(firstPersonId);
        LOG.info("Retrieved single entity by ID \'" + firstPersonId + "\': " + p);
    }

    /**
     * List all entities of a given type. This is quite simple as we store with primary indexes that are already bound
     * to a type.
     * // TODO: example how to list ALL entities in a store (so, not per index).
     */
    @Test
    public void listAllEntities() {
        final int AMOUNT = 50;
        PrimaryIndex<Long, Person> primaryIndex = entityStore.getPrimaryIndex(Long.class, Person.class);

        // Create 50 entities.
        for (final Person person : this.sampleEntities(AMOUNT)) {
            primaryIndex.put(person);
        }
        LOG.info("Persisted " + AMOUNT + " entities! Next: retrieve all and list...");

        // Retrieve ALL...
        final EntityCursor<Person> cursor = primaryIndex.entities();
        try {
            for (Person entity = cursor.first();
                 entity != null;
                 entity = cursor.next()) {
                LOG.info("-> Found entity: " + entity);
            }
        } finally {
            cursor.close();
        }
    }

}