package de.huepattl.playground.berkeleydb.domain.model;

import com.sleepycat.persist.model.Persistent;

/**
 * This is a simple embeddable object. Each {@link SimplePerson} entity has a reference to this class. All non-standard
 * objects (such as primitives, String, some collections etc.) can be used in an entity but require the {@link
 * Persistent} annotation in order to work or a proxy class in case you cannot change the source code of the object to
 * embed.
 * So, sub-objects to be persisted require the {@link Persistent} annotation in order to be embedded. Please have in
 * mind that this represents an exclusive relationship - the very instance (object, not class!) can only be
 * used/associated once. If you want to express relationships between independent entities, you have to do by referring
 * to their keys. In this case, you will most likely want to have separate model classes for persistence and domain.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @see SimplePerson#contacts
 * @since 2013-04-27
 */
@Persistent
public class Contacts {

    /*
    Some attributes like phone numbers, address...
     */
    private String phone;
    private String mobilePhone;
    private String email;
    private String street;
    private String city;
    private String zip;

    /**
     * {@link Persistent} requires a default constructor, scope may be another one (such as private).
     */
    public Contacts() {
        //
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Override
    public String toString() {
        return "Contacts{" +
                "phone='" + phone + '\'' +
                ", mobilePhone='" + mobilePhone + '\'' +
                ", email='" + email + '\'' +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", zip='" + zip + '\'' +
                '}';
    }
}
