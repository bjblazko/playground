package de.huepattl.playground.berkeleydb.domain.model;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

/**
 * Simple POJO with Berkeley DB (JE) annotations that will be used for persisting and retrieving this JavaBean. Such
 * bean is called entity then. An entity is annotated with {@link Entity} and represents an independent object with its
 * own identifier. Entities may contain sub-objects that exclusively belong to them, but these are embedded ones and
 * must bear the annotation {@link com.sleepycat.persist.model.Persistent}.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @see Contacts
 * @since 2013-04-24
 */
@Entity
public class SimplePerson {

    /**
     * Primary key used for looking up the object. We use a sequence which means that the database will take care of
     * generating the identifier value. The sequence name ("ID") has to be defined when setting up the entity store
     * using {@link com.sleepycat.persist.EntityStore#setSequenceConfig(String, com.sleepycat.je.SequenceConfig)}.
     */
    @PrimaryKey(sequence = "ID")
    protected long id;

    protected String lastName;
    protected String firstName;
    protected Contacts contacts;

    public SimplePerson() {

    }

    public SimplePerson(final String lastName, final String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public SimplePerson(final String lastName, final String firstName, final Contacts contacts) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.contacts = contacts;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Contacts getContacts() {
        return contacts;
    }

    public void setContacts(Contacts contacts) {
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        return "SimplePerson: " + this.lastName + ", " + this.firstName + " (" + id + ")";
    }
}
