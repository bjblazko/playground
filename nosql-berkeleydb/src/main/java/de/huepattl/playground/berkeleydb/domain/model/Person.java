package de.huepattl.playground.berkeleydb.domain.model;

import com.sleepycat.persist.model.DeleteAction;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * In contrast to {@link SimplePerson}, this entity model uses inheritance and can relate to other entities.
 * The abstract object we derive from ({@link AbstractPerson}) HAS TO BE annotated with {@link
 * com.sleepycat.persist.model.Persistent} (such as for embedded objects), simple POJOs are not enough.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @see Contacts
 * @see AbstractPerson
 * @see SimplePerson
 * @since 2013-04-29
 */
@Entity
public class Person extends AbstractPerson {

    private Date dateOfBirth;

    /**
     * Relation to other {@link Person} entities via their keys. By making it typed (relatedEntity), the JE can consult
     * the according primary index and issue consistency checks.
     */
    @SecondaryKey(
            relate = Relationship.ONE_TO_MANY, // have many friends
            relatedEntity = Person.class, // allows for consistency checks
            onRelatedEntityDelete = DeleteAction.NULLIFY) // will remove person if deleted
    private Set<Long> friends = new HashSet<>();

    public Person() {

    }

    public Person(final String lastName, final String firstName) {
        super.lastName = lastName;
        super.firstName = firstName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Set<Long> getFriends() {
        return friends;
    }

    public void setFriends(Set<Long> friends) {
        this.friends = friends;
    }

    public void addFriend(final long friendId) {
        if (friendId > 0) {
            this.friends.add(friendId);
        }
    }

    @Override
    public String toString() {
        return "Person: " + this.lastName + ", " + this.firstName + " (" + id + ")";
    }
}
