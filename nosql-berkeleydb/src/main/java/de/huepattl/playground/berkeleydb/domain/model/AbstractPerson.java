package de.huepattl.playground.berkeleydb.domain.model;

import com.sleepycat.persist.model.Persistent;
import com.sleepycat.persist.model.PrimaryKey;

/**
 * Abstract base class for a person entity. This is to demonstrate inheritance usage in Berkeley DB. Entities CANNOT
 * inherot from classes that are marked with {@link com.sleepycat.persist.model.Entity}. Instead, you have to use
 * (possibly abstract) classes which have to be annotated with {@link
 * com.sleepycat.persist.model.Persistent}.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @see de.huepattl.playground.berkeleydb.domain.model.Contacts
 * @see Person
 * @see SimplePerson
 * @since 2013-04-29
 */
@Persistent
public abstract class AbstractPerson {

    /**
     * Primary key used for looking up the object.
     */
    @PrimaryKey(sequence = "ID")
    protected long id;

    protected String lastName;
    protected String firstName;
    protected Contacts contacts;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Contacts getContacts() {
        return contacts;
    }

    public void setContacts(Contacts contacts) {
        this.contacts = contacts;
    }
}
