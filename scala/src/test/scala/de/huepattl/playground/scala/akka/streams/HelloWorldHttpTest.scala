package de.huepattl.playground.scala.akka.streams

import org.scalatest.{ Matchers, WordSpec }
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.http.scaladsl.server._
import Directives._

/**
  * Created by blazko on 26.08.16.
  */
class HelloWorldHttpTest extends WordSpec with Matchers with ScalatestRouteTest with RestService {

  "REST service with" should {

    "GET / returns 'Hello World'" in {
      Get() ~> route ~> check{
        responseAs[String] shouldEqual "Hello World"
      }
    }

    "GET /doh returns 'DOH!'" in {
      Get("/doh") ~> route ~> check{
        responseAs[String] shouldEqual "DOH!"
      }
    }

  }
}
