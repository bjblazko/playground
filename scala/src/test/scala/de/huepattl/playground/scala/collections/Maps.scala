package de.huepattl.playground.scala.collections

import org.scalatest.FunSuite

/**
  * Maps.
  *
  * @author blazko
  * @since 2016-06-11
  */
class Maps extends FunSuite {

  /**
    * Build a map in one line.
    */
  test("simple") {
    val map = Map(1 -> "Step I", 2 -> "Step II", 3 -> "Step III")
    println(map.mkString("\n"))
  }

  /**
    * Map access strategies.
    */
  test("access") {
    val map = Map(1 -> "Step I", 2 -> "Step II", 3 -> "Step III")

    // by index
    println(map(1))

    // iteration using a tuple
    map.foreach(entry => {
      println(entry._1 + ": " + entry._2)
    })
    // using for
    for ((key, value) <- map) {
      println(key + ": " + value)
    }
  }

  /**
    * Add to map.
    */
  test("add") {
    var map = Map(1 -> "Step I", 2 -> "Step II", 3 -> "Step III")
    val id1 = System.identityHashCode(map)

    map += (1 -> "Step IV")
    val id2 = System.identityHashCode(map)

    map += (1 -> "Step V")
    val id3 = System.identityHashCode(map)

    println(map.mkString(", "))
    println(map.getClass, id1, id2, id3) // IDs shouod not equal (=immutable map)
  }
}
