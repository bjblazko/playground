package de.huepattl.playground.scala.lang

/*
 * Import entire package, equals Java's wildcard (*)
 */

import java.nio._

/*
 * Import two classes.
 */

import org.scalatest.{FunSpec, FunSuite}

/**
  * Package in files...
  */
package inline {

  class ClassFromAnotherPackage {
    def publicMethod(): String = {
      "a"
    }

    private def privateMethod(): String = {
      "b"
    }


    private[lang] def privateExceptForLangPackageMembers(): String = {
      "c"
    }
  }

}

/**
  * Import of classes, packages, members etc. Just the important stuff is shown here...
  *
  * @author blazko
  * @since 2016-07-08
  */
class PackageImports extends FunSuite {

  test("Use import at any place, even within functions") {
    import java.time.LocalDateTime

    val now = LocalDateTime.now()
    assert(LocalDateTime.MIN.isBefore(now))
  }

  test("Import of class members") {
    import System._

    // out/err are System.out and System.err
    out.print("Hello STDOUT!")
    err.print("Hello STDERR!")
  }

  test("Aliasing/renaming") {
    import java.time.LocalDateTime

    val now = LocalDateTime.now()

    import java.time.{LocalDateTime => Cal}

    val max = Cal.MAX // actually is: LocalDateTime.MAX

    assert(max.isAfter(now))
  }

  test("Hiding") {
    // Imports all java.util.* except Date
    import java.util.{Date => _, _}

    val curr = Currency.getAvailableCurrencies

    /*
     * Next: java.util.Date not visible!
     */
    //val date = new Date()
  }

  test("Scopes") {
    import de.huepattl.playground.scala.lang.inline.ClassFromAnotherPackage

    val x = new ClassFromAnotherPackage

    /*
     * a() visible, because it is public
     */
    assert(x.publicMethod() == "a")

    /*
     * b() is not accessible, because it is private
     */
    //assert(x.b() == "b")

    /*
     * c() is accessible, although being private. Exception: for all members of package this
     * test method is in! How cool is that?!
     */
    assert(x.privateExceptForLangPackageMembers() == "c")
  }

}
