package de.huepattl.playground.scala.lang

/**
  * Created by tboewing on 11/06/16.
  */
class ApplicationMainTest extends App {

  override def main(args: Array[String]) = {
    println("Testing...")
    args.foreach(println)
  }
}
