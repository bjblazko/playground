package de.huepattl.playground.scala.lang

import java.io.{FileNotFoundException, FileReader, IOException}

import org.scalatest.{FlatSpec, FunSuite, Ignore}

/**
  * Examples of simple control flow stuff such as if, switch, loops etc. Please note that the more advanced
  * iteration/loop stuff is demonstrated in dedicated examples (see Arrays, Lists, Maps, Lists).
  *
  * @author blazko
  * @since 2016-06-23
  */
class ControlStructures extends FunSuite {

  private val list = List("One", "Two", "Three", "Four", "Five")
  private val foo = "bar"

  test("if: else") {
    if (foo == "bar") {
      println("found foo")
    } else {
      println("found something else")
    }
  }

  test("if: simple") {
    if (foo == "bar") println("found foo") else println("something else")
  }

  test("if: ternary") {
    val baz = if (foo == "bar") "found foo" else "something else"
    println(baz)
  }

  test("switch: action") {
    foo match {
      case "bar" => println("found foo")
      case "baz" => println("found baz")
      case _ => println("found something else")
    }
  }

  test("switch: result") {
    val baz = foo match {
      case "bar" => "found foo"
      case "baz" => "found baz"
      case _ => "found something else"
    }

    println(baz)
  }

  test("loop: foreach") {
    list.foreach(println)

    // More explicit:
    list.foreach(element => println(element))

    // Or
    list.foreach(element => {
      println("Value:")
      println(element)
    })
  }

  test("loop: for... in") {
    for (ele <- list) {
      println(ele)
    }
  }

  test("loop: for conditional") {
    for (ele <- list if ele.contains("v")) {
      println(ele)
    }
  }

  test("loop: for range") {
    for (i <- 1 to 3)
      println(list(i - 1))
  }

  test("loop: while") {
    var i = 0;
    while (i < list.size) {
      println(list(i))
      i += 1
    }
  }

  test("loop: do while") {
    var i = 0;
    do {
      println(list(i))
      i += 1
    } while (i < list.size)
  }

  test("exceptions: try... catch") {
    try {
      val f = new FileReader("foo.bar")
    } catch {
      case ex: FileNotFoundException => println("Sorry, file not found")
      case ex: IOException => println("Doh! Trouble!")
    }
  }

  /**
    * Ignoring test. There is no 'try with resources' like in Java and the below would work for later exceptions
    * in file processing but not when opening. Would have to use a var instead of val? ;(
    * Seems that recommended way is to use "Loan Pattern" (not yet covered here).
    */
  ignore("exceptions: try... finally") {
    val f = new FileReader("foo.bar")
    try {
      // use it...
    } finally {
      println("closing file reader")
      f.close()
    }
  }

  test("exceptions: try... catch... finally") {
    try {
      val f = new FileReader("foo.bar")
    } catch {
      case ex: FileNotFoundException => println("Sorry, file not found")
      case ex: IOException => println("Doh! Trouble!")
    } finally {
      println("Always execute")
    }
  }

}
