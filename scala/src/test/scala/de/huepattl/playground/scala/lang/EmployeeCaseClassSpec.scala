package de.huepattl.playground.scala.lang

import java.time.LocalDate

import collection.mutable.Stack
import org.scalatest._
import org.scalatest.GivenWhenThen

/**
  * Created by tboewing on 21/06/16.
  */
class EmployeeCaseClassSpec extends FeatureSpec with GivenWhenThen {

  feature("Construction of case class Employee") {

    info("As a programmer")
    info("I want to be able to create an instance of case class Employee")

    scenario("using anonymous arguments") {

      Given("arguments first and last name, date of birth")
      val fname = "John"
      val lname = "Doe"
      val dateOfBirth = LocalDate.of(2001, 4, 19)

      When("called anonymously in correct order (by type)")
      val employee = new Employee(fname, lname, dateOfBirth)

      Then("the instance is not null")
      assert(employee != null)

      And("fields are assigned correctly")
      assert(employee.lastName == lname)
      assert(employee.firstName == fname)
      assert(employee.dateOfBirth == dateOfBirth)
    }

    scenario("construction using named arguments") {

      Given("arguments first and last name, date of birth")
      val fname = "John"
      val lname = "Doe"
      val dateOfBirth = LocalDate.of(2001, 4, 19)

      When("called with named arguments first and last name, date of birth")
      val employee = new Employee(firstName = fname, lastName = lname, dateOfBirth = dateOfBirth)

      Then("the instance is not null")
      assert(employee != null)

      And("fields are assigned correctly")
      assert(employee.lastName == lname)
      assert(employee.firstName == fname)
      assert(employee.dateOfBirth == dateOfBirth)
    }

    scenario("construction using named arguments in different order") {

      Given("arguments first and last name, date of birth")
      val fname = "John"
      val lname = "Doe"
      val dateOfBirth = LocalDate.of(2001, 4, 19)

      When("called with named arguments in random order")
      val employee = new Employee(firstName = fname, dateOfBirth = dateOfBirth, lastName = lname)

      Then("the instance is not null")
      assert(employee != null)

      And("fields are assigned correctly")
      assert(employee.lastName == lname)
      assert(employee.firstName == fname)
      assert(employee.dateOfBirth == dateOfBirth)
    }

    scenario("construction using defaults (optional arguments)") {
      Given("arguments first name and last name only")
      val fname = "John"
      val lname = "Doe"

      When("called only with first nad last name")
      val employee = new Employee(firstName = fname, lastName = lname)

      Then("the instance is not null")
      assert(employee != null)

      And("date of birth is not empty")
      assert(employee.dateOfBirth != null)

      And("date of birth has a default value of Dec 31st, 1980")
      assert(employee.dateOfBirth == LocalDate.of(1980, 12, 31))

      And("remaining fields are assigned correctly")
      assert(employee.lastName == lname)
      assert(employee.firstName == fname)
    }
  }
}
