package de.huepattl.playground.scala.lang.traits

import org.scalatest.{FeatureSpec, GivenWhenThen}

import scala.collection.mutable.ArrayBuffer

/**
  * Shows how to use traits and mix them in.Code example taken from the Scala Book
  * (http://www.scala-lang.org/documentation/books.html - chapter 12)
  *
  * @author blazko
  * @since 2016-07-08
  *
  */
class MixingStackable extends FeatureSpec with GivenWhenThen {

  /**
    * Out queue interface.
    */
  abstract class IntQueue {
    def get(): Int

    def put(x: Int)
  }

  /**
    * Simple queue implementation.
    */
  class BasicIntQueue extends IntQueue {

    private val buf = new ArrayBuffer[Int]

    override def get(): Int = buf.remove(0)

    override def put(x: Int): Unit = {
      buf += x
    }
  }

  /**
    * Trait for queue that takes care of doubling the value put into it (i.e. multiply by 2).
    * Note that we are using {@code abstract override} here, which is only allowed in traits and
    * advices the compiler that we need the caller to already have an implementation of the
    * according function (invoked via {@code super#put} here).
    */
  trait Doubling extends IntQueue {
    abstract override def put(x: Int): Unit = {
      super.put(2 * x)
    }
  }

  /**
    * Trait that increments value by one before adding to queue.
    */
  trait Incrementing extends IntQueue {
    abstract override def put(x: Int): Unit = {
      super.put(x + 1)
    }
  }

  /**
    * Trait that only puts value to queue when >= 0.
    */
  trait Filtering extends IntQueue {
    abstract override def put(x: Int): Unit = {
      if (x >= 0) super.put(x)
    }
  }

  feature("BasicIntQueue") {
    info(
      """Using BasciIntQueue, testing that adding values to it
        |and then reading, we get values in the same order in which
        |we have put them in.""".stripMargin)

    scenario("dunno") {
      Given("Creating empty queue and adding values 10, then 20")
      val q = new BasicIntQueue
      q.put(10)
      q.put(20)

      When("Reading from queue twice")
      val expecting10 = q.get()
      val expecting20 = q.get()

      Then("Expecting to have read in order values have been originally put in (i.e. 10, then 20)")
      assert(expecting10 == 10)
      assert(expecting20 == 20)
    }
  }

  feature("Simple mixins") {

    scenario("Doubling using new class") {

      Given("DoublingQueue with two values added (10, 20)")
      class DoublingQueue extends BasicIntQueue with Doubling // using dedicated class definition
      val dqueue = new DoublingQueue
      dqueue.put(10)
      dqueue.put(20)

      When("Reading from queue twice")
      val expecting20 = dqueue.get()
      val expecting40 = dqueue.get()

      Then("Expecting to have receive original values doubled (value x 2, so expecting 20, 40)")
      assert(expecting20 == 20)
      assert(expecting40 == 40)
    }

    scenario("Doubling using runtime mixing") {

      Given("DoublingQueue with two values added (10, 20)")
      val dqueue = new BasicIntQueue with Doubling // using runtime construct (no explicit class def!)
      dqueue.put(10)
      dqueue.put(20)

      When("Reading from queue twice")
      val expecting20 = dqueue.get()
      val expecting40 = dqueue.get()

      Then("Expecting to have receive original values doubled (value x 2, so expecting 20, 40)")
      assert(expecting20 == 20)
      assert(expecting40 == 40)
    }

  }

  feature("Stacked") {

    info("Mixing multiple traits to class, in this context an entire chain is executed (rightmost first)")

    scenario("Mix two traits into class") {
      Given("Adding value to queue: -1, 0 and 1")
      val mightyQueue = new BasicIntQueue with Incrementing with Filtering // eval from right to left!
      mightyQueue.put(-1)
      mightyQueue.put(0)
      mightyQueue.put(1)

      When("Reading from queue twice")
      val expecting1 = mightyQueue.get()
      val expecting2 = mightyQueue.get()

      Then("Expecting -1 filtered away, 0 incremented to 1, 1 incremented to 2")
      assert(expecting1 == 1)
      assert(expecting2 == 2)
    }
  }
}