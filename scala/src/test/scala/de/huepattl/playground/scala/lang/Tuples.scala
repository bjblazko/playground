package de.huepattl.playground.scala.lang

import java.time.LocalDateTime

import org.scalatest.FunSuite

/**
  * Demonstrates built-in tuples, like known from the *ML languages. Just like these, tuples are not zero-based
  * but start from 1.
  *
  * @author blazko
  * @since 2016-06-11
  */
class Tuples extends FunSuite {

  /**
    * Simple example on how to declare a tuple value and how to query it.
    */
  test("simple") {
    val pair = ("NCC", 1701)
    printf("Contents of 'pair', type %s:\n", pair.getClass)
    printf("- '%s' (type: %s)\n", pair._1, pair._1.getClass)
    printf("- '%s' (type: %s)", pair._2, pair._2.getClass)
  }

  /**
    * Tuples can take more than two values, see here. There is built-in support for up to {@link Tuple11}.
    */
  test("many") {
    val values = ("NCC", 1701, true, LocalDateTime.now())
    printf("Contents of 'values', type %s:\n", values.getClass)
    printf("- '%s' (type: %s)\n", values._1, values._1.getClass)
    printf("- '%s' (type: %s)\n", values._2, values._2.getClass)
    printf("- '%s' (type: %s)\n", values._3, values._3.getClass)
    printf("- '%s' (type: %s)\n", values._4, values._4.getClass)
  }

  /**
    * Example function returning a tuple instead of single values or objects.
    *
    * @param arg1 1st argument to echo
    * @param arg2 2nd argument to echo
    * @param arg3 3rd argument to echo
    * @return Returned Tuple3 containing the arguments passed.
    * @see #useFunction
    */
  def echo(arg1: String, arg2: Int, arg3: Boolean): Tuple3[String, Int, Boolean] = {
    return (arg1, arg2, arg3)
  }

  /**
    * Example of calling a function returning a tuple.
    *
    * @see #echo
    */
  test("useFunction") {
    // Using tuple...
    val res = echo("Hello", 1701, true)
    printf("Arg1: %s, Arg2: %d, Arg3: %b", res._1, res._2, res._3)

    // Using discrete value inference...
    val (es, ei, eb) = echo("Hello", 1701, true)
    printf("Arg1: %s, Arg2: %d, Arg3: %b", es, ei, eb)
  }
}
