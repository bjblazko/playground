package de.huepattl.playground.scala.lang.traits

import org.scalatest.{FeatureSpec, GivenWhenThen}

/**
  * Created by blazko on 06.07.16.
  */
class Comparing extends FeatureSpec with GivenWhenThen {

  class StringNumber(val value: String) extends Ordered[StringNumber] {

    /**
      * From the original docs of {@link Ordered}:
      * Result of comparing this with operand that. Returns x where:
      * - x < 0 if this < that
      * - x == 0 if this == that
      * - x > 0 if this > that
      * @param that
      * @return
      */
    override def compare(that: StringNumber): Int = {
      this.value match {
        case "one" => {
          that.value match {
            case "one" => 0 // that is =
            case "two" => -1 // that is >
            case "three" => -1 // that is >
          }
        }
        case "two" => {
          that.value match {
            case "one" => 1 // that is <
            case "two" => 0 // that is =
            case "three" => -1 // that is >
          }
        }
        case "three" => {
          that.value match {
            case "one" => 1 // that is <
            case "two" => 1 // that is <
            case "three" => 0 // that is =
          }
        }
      }
    }
  }

  feature("StringNumber can be compared against others") {
    info("...")

    ignore("Equality check") {
      Given("Two equal string numbers, both having same value ")
      val oneA = new StringNumber("one")
      val oneB = new StringNumber("one")


      When("Compared for equality")
      val compared = oneA == oneB // FIXME! Debugger tells me it is not entering compare()

      Then("OK")
      assert(compared)
    }

    scenario("Greater than") {
      Given("Two equal string numbers, both having same value ")
      val one = new StringNumber("one")
      val two = new StringNumber("two")
      val three = new StringNumber("three")


      When("Compared for >")
      val compared1 = two < three

      Then("OK")
      assert(compared1)

      When("Compared for >")
      val compared2 = three > two

      Then("OK")
      assert(compared2)

    }

  }
}
