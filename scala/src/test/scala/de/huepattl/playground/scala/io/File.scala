package de.huepattl.playground.scala.io

import org.scalatest.{FlatSpec, FunSuite}

import scala.io.Source

/**
  * Simple file input and output operations.
  *
  * @author blazko
  * @since 2016-06-11
  */
class File extends FunSuite {

  test("Loading a file and printing it line by line") {
    for (line <- Source.fromFile("/etc/passwd").getLines()) {
      println(line)
    }
  }

}
