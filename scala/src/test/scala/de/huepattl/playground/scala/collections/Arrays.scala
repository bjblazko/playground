package de.huepattl.playground.scala.collections

import org.scalatest.FunSuite

import scala.collection.mutable.ListBuffer

/**
  * Demonstration of arrays (always mutable).
  *
  * @author blazko
  * @since 2016-06-11
  */
class Arrays extends FunSuite {

  val stringArray = Array("one", "two", "three", "four", "five")

  test("buildSimpleList") {
    val arr = new Array[String](3)

    arr.update(0, "one") // same as next line
    arr(0) = "one"

    arr(1) = "two"
    arr(2) = "three"
  }

  test("whileLoop") {
    var i = 0;
    while (i < stringArray.length) {
      println(stringArray(i))
      i += 1
    }
  }

  test("foreachLoop") {
    stringArray.foreach(elem => println(elem))
    stringArray.foreach(println) // if single-arg, use this
  }

  test("forLoop") {
    for (elem <- stringArray) {
      println(elem)
    }

    for (i <- 2.to(4)) println(stringArray(i)) // same as next line
    for (i <- 2 to 4) println(stringArray(i))
  }

}
