package de.huepattl.playground.scala.collections

import org.scalatest.FunSuite

import scala.collection.mutable.ListBuffer

/**
  * Demonstration of Lists (always immutable).
  *
  * @author blazko
  * @since 2016-06-11
  * @see lang.Tuples
  * @see Lists
  */
class Lists extends FunSuite {

  val stringList = List("one", "two", "three", "four", "five")
  val alphabet = ('A' to 'Z').mkString.toCharArray

  def nl = println()

  def nl2 = {
    println()
    println()
  }

  /**
    * The colon ":" causes the left side to be an operand of the right side.
    * So, the below code can also be written as
    * {@code
    * List("six", "seven").:::(stringList)
    * }
    * Here, {@code stringList} is prepended to the new List.
    * {@code ::} prepends a single element while {@code :::} prepends a list.
    */
  test("prepend") {
    val untilSeven = stringList ::: List("six", "seven")
    untilSeven.foreach(println)
    nl

    val withZero = "zero" :: untilSeven
    withZero.foreach(println)
    nl

    println(1 :: 2 :: 3 :: Nil) // Nil is required, so that we have a left hand operator
  }

  test("listbuilder") {
    var listbuilder = new ListBuffer[Int]
    for (i <- 65 to 90) {
      listbuilder += i // append single element
    }
    listbuilder.toList.foreach(value => print(value.toChar + " "))
    nl

    listbuilder +=(33, 34) // append multiple elelements at once
    listbuilder.toList.foreach(value => print(value.toChar + " "))
    nl2
  }

  test("basic") {
    alphabet.foreach(print) // original
    nl2

    println("Reverse: ")
    alphabet.reverse.foreach(print) // reverse list (ZYX...CBA)
    nl2

    println("Length: ")
    println(alphabet.length) // length/size of list
    nl2

    println("Only letters?")
    println(alphabet.forall(c => {
      c.isLetter
    })) // checks is all elements are letters and no number etc.
    nl2

    println("Does our alphabet contain letter 'M'?")
    println(alphabet.exists(c => c == 'M'))
    nl2

    println("Empty?")
    println(alphabet.isEmpty)
    nl2

    println("Count even letters (based on their numberical ASCII value)")
    println(alphabet.count(c => (c.toInt % 2) == 0))
    nl2

    println("Modify each entry: ")
    stringList.map(c => c + "?").foreach(print)
    nl2

    println("MkString: ")
    println(alphabet.mkString(", "))
    println(alphabet.mkString("[", ", ", "]"))
    nl2

    println("Equality: ")
    println(List(1, 2, 3) == List(1, 2, 3)) // equal
    println(List(1, 2, 3) == List(4, 5, 6)) // differ
    println(List(1, 2, 3) == null) // ...even when null
    nl2

  }

  test("filters") {
    println("Filter: ")
    alphabet.filter(c => c == 'M').foreach(print)
    nl
    alphabet.filterNot(c => c == 'M').foreach(print)
    nl2

    println("Drop first 5:")
    alphabet.drop(5).foreach(print) // first 5 elements missing
    nl2

    println("Drop last 3:")
    alphabet.dropRight(3).foreach(print) // last 3 elements missing
    nl2

    println("Head: ")
    println(alphabet.head) // 1st element, i.e. A
    nl2

    println("Last: ")
    println(alphabet.last) // last element, i.e. Z
    nl2

    println("Init:")
    alphabet.init.foreach(print) // all elements except last (Z missing)
    nl2

    println("Tail:")
    alphabet.tail.foreach(print) // all elements except first (A missing)
    nl2

    println("Random access, get 2nd: ")
    println(alphabet(1))
  }

  test("sorting") {
    println("Sort: ")
    alphabet.sorted.foreach(print) // implicit sort
    nl
    alphabet.sortWith((a, b) => a > b).foreach(print) // sort by value/property
    nl
    alphabet.sortWith((a, b) => a < b).foreach(print) // sort by value/property
    nl
    alphabet.sortBy(c => (c.toInt % 2 == 0)).foreach(print) // sort by function, here odd vs. even
  }


}
