package de.huepattl.playground.scala.lang

import org.scalatest.{FunSpec, FunSuite}

/**
  * Created by tboewing on 23/06/16.
  */
class Functions extends FunSuite {

  {
    def noArgs1(): Unit = {
      println("noargs #1")
    }

    def noArgs2() = {
      println("noargs #2")
    }

    def noArgs3(): Unit = {
      println("noargs #3")
    }

    def noArgs4 {
      println("noargs #4")
    }

    def args(arg1: String, arg2: Int, arg3: Boolean): String = {
      s"Got these arguments:\n\targ1(String): ${arg1}\n\targ2(Int): ${arg2}\n\targ3(Boolean): ${arg3}"
    }

    def varargs(i: Int, b: Boolean, va: String*): String = {
      val values = va.mkString("[", ",", "]")
      s"${i}, ${b}, varargs: ${values}"
    }

    def defaultarg(arg1: String, arg2: Int = 1977, arg3: Boolean = false): String = {
      s"Got these arguments:\n\targ1(String): ${arg1}\n\targ2(Int): ${arg2}\n\targ3(Boolean): ${arg3}"
    }

    /**
      * The functions used show four different styles of calling functions with no arguments and no return.
      */
    test("arguments: none") {
      noArgs1()
      noArgs2()
      noArgs3()
      noArgs4
    }

    test("arguments: anonymous (by type)") {
      println(args("Hello", 1977, true))
    }

    test("arguments: named (same order, matching function signature)") {
      println(args(arg1 = "Hello", arg2 = 1977, arg3 = true))
    }

    test("arguments: named (different order, not matching function signature)") {
      println(args(arg3 = true, arg1 = "Hello", arg2 = 1977))
    }

    test("arguments: variable list") {
      println(varargs(1977, true, "One"))
      println(varargs(1977, true, "One", "Two"))
      println(varargs(1977, true, "One", "Two", "Three"))
    }

    test("arguments: defaults/optional") {
      println(defaultarg("Hello", 2, true))
      println(defaultarg("Hello"))
      println(defaultarg("Hello", 2))
      println(defaultarg("Hello", arg3 = true))
    }
  }

  {
    def functionWithLocalOne(arg: String): String = {

      def addQuotes: String = {
        s"'${arg}'" // look, ma! I have access to scope of enclusing function
      }

      addQuotes
    }

    test("function-local functions") {
      println(functionWithLocalOne("hello"))
    }
  }

  test("first class functions") {
    var inc = (toIncrement: Int, by: Int) => toIncrement + by
    val by = 100

    var i = 0
    println(i)
    i = inc(i, by)
    println(i)
    i = inc(i, by)
    println(i)
  }

  {
    def addFunction(a: Int, b: Int): Int = {
      a + b
    }

    val partiallyAppliedAddFunction = addFunction _
    val partiallyAppliedAddFunction2 = addFunction(1, _: Int)

    //val helloWhat = (x: String) => "Hello " + x + what + "!"

    test("partially applied functions") {
      println(addFunction(3, 5)) // = 8
      println(partiallyAppliedAddFunction.apply(3, 5)) // = 8
      println(partiallyAppliedAddFunction(3, 5)) // = 8

      println(partiallyAppliedAddFunction2(5)) // = 6

      //var what = "World"
      //println(helloWhat)
    }
  }

  {
    def helloCurry(a: String)(b: String):String = {
      s"Hello ${a} ${b}!"
    }

    test("curry") {
      println(helloCurry("Curry")("World"))
    }
  }
}
