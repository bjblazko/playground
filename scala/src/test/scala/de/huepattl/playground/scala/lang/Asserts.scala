package de.huepattl.playground.scala.lang

import java.lang.IllegalArgumentException


//import org.scalatest.{Assertions => _, _}
import org.scalatest._

/**
  * Assertion and similar checks.
  *
  * @author blazko
  * @since 2016-07-08
  */
class Asserts extends FunSuite {

  private val TestValue = "Hello!"

  def echoSimple(arg: String): String = arg


  def echoAssert(arg: String): String = {
    assert(arg != null, "argument may not be null")
    arg
  }

  /**
    * Recommended for production: use require() for checking inputs at runtime.
    */
  def echoRequire(arg: String): String = {
    require(arg != null, "argument may not be null")
    arg
  }

  def echoPostCheck(arg: String): String = {
    {
      arg
    } ensuring(arg != null, "result may not be null")
  }

  def echoOption(arg: Option[String]): String = {
    arg.getOrElse("default")
  }


  test("no check") {
    val succ = echoSimple(TestValue)
    val fail = echoSimple(null)

    assert(succ == TestValue)
    assert(fail == null)
  }

  test("check with require()") {
    // TODO: Option[String]
    try {
      val succ = echoRequire("")
      val fail = echoRequire(null)
    } catch {
      case iae: IllegalArgumentException => println(s"(This is OK!) Caught ${iae.getClass.getCanonicalName}: '${iae.getMessage}'")
    }
  }

  ignore("check with assert() - does not work, calls ScalaTest's assert despite modified import above...") {
    try {
      val succ = echoAssert("")
      val fail = echoAssert(null)
    } catch {
      case iae: IllegalArgumentException => println(s"(This is OK!) Caught ${iae.getClass.getCanonicalName}: '${iae.getMessage}'")
    }
  }

  test("post-check with ensuring()") {
    try {
      val fail = echoPostCheck(null)
    } catch {
      case iae: AssertionError => println(s"(This is OK!) Caught ${iae.getClass.getCanonicalName}: '${iae.getMessage}'")
    }
  }

  test("post-check with Option[T]") {
    val succ = echoOption(Option(TestValue))
    val fail = echoOption(Option(null))

    assert(succ == TestValue)
    assert(fail == "default")
  }

}
