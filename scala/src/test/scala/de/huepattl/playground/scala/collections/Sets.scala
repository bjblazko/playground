package de.huepattl.playground.scala.collections

import org.scalatest.FunSuite

import scala.collection._
import scala.collection.immutable.HashSet

/**
  * Demonstration of Sets, they can be mutable (via import) or immutable (default).
  * <pre>
  * scala.collection.Set (trait)
  * -> scala.collection.immutable.Set (trait)
  * -> scala.collection.immutable.HashSet
  * -> scala.collection.mutable.Set (trait)
  * -> scala.collection.mutable.HashSet
  * </pre>
  * Sets, unlike Lists, do not guarantee order.
  *
  * @author blazko
  * @since 2016-06-11
  */
class Sets extends FunSuite {

  /**
    * When fprcing usage of a HashSet, we guarantee that no entry exists more than once. When using the default
    * (i.e. just Set), there is no such guarantee).
    */
  test("hashing") {
    // No order guaranteed, entries may exist multiple times
    val set = Set("Enterprise", "Voyager", "Voyager", "Intrepid", "Intrepid", "Defiant")
    println("Hoping to find 3 instead of 5: " + set.size)

    // No order guaranteed, entries only exist once
    val hset = HashSet("Enterprise", "Voyager", "Voyager", "Intrepid", "Intrepid", "Defiant")
    println("Guaranteed to find 3 instead of 5: " + hset.size)
  }

  /**
    * Use an immutable set. Per change to the set, a new instance is returned.
    */
  test("appendImmutableSet") {
    var set = Set("Enterprise", "Voyager", "Constellation")
    for (i <- 1 to 5) {
      set += "Ship " + i
      println(System.identityHashCode(set)) // prove that each time we have a DIFFERENT identity (=instance)
    }
    println(set.mkString(", "))
  }

  /**
    * Use a mutable set. Per change, the same set is reused.
    */
  test("appendMutableSet") {
    var set = mutable.Set("Enterprise", "Voyager", "Constellation")
    println(set.getClass)
    set = set + "Grissom" // new Set
    set += "Defiant" // new Set, shorter notation
    set.+=("DS9") // this is what above is really done...
    for (i <- 1 to 5) {
      set += "Ship " + i
      println(System.identityHashCode(set)) // prove that each time we have the SAME identity (=instance)
    }
    println(set.mkString(", "))
  }

  test("useful") {
    val ships = Set("Enterprise", "Voyager", "Excelsior")

    println("Shipp missing due to Transwarp? " + (ships - "Excelsior").mkString(", "))

    // for more, see Lists
  }

}
