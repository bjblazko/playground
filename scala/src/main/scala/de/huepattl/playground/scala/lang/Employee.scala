package de.huepattl.playground.scala.lang

import java.time.LocalDate

/**
  * Created by tboewing on 12/06/16.
  */
case class Employee(
                firstName: String,
                lastName: String,
                dateOfBirth: LocalDate = LocalDate.of(1980, 12, 31)) {

  require(firstName != null && firstName.length > 1)

  //def this(firstName:String, lastName: String) = this(firstName, lastName, null)

  override def toString: String = s"Employee: $firstName $lastName, born $dateOfBirth"

}
