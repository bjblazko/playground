/**
  * That is basically the hello world as a Scala Script, printing to STDOUT.
  * Just execute in the command line via:
  * {@code
  * scala hello.sc
  * }
  */
println("Hello Scala!")

/**
  * Defines an immutable value, deferred to String automatically by compiler.
  * Trying to change it's value will result in a compiler error.
  */
val staticMessage = "Hello static message"

/**
  * Defines an immutable value, declared as String explicitly.
  */
val staticMessage2: String = "explicitly defined as string"

/**
  * A string variable, can be changed.
  */
var variableMessage = "Hello variable message"

/**
  * Function definition in the form:
  * {@code
  * def name(arg1: type, arg2: type...): ResultType = { body }
  * }
  * Note that there is no result, indicated by {@link Unit}, similar to Java's void.
  *
  * @param msg String message to print
  */
def sayHello(msg: String): Unit = {
  println(msg)
}

/**
  * Performs an addition using both arguments.
  *
  * @param arg1 First number.
  * @param arg2 Secind number to add to first one.
  * @return Sum of #arg1 and #arg2
  */
def calculate(arg1: Int, arg2: Int): Int = {
  val res = arg1 + arg2
  return res
}

// Prints our val
sayHello(staticMessage)

// Prints our var
sayHello(variableMessage)

// Update nad print changed var
variableMessage += " updated"
sayHello(variableMessage)

// Perform caluclation and print it
println("1 + 2 = " + calculate(1, 2))
println(s"Calculate directly: 1 + 2 = ${1 + 2}")

// String formatting
val withEscapes = "Hello,\nWorld!"
println(s"Inject variable $withEscapes") // merges var, including escapes
println(raw"Inject variable $withEscapes") // merges var, but ignores escapes
println(f"${math.Pi}%.4f") // like printf

/*
 * Of course, you can pass command line arguments to the script, e.g. using {@code scala hello.sc one two three}.
 * There is an implicit array field {@code args} that you can access by index. Note that we use normal braces () here
 * instead of rectangular brackets like in java (as we call a method, actualy...).
 */
if (args.length > 0) {
  println("Args:")
  var i = 0;
  while (i < args.length) {
    println("Argument " + i + ": " + args(i))
    i += 1;
  }
}