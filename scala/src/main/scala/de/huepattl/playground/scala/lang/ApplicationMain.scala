package de.huepattl.playground.scala.lang

/**
  * Created by tboewing on 11/06/16.
  */
class ApplicationMain {

  def main(args: Array[String]) {
    println("Hello, got arguments: " + args);
  }
}
