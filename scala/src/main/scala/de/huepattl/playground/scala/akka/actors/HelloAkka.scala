package de.huepattl.playground.scala.akka.actors

import akka.actor.{Actor, ActorSystem, Props}
import akka.actor.Actor.Receive

class Hello extends Actor {
  override def receive = {
    case "hello" =>
      println("Received 'hello'")
    case "world" =>
      println("received 'world'")
    case _ =>
      println(s"Received unknown")
  }
}

/**
  * Created by blazko on 22.06.16.
  */
object HelloAkka extends App {
  val system = ActorSystem("HelloSystem")
  val actor = system.actorOf(Props[Hello], name = "HelloActor")

  actor ! "hello"
  actor ! "world"
  actor ! "something else"
}
