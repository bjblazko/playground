package de.huepattl.playground.scala.akka.streams

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._

/**
  * Created by blazko on 26.08.16.
  */
trait RestService {
  implicit val system: ActorSystem
  implicit val materializer: ActorMaterializer

  val route =
    get {
      pathSingleSlash {
        complete {
          "Hello World"
        }
      } ~
      path("doh") {
        complete {
          "DOH!"
        }
      }
    }
}

class Server(implicit val system: ActorSystem,
             implicit val materializer: ActorMaterializer) extends RestService {
  /**
    *
    * @param address
    * @param port
    * @return
    */
  def startServer(address: String, port: Int) = {
    Http().bindAndHandle(route, address, port)
  }
}

object App {

  /**
    *
    * @param args
    */
  def main(args: Array[String]) {
    implicit val system = ActorSystem("helloWorldHttp")
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val server = new Server()
    server.startServer("localhost", 7676)
  }
}
