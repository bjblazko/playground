package de.huepattl.playground.scala.actors

import java.io.IOException

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.{ActorMaterializer, Materializer}
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.typesafe.config.{Config, ConfigFactory}
import spray.json.DefaultJsonProtocol
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.math._

/**
  * Simple RESTful microsverice.
  * Based on http://www.lightbend.com/activator/template/akka-http-microservice
  *
  * Uses src/main/resources/application.conf
  *
  * @author blazko
  * @since 2016-07-13
  */

case class IpInfo(
                   query: String,
                   country: Option[String],
                   city: Option[String],
                   latitude: Option[Double],
                   longitude: Option[Double]
                 )

case class IpPairSummaryRequest(ip1: String, ip2: String)

case class IpPairSummary(
                          distance: Option[Double],
                          ip1: IpInfo,
                          ip2: IpInfo)

object IpPairSummary {
  def apply(ip1: IpInfo, ip2: IpInfo): IpPairSummary =
    IpPairSummary(calculateDistance(ip1, ip2), ip1, ip2)

  private def calculateDistance(ip1: IpInfo, ip2: IpInfo): Option[Double] = {
    (ip1.latitude, ip1.longitude, ip2.latitude, ip2.longitude) match {
      case (Some(lat1), Some(long1), Some(lat2), Some(long2)) =>
        val φ1 = toRadians(lat1)
        val φ2 = toRadians(lat2)
        val Δφ = toRadians(lat2 - lat1)
        val Δλ = toRadians(long2 - long1)
        val a = pow(sin(Δφ / 2), 2) + cos(φ1) * cos(φ2) * pow(sin(Δλ / 2), 2)
        val c = 2 * atan2(sqrt(a), sqrt(1 - a))
        Option(EarthRadius * c)
      case _ => None
    }
  }

  private val EarthRadius = 6371.0
}

trait Protocols extends DefaultJsonProtocol {
  implicit val IpInfoFormat = jsonFormat5(IpInfo.apply)
  implicit val ipPairSummaryFormat = jsonFormat2(IpPairSummary.apply)
  implicit val ipPairSummaryRequestFormat = jsonFormat2(IpPairSummaryRequest.apply)
}

trait Service extends Protocols {
  implicit val system: ActorSystem

  implicit def executor: ExecutionContextExecutor

  implicit val materializer: Materializer

  def config: Config

  val logger: LoggingAdapter

  lazy val ipApiConnectionFlow: Flow[HttpRequest, HttpResponse, Any] =
    Http().outgoingConnection(
      config.getString("services.ip-api.host"),
      config.getInt("services.ip-api.port")
    )

  def ipApiRequest(request: HttpRequest): Future[HttpResponse] =
    Source.single(request).via(ipApiConnectionFlow).runWith(Sink.head)

  def fetchIpInfo(ip: String): Future[Either[String, IpInfo]] = {
    ipApiRequest(RequestBuilding.Get(s"/json/$ip")).flatMap {
      response =>
        response.status match {
          case StatusCodes.OK => Unmarshal(response.entity).to[IpInfo].map(Right(_))
          case StatusCodes.BadRequest => Future.successful(Left(s"$ip: incorrect IP format"))
          case _ => Unmarshal(response.entity).to[String].flatMap { entity =>
            val error = s"GeoIP request failed with ${response.status} and entity $entity"
            logger.error(error)
            Future.failed(new IOException(error))
          }
        }
    }
  }

  val routes = {
    logRequestResult("rest-service") {
      pathPrefix("ip") {
        (get & path(Segment)) { ip =>
          complete {
            fetchIpInfo(ip).map[ToResponseMarshallable] {
              case Right(ipInfo) => ipInfo
              case Left(errorMessage) => StatusCodes.BadRequest -> errorMessage
            }
          }
        } ~
          (post & entity(as[IpPairSummaryRequest])) { ipPairSummaryFormat =>
            complete {
              val ipInfoFuture1 = fetchIpInfo(ipPairSummaryFormat.ip1)
              val ipInfoFuture2 = fetchIpInfo(ipPairSummaryFormat.ip2)
              ipInfoFuture1.zip(ipInfoFuture2).map[ToResponseMarshallable] {
                //case (Right(info1), Right(info2)) => IpPairSummary(info1, info2)
                case (Left(errorMessage), _) => StatusCodes.BadRequest -> errorMessage
                case (_, Left(errorMessage)) => StatusCodes.BadRequest -> errorMessage
              }
            }
          }
      }
    }
  }
}

object AkkaRestService extends App with Service {
  override implicit val system = ActorSystem()
  override implicit val executor = system.dispatcher
  override implicit val materializer = ActorMaterializer()

  override implicit val config = ConfigFactory.load()
  override val logger = Logging(system, getClass)

  Http().bindAndHandle(routes, config.getString("http.interface"), config.getInt("http.port"))
}
