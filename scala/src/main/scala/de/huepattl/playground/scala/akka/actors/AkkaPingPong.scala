package de.huepattl.playground.scala.akka.actors

import akka.actor.Actor.Receive
import akka.actor._

/**
  * Created by blazko on 22.06.16.
  */

case object PingMessage

case object PongMessage

case object StartMessage

case object StopMessage

class Ping extends Actor {
  var count = 0

  def incrementAndPrint {
    count += 1; println("ping")
  }

  override def receive = {
    case StartMessage =>
      incrementAndPrint
      sender ! PingMessage
    case PongMessage =>
      incrementAndPrint
      if (count > 99) {
        sender ! StopMessage
        println("Stopped")
        context.stop(self)
      } else {
        sender ! PingMessage
      }
  }
}

class Pong extends Actor {
  override def receive = {
    case PingMessage =>
      println("  pong")
      sender ! PongMessage
    case StopMessage =>
      println("pong stopped")
      context.stop(self)
  }
}

object AkkaPingPong extends App {
  val system = ActorSystem("PingPongSystem")
  val pong = system.actorOf(Props[Pong], name = "pong")
  val ping = system.actorOf(Props[Ping], name = "ping")
  //val ping = system.actorOf(Props(new Ping(pong)), name = "ping")
  // start them going
  ping ! StartMessage
}
