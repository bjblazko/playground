logLevel := Level.Warn

// For development: Detects source/file changes, recompiles them, see /build.sbt
addSbtPlugin("io.spray" % "sbt-revolver" % "0.8.0")

// Shards application into a single JAR file
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.3")

// Oly needed when deploying on Heroku?
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.1.0-RC1")