package de.huepattl.playground.arquillian;

import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import java.util.List;
import javax.inject.Named;

/**
 * Simple (JPA) persistence service for {@link Employee}.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-07-21
 */
@Local
@Singleton
public class EmployeeService {

    /**
     * Our entity manager, configured by the according persistence XML.
     */
    @PersistenceContext
    EntityManager em;

    /**
     * Find a single employee by the given identifier.
     *
     * @param id Employee ID to find for.
     * @return Found employee or NULL.
     */
    public Employee findById(final long id) {
        TypedQuery<Employee> q = em.createQuery(
                "SELECT e FROM Employee e WHERE e.id = :id", Employee.class);
        q.setParameter("id", id);
        return q.getSingleResult();
    }

    /**
     * Returns all employees found in the database. In real life, use such method with care as it might drain your
     * performance.
     *
     * @return List of all found employees. May be empty but never null.
     */
    public List<Employee> findAll() {
        TypedQuery<Employee> q = em.createQuery(
                "SELECT e FROM Employee e", Employee.class);
        return q.getResultList();
    }

    /**
     * Persists an entity bean.
     *
     * @param employee Entity to persist/update.
     * @return Attached entity.
     */
    public Employee persist(Employee employee) {
        if (employee.getId() == 0) {
            // employee will be updated with ID
            em.persist(employee);
        } else {
            // Merge back to entity manager, apply auto-save
            employee = em.merge(employee);
        }
        return employee;
    }
}
