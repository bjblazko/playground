package de.huepattl.playground.arquillian.servlet;

import de.huepattl.playground.arquillian.SimpleCdiService;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by blazko on 18.08.15.
 */
@WebServlet("/hello")
public class HelloServlet extends HttpServlet {

    @Inject
    private SimpleCdiService simpleCdiService;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        PrintWriter writer = response.getWriter();
        writer.println(String.format(
                "<html>Hello, I am a Java Servlet! The time is: %s - as told by bean %s, instance %s</html>",
                simpleCdiService.whatTime(),
                simpleCdiService.getClass(),
                simpleCdiService.toString()));
        writer.flush();
    }
}
