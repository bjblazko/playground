package de.huepattl.playground.arquillian;

import javax.persistence.*;
import java.util.Date;

/**
 * Simple entity for Arquillian persistence test.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-07-21
 */
@Entity
public class Employee {

    /**
     * Primary key.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * Employee last name.
     */
    private String name;

    /**
     * Employee first name;
     */
    private String firstName;

    /**
     * Employee birthday.
     */
    @Temporal(TemporalType.DATE)
    private Date birthday;

    public Employee() {
        // NOOP
    }

    public Employee(final String name, final String fname, final Date birthday) {
        super();
        this.name = name;
        this.firstName = fname;
        this.birthday = birthday;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (birthday != null ? !birthday.equals(employee.birthday) : employee.birthday != null) return false;
        if (firstName != null ? !firstName.equals(employee.firstName) : employee.firstName != null) return false;
        if (name != null ? !name.equals(employee.name) : employee.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (birthday != null ? birthday.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
