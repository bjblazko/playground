package de.huepattl.playground.arquillian;

import javax.inject.Named;
import java.time.Instant;
import java.time.ZoneId;

/**
 * Simple service subject to be injected into test SimpleCdiServiceTest.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-07-19
 */
@Named
public class SimpleCdiService {

    /**
     * Simple method which is just returning a hello string with a name provided as argument.
     *
     * @param forWhom Some name to echo.
     * @return Hello string.
     */
    public String saySomething(final String forWhom) {
        return "Hello, " + forWhom +
                ". I come from a CDI injected service executed within Arquillian.";
    }

    /**
     * Just returns the current time in UTC.
     *
     * @return Current time, e.g. '2015-08-18T20:41:33.844Z[UTC]'.
     */
    public String whatTime() {
        return "Time " + Instant.now().atZone(ZoneId.of("UTC")).toString();
    }
}
