package de.huepattl.playground.arquillian.simple;

import de.huepattl.playground.arquillian.Employee;
import de.huepattl.playground.arquillian.EmployeeService;
import java.time.LocalDate;
import java.time.Month;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created with IntelliJ IDEA. User: blazko Date: 21.07.13 Time: 13:28 To change
 * this template use File | Settings | File Templates.
 */
@RunWith(Arquillian.class)
public class PersistenceTest {

    /*
     * Some test constants...
     */
    public static final String E1_NAME = "Doe";
    public static final String E1_FNAME = "John";
    public static final Date E1_BIRTH = new Date(java.sql.Date.valueOf(
            LocalDate.of(1977, 12, 31)).getTime());
    public static final String E2_NAME = "Smith";
    public static final String E2_FNAME = "Jane";
    public static final Date E2_BIRTH = new Date(java.sql.Date.valueOf(
            LocalDate.of(1980, Month.JULY, 22)).getTime());

    /**
     * This injects (done by the JEE container) our (persisttence) service to
     * our test.
     */
    @Inject
    EmployeeService employeeService;

    /**
     * <p>
     * Issued before the test - creates a transient JAR archive which is going
     * to be deployed into the configured (see POM) JEE container for test
     * execution. The archive will contain our employee entity class, the
     * employee persistence service, an empty beans.xml to allow CDI and our
     * persistence XML aliased to the standard name. Aliasing resources is an
     * easy way to test with severl persistence units just by providing the
     * correct one to {@link ShrinkWrap}.</p>
     * <p>
     * Please note our POM - even though we use JPA, we do not provide any
     * additional JPA dependencies such as Hibernate - because we execute in a
     * JEE container (e.g. embedded Glassfish), we have everything we need even
     * for testing. Only if you want to override default implementations you
     * need to provide these depnednecies.</p>
     *
     * @return Transient JAR archive containing all artefacts used for our
     *         integration test. Arquillian takes care of deployment and test
     *         execution through the according JUnit test runner
     *         ({@link Arquillian}).
     */
    @Deployment
    public static JavaArchive createJarToTest() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(Employee.class)
                .addClass(EmployeeService.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsManifestResource("persistence-test.xml", "persistence.xml");
    }

    /**
     * Our test: after our test classes have been deployed to our JEE container
     * using {@link ShrinkWrap}, we excute our JPA test against the micro-JAR.
     *
     * @throws Exception Fine for integration test.
     */
    @Test
    public void testEmployeePersistence() throws Exception {
        long e1id = 0;
        long e2id = 0;

        // Employee #1: Create object and a clone used to verify persistence.
        final Employee e1 = new Employee(E1_NAME, E1_FNAME, E1_BIRTH);
        final Employee e1verify = new Employee(E1_NAME, E1_FNAME, E1_BIRTH);

        // Employee #2: Create object and a clone used to verify persistence.
        final Employee e2 = new Employee(E2_NAME, E2_FNAME, E2_BIRTH);
        final Employee e2verify = new Employee(E2_NAME, E2_FNAME, E2_BIRTH);

        /*
         * Persist and retrieve both objects.
         */
        e1id = employeeService.persist(e1).getId();
        e2id = employeeService.persist(e2).getId();

        // Employye #1: Check that persisted entity equals our unpersisted one by value
        assertNotEquals(0, e1id); // was a primary key assigned by DB?
        assertEquals(e1verify, employeeService.findById(e1id)); // equals() w/o checking ID!

        // Employye #2: Check that persisted entity equals our unpersisted one by value
        assertNotEquals(0, e2id); // was a primary key assigned by DB?
        assertEquals(e2verify, employeeService.findById(e2id)); // equals() w/o checking ID!

        // Do both entities really have different unique IDs?
        assertNotEquals(e1id, e2id);

        /*
         * Show the results.
         */
        final List<Employee> all = employeeService.findAll();

        System.out.println("Listing all persisted employees:");
        for (final Employee current : all) {
            System.out.println("   - Found: " + current);
        }

    }
}
