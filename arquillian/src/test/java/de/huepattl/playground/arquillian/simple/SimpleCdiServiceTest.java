package de.huepattl.playground.arquillian.simple;

import de.huepattl.playground.arquillian.SimpleCdiService;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

/**
 * This demo shows the most simple test using Arquillian. It assumes a service to test to be injected via CDI.
 * The service {@link de.huepattl.playground.arquillian.SimpleCdiService} is packed into a transient JAR (accomplished in the {@link #createJarToTest()}
 * annotated with {@link Deployment}), deployed to any JEE container (like Glassfish) and tested against.
 * <p>
 * For this, JUnit delegates to the {@link Arquillian} JUnit runner which will launch the JEE container, deploy the
 * transient JAR and issue the test. Test execution depends on whether the container is embedded (same JVM), managed or
 * remote (separate JVM, tests instructed e.g. via HTTP).
 * <p>
 * This allows you to use advanced JEE features such as EJB3.1/CDI etc. but without having the need to write mocks
 * and bind them manually. Just define the dependent classes for your test set in the micro-deployment (via.
 * ShrinkWrap) and you are done.
 * <p>
 * Despite the fact that you often have the choice of the container mode (embedded, managed, remote), you have several
 * choices which (JEE) containers to use for testing. Examples are:
 * <p>
 * <ul>
 *     <li>(JBoss AS)</li>
 *     <li>Glassfish</li>
 *     <li>Weld</li>
 *     <li>Tomcat</li>
 *     <li>Jetty</li>
 *     <li>Websphere</li>
 *     <li>Weblogic</li>
 *     <li>OpenEJB/OpenWebBeans</li>
 * </ul>
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-07-19
 */
@RunWith(Arquillian.class)
public class SimpleCdiServiceTest {

    /**
     * This is the CDI managed bean that we are going to inject and use in our test.
     */
    @Inject
    private SimpleCdiService cdiService;

    /**
     * This method takes our CDI test service, an empty beans.xml (we use annotations, but the file has to be present)
     * and returns a transient JAR which is going to be deployed into our test container (Glassfish) an tested against.
     *
     * @return Transient JAR to be deployed into test cotainer.
     */
    @Deployment
    public static JavaArchive createJarToTest() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(SimpleCdiService.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    /**
     * This test is actually executed in our embedded JEE container (Glassfish)! It uses the deployed JAR as provided
     * by
     * {@link #createJarToTest()} and invokes the CDI-injected service, which just returns a simple message that we
     * echo
     * here.
     */
    @Test
    public void testHelloFromCdiService() {
        final String echo =
                cdiService.saySomething(SimpleCdiServiceTest.class.getCanonicalName());

        System.out.println("Echo from injected CDI service: " + echo);

        Assert.assertNotNull(echo);
    }
}
