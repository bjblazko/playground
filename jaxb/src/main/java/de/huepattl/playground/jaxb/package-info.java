/**
 * Besdies documentary reasons, this class takes care of using a default
 * namespace in our XML files for all classes contained in this package. Thus,
 * you do not need to annotate your beans with additional namespace
 * information.<p>
 * So, all beans in this package represent business objects and are enriched
 * with JAXB annotations that allow for marshalling these into XML (or JSON)
 * files and vice versa, generate an object structure from a correct XML file.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-06-07
 * @see Company
 * @see Organisation
 * @see Employee
 */
@XmlSchema(namespace = "http://huepattl.de/example", elementFormDefault = XmlNsForm.QUALIFIED, xmlns = {
    @XmlNs(prefix = "ex", namespaceURI = "http://huepattl.de/example")})
package de.huepattl.playground.jaxb;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
