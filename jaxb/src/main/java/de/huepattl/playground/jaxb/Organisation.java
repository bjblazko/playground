package de.huepattl.playground.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Organisation unit also being a XML entity.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-07-23
 */
@XmlRootElement(name = "org")
@XmlAccessorType(XmlAccessType.FIELD)
public class Organisation {
    
    @XmlAttribute
    private String name;

    @XmlElementWrapper(name = "employees")
    @XmlElement(name = "person")
    private List<Employee> employees = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

}
