package de.huepattl.playground.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Root object for an org graph. All th XML files generated from all these
 * classes will contain namespace information. This is archieved by declaring
 * such namespace in the package-info file.
 *
 * @author blazko
 * @since 2014-07-02
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class Company {

    /**
     * {@link XmlElementWrapper} will add a grouping element, while
     * {@link XmlElement} declares each entry in that collection, so the output
     * will look like this:
     * <p>
     * >
     * {@code
     * &lt;organisations&gt;
     *    &lt;org&gt;...
     *    &lt;org&gt;...
     * &lt;/organisations&gt;
     * }
     */
    @XmlElementWrapper(name = "organisations")
    @XmlElement(name = "org")
    private List<Organisation> organisations = new ArrayList<>();

    public List<Organisation> getOrganisations() {
        return organisations;

    }

    public void setOrganisations(List<Organisation> organisations) {
        this.organisations = organisations;
    }
}
