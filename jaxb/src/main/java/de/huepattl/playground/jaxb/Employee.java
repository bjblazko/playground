package de.huepattl.playground.jaxb;

import java.util.ArrayList;
import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.List;

/**
 * A custom annotated XML entity using JAXB annotations. There are basically two
 * ways of using JAXB entities: manually annotate existing classes or let JAXB
 * (XJC) generate classes from an existing XML Schema (XSD). In real life, you
 * would use JAXB generated classes only for serialisation and not in your core
 * code. With manually annotated code, you have simpler coding but it is much
 * more hard to to make them work with a schema. Most simple is to have no
 * generated classes and use annotated classes in cases you do not have to
 * fulfill an XSD.
 * <p>
 * {@link javax.xml.bind.annotation.XmlRootElement} marks this class as an XML
 * entity. {@link javax.xml.bind.annotation.XmlType} with propOrder here is ised
 * to define the order for XML serialisation.
 * {@link javax.xml.bind.annotation.XmlAccessorType} with FIELD causes JAXB/XJC
 * to look at the class fields and ignore the getters/setters.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-07-23
 */
@XmlRootElement(name = "employee")
@XmlType(propOrder = {"id", "name", "firstName", "birthDate", "external", "emails"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Employee {

    /**
     * Defining {@link javax.xml.bind.annotation.XmlAttribute} causes the field
     * to be serialised as an XML attribute, e.g.: null null null     {@code
     * <employee id="1">...</employee>
     * }
     */
    @XmlAttribute
    private long id;

    /**
     * Using {@link javax.xml.bind.annotation.XmlTransient} causes a field to be
     * ignored for XML (de-) serialisation. However, use it during runtime as
     * usual.
     */
    @XmlTransient
    private int doNotUseExternally;

    /**
     * {@link javax.xml.bind.annotation.XmlElement} will be used by default on
     * all fields, so there is no need to explicitly use it. So, each field will
     * become a dedicated XML <element />. In order to ignore fields, use
     * {@link javax.xml.bind.annotation.XmlTransient} annotation. Making an
     * attribute (<element attrib="foo" />), use the
     * {@link javax.xml.bind.annotation.XmlAttribute} annotation.
     */
    @XmlElement
    private String name;

    //@XmlElement(name = "first-name")
    private String firstName;

    //@XmlElement(name = "birth-date", nillable = true)
    private Date birthDate;

    private boolean external;

    /**
     * XmlList is interesting: it allows plain list, for exmple, this will render to:
     * {@code
     * &lt;emails&gt;mail@example another@example&lt;/emails&gt;
     * }
     */
    @XmlElement
    @XmlList
    private List<String> emails;

    public Employee() {

    }

    public Employee(final long id, final String name, final String firstName, final Date birthDate) {
        super();
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.birthDate = birthDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getFirstName() {
        return firstName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setBirthDate(Date date) {
        this.birthDate = date;
    }

    public boolean isExternal() {
        return external;
    }

    public void setExternal(boolean external) {
        this.external = external;
    }

    public List<String> getEmails() {
        if (this.emails == null) {
            emails = new ArrayList<>();
        }
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name
                + ", firstName=" + firstName + ", birthDate="
                + birthDate + ", external=" + external + '}';
    }

}
