package de.huepattl.playground.jaxb;

/**
 * Imagine that for historic reasons our XML Schmea (XSD) file defines booleans that do not use xsd:boolean but
 * plain strings. For our annotaed model class {@link Employee} as well as for the generated one, we do not want to
 * deal
 * with a string property but want to have it as a clean boolean one. There is help! Using our mapping file, we can
 * define
 * a converter that translates between those.
 * User: tboewing
 * Date: 10/1/13
 * Time: 6:01 PM
 * To change this template use File | Settings | File Templates.
 */
public final class BooleanStringConverter {
    public static String marshal(boolean value) {
        return Boolean.toString(value);
    }

    public static boolean unmarshal(String value) {
        return Boolean.parseBoolean(value);
    }
}
