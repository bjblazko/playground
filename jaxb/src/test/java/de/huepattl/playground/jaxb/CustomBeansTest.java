package de.huepattl.playground.jaxb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import javax.xml.XMLConstants;
import javax.xml.bind.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests demonstrating how to deal with an existing, manually annotated Java
 * Bean (POJO) (=XML Entity).
 *
 * @since 2013-07-23
 * @author Timo Boewing (bjblazko@gmail.com)
 */
public class CustomBeansTest {

    private static final Logger LOG = LogManager.getLogger(CustomBeansTest.class.getName());

    /**
     * JUnit takes care to clean and delete that folder after test execution.
     */
    @Rule
    public TemporaryFolder temp = new TemporaryFolder();

    /**
     * Takes an existing (i.e. not generated) POJO and creates XML data with
     * help of annotations in the model.
     *
     * @throws Exception
     * @see #xmlFileToBeans()
     */
    @Test
    public void beansToXmlFile() throws Exception {
        File testFile = temp.newFile("test.xml");
        LOG.info("Using temporary test file: " + testFile.getCanonicalPath());

        /**
         * Create a JAXB context y telling it the annotated root class(es) for
         * marshalling later on.
         */
        JAXBContext context = JAXBContext.newInstance(Company.class);

        /**
         * Test data: a company with one organisation containing three
         * employees.
         */
        final Company rootOrg = TestData.generateCompany(3);

        /*
         * Marshal POJO structure to XML (file).
         */
        {
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            /*
             * Runtime does not matter ;) 1. marshal to STDOUT 2. marshal to
             * temporary test file
             */
            marshaller.marshal(rootOrg, System.out);
            marshaller.marshal(rootOrg, testFile);
        }
    }

    /**
     * This example generates a file from existing beans just like in
     * {@link #beansToXmlFile()}, and unmarshals the file back into new,
     * identical set of beans.
     *
     * @throws Exception
     * @see #beansToXmlFile()
     */
    @Test
    public void xmlFileToBeans() throws Exception {
        File testFile = temp.newFile("test.xml");
        LOG.info("Using temporary test file: " + testFile.getCanonicalPath());

        JAXBContext context = JAXBContext.newInstance(Company.class);

        /*
         * Create XML file as basis to unmarshall back to POJOs.
         */
        final Company rootOrg = TestData.generateCompany(3);
        {
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(rootOrg, System.out);
            marshaller.marshal(rootOrg, testFile);
        }

        /*
         * Okay, now let us unmarshal data from the XML file back to new
         * instances of POJOs.
         */
        {
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Company readOrg = (Company) unmarshaller.unmarshal(testFile);

            /*
             * Test our read data. Compare org units, for employees we are
             * satisfied with the same count ;) We do not rely on same orders,
             * so comparing employees would be a bit more complicated...
             */
            assertEquals(rootOrg.getOrganisations().get(0).getName(),
                    readOrg.getOrganisations().get(0).getName());
        }
    }

    /**
     * Generates an XML Schema (XSD) from the POJOs. In real life, you normally
     * do not want to do this but use an already existing XSD or create one from
     * scratch and then generate POJOs from that using a Maven plugin.
     *
     * @throws Exception
     */
    @Test
    public void createSchemaFromBeans() throws Exception {
        final List<DOMResult> domTree = new ArrayList<>();

        JAXBContext context = JAXBContext.newInstance(Company.class);
        context.generateSchema(
                new SchemaOutputResolver() {
                    @Override
                    public Result createOutput(String namespaceUri, String fileName) throws IOException {
                        DOMResult result = new DOMResult();
                        result.setSystemId(fileName);
                        domTree.add(result);
                        return result;
                    }
                }
        );

        // Output generated XSD to STDOUT...
        TransformerFactory factory = TransformerFactory.newInstance();

        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        StringWriter writer = new StringWriter();
        Result result = new StreamResult(writer);
        Source source = new DOMSource(domTree.get(0).getNode());

        transformer.transform(source, result);

        writer.close();
        System.out.println(writer.toString());
    }

    /**
     * This example takes en existing XML file, unmarshals it to generated model
     * classes (based on the XSD). It does so by referencing the XSD Schema, so,
     * if anything is not valid, you will receive a {@link UnmarshalException}.
     *
     * @throws Exception
     */
    @Test
    public void readXmlFileAndSchemaValidate() throws Exception {
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        /*
         * Specify source XML file and XSD Schema file.
         */
        LOG.info("Validating XML file against schema...");
        final File xsdFile = new File(getClass().getResource("/org-schema.xsd").getFile());
        final File xmlFile = new File(getClass().getResource("/example-data.xml").getFile());
        LOG.info("Using XSD Schema: " + xsdFile.getAbsolutePath());
        LOG.info("Using XML file: " + xsdFile.getAbsolutePath());

        /*
         * Unmarshal file with reference to XSD.
         */
        final Schema schema = sf.newSchema(xsdFile);
        JAXBContext jc = JAXBContext.newInstance(Company.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        unmarshaller.setSchema(schema);
        final Company company = (Company) unmarshaller.unmarshal(xmlFile);

        // You will most likely not reach this code in case of issues due to UnmarshallException...
        assertNotNull(company);
        final Organisation org = company.getOrganisations().get(0);
        assertEquals(false, org.getEmployees().isEmpty());

        LOG.info("Found organisation: \'" + org.getName() + "\' with employees:");
        org.getEmployees().stream().forEach((e) -> {
            LOG.info("   " + e.getFirstName() + " " + e.getName() + ", " + e.getBirthDate()
                    + ", external: " + e.isExternal());
        });

        LOG.info("Looks good, seems that XML data is schema valid.");
    }
}
