package de.huepattl.playground.jaxb;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Just generate some test data for our XML tests. It generates our custom
 * entities and generated ones.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-02-02
 */
public class TestData {

    /**
     * List of first names.
     */
    private static final String[] firstNames = {"Jane", "John", "Neo", "Sid", "Mary-Jane", "Sarah", "Michael", "Tim", "Christina", "Mitch", "Jennifer", "Frank", "Joe", "Anna", "Walter", "Bart", "Lisa", "Herbert", "Bruce", "Edward", "Chris", "Mel", "Jean", "Maria", "Rob", "Simon", "Hans", "Gretel"};

    /**
     * List of last names.
     */
    private static final String[] lastNames = {"Miller", "Doe", "Bosch", "Vicious", "Smith", "Buchanan", "Stuart", "von Billerbeck", "Berg", "Milhouse", "Simpson", "Black", "White", "Anna", "Pink", "Schulz", "Macmillian", "Gruber", "Grosse", "Robertson"};

    /**
     * Creates a company with a single organisation with the given number of
     * employees.
     *
     * @param numEmployees Number of random employees to add.
     *
     * @return Company with 1 org and passed number of employees.
     */
    public static Company generateCompany(int numEmployees) {
        Company root = new Company();

        {
            Organisation org = new Organisation();
            org.setName("someOrg");
            org.setEmployees(generateEmployees(numEmployees));
            root.getOrganisations().add(org);
        }

        return root;
    }

    /**
     * Creates a list of randome employees.
     *
     * @param number Number of employees to create.
     *
     * @return List of employees, never NULL.
     */
    private static List<Employee> generateEmployees(int number) {
        List<Employee> list = new ArrayList<>();

        Random r = new Random();

        Date b;
        for (int i = 0; i < number; i++) {
            Employee e = new Employee();
            e.setId(i);
            e.setName(lastNames[r.nextInt(lastNames.length)]);
            e.setFirstName(firstNames[r.nextInt(firstNames.length)]);
            b = new Date(java.sql.Date.valueOf(LocalDate.of(
                    r.nextInt(100) + 1900, r.nextInt(12) + 1, r.nextInt(28) + 1)).getTime());
            e.setBirthDate(b);
            e.setExternal(r.nextBoolean());
            // Always add two emails per employee...
            e.getEmails().add(e.getName().replaceAll(" ", "-") + "@company.example");
            e.getEmails().add(e.getFirstName().replaceAll(" ", "-") + "@private.example");
            list.add(e);
        }

        return list;
    }

}
