package de.huepattl.playground.jaxb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

/**
 * Demonstrates how to use JAXB streaming features. Using streaming, we can do
 * for example:
 * <p>
 * <ul>
 * <li>Write huge XML documents by using the comfort og JAXB but no need to have
 * everythinf in memory.</li>
 * <li>Read huge XML documents (or parts of it), also without having to read it
 * into memory at once but having the comfort of JAXB unmarshalling.</li>
 * </ul>
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-02-02
 */
public class StreamingTest {

    private static final Logger LOG = LogManager.getLogger(StreamingTest.class.getName());

    /**
     * JUnit takes care to clean and delete that folder after test execution.
     */
    @Rule
    public TemporaryFolder temp = new TemporaryFolder();

    /**
     * This creates some test objects (custom, annotated XML entities) in memory
     * and persists them to XML via streaming. To see file contents, just set a
     * breakpoint at the end of the test method and open the file e.g. with
     * xmllint.
     *
     * @throws Exception
     */
    @Test
    public void testStreamWriting() throws Exception {

        Marshaller m;
        XMLStreamWriter streamWriter;

        LOG.info("Let the streaming begin!");

        JAXBContext context = JAXBContext.newInstance(Employee.class);
        m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

        // Start document
        File testFile = temp.newFile("test.xml");
        LOG.info("Using test file: " + testFile);
        streamWriter = XMLOutputFactory.newFactory().createXMLStreamWriter(new FileOutputStream(testFile));
        streamWriter.writeStartDocument(); // <?xml version="1.0" ...?>
        streamWriter.writeStartElement("org"); // <org>

        /*
         * Write XML body. We generate 100 org units with 500 random employees
         * each.
         */
        for (int i = 0; i < 100; i++) {
            LOG.info("Handling org unit " + (i + 1) + "/100");
            Company company = TestData.generateCompany(500);
            for (final Employee current : company.getOrganisations().get(0).getEmployees()) {
                LOG.debug("-> Write XML: " + current);

                JAXBElement<Employee> ele = new JAXBElement<>(
                        QName.valueOf("employee"), Employee.class, current);
                m.marshal(ele, streamWriter);
            }
        }

        // End document
        streamWriter.writeEndElement(); // </org>
        streamWriter.writeEndDocument();
        streamWriter.close();
    }

    /**
     * This example shows how we stream an XML document using StAX. Only for
     * matching elements (we filter for {@link Employee} only), the JAXB
     * unmarshaller receives work to do.
     *
     * @throws Exception
     */
    @Test
    public void testStreamReading() throws Exception {

        final File xmlFile = new File(getClass().getResource("/example-data.xml").getFile());
        final JAXBContext context = JAXBContext.newInstance(Employee.class);
        final Unmarshaller um = context.createUnmarshaller();

        final XMLStreamReader streamReader = XMLInputFactory.newFactory()
                .createXMLStreamReader(new FileInputStream(xmlFile));

        LOG.info("Unmarshalled Employee entities via JAXB/StAX streaming:");

        /*
         * Stream source XML...
         */
        while (streamReader.hasNext()) {
            /*
             * Only react when <person... start elemnt occurs. Employee
             * representation in XML is person due to the redefinition in
             * Organsiation class via @XmlElement.
             */
            if (streamReader.isStartElement()
                    && "person".equals(streamReader.getLocalName())) {

                /*
                 * Fine, matched <person> - just unmarshal it directly! The
                 * unmarshaller acts on the stream position as advanced by
                 * stream reader's following call to next()
                 */
                final JAXBElement jaxbEmployee = um.unmarshal(streamReader, Employee.class);
                final Employee e = (Employee) jaxbEmployee.getValue();
                LOG.info("   " + e);
            }

            streamReader.next();
        }
        streamReader.close();
    }

}
