function callMeA() {
    print('Invoked function callMeA');
    return 1+1;
}

function callMeB(x) {
    print('Invoked function callMeB with value ' + x);
    return x;
}