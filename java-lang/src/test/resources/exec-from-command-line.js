#!/usr/bin/env jjs

/*
 Execute scrip from a shell, e.g. give scrip exec permissions (chmod +x ./exec-from-command-line.js)
 and run it by just typing:

    ./exec-from-command-line.js -- Otto

 Reference: http://www.infoq.com/articles/nashorn
 Note: requires Java8, otherwise you get an error.
 */

var name = $ARG[0];
print(name ? "Hello, ${name}!" : "Hello, world!");
