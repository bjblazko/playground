

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

import javax.script.*;

/**
 * Examples for using Java 8 Nashorn JavaScript engine embedded into JRE application.
 *
 * @author blazko
 * @see <a href="https://docs.oracle.com/javase/8/docs/technotes/guides/scripting/prog_guide/javascript.html">Java in JavaScript</a>
 * @since 2015-12-28
 */

public class NashornJavaScriptTest {

    public class Pojo {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Pojo{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    private static ScriptEngine js;

    @BeforeClass
    public static void setup() throws ScriptException {
        js = new ScriptEngineManager().getEngineByName("nashorn"); // of course, can give you other engines than Nashorn
        js.eval(""); // warmup
    }

    @Test
    public void helloNashorn() throws ScriptException {
        js.eval("print('Hello World!')");
    }

    @Test
    public void helloBindings() throws ScriptException {
        SimpleBindings bindings = new SimpleBindings();
        bindings.put("user", System.getProperty("user.name"));
        js.eval("print('Hello, ' + user + '!')", bindings);
    }

    @Test
    public void helloExternalScript() throws ScriptException {
        js.eval("load('src/test/resources/hello.js');");
    }

    @Test
    public void modify() throws ScriptException {
        final String oldName = "Franz";

        Pojo pojo = new Pojo();
        pojo.setName(oldName);

        SimpleBindings bindings = new SimpleBindings();
        bindings.put("pojo", pojo);

        js.eval("load('src/test/resources/interact.js');", bindings);

        assertNotEquals(oldName, pojo.getName());
    }

    @Test
    public void cachedScript() throws ScriptException {
        CompiledScript compiled = ((Compilable) js).compile("print('Compiled Hello')");
        compiled.eval();
    }

    @Test
    public void poorCachingBenchmark() throws ScriptException {
        long start = 0;
        long end = 0;

        start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            Bindings b = new SimpleBindings();
            b.put("time", System.currentTimeMillis());
            js.eval("print(time)", b);
        }
        end = System.currentTimeMillis();
        System.out.println(end - start);

        CompiledScript compiled = ((Compilable) js).compile("print(time)");
        compiled.eval();

        start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            Bindings b = new SimpleBindings();
            b.put("time", System.currentTimeMillis());
            compiled.eval(b);
        }
        end = System.currentTimeMillis();
        System.out.println(end - start);

    }

    @Test
    public void callFunctions() throws ScriptException, NoSuchMethodException {
        final String param1 = "Hello";

        js.eval("load('src/test/resources/functions.js');");

        Invocable invocable = (Invocable) js;

        Object result1 = (Integer) invocable.invokeFunction("callMeA");
        assertEquals(2, result1);

        Object result2 = (String) invocable.invokeFunction("callMeB", param1);
        assertEquals(param1, result2);
    }

}