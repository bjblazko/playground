Project documentation is realised by a Maven site, which is located here in the ./site folder.
Just point a web browser to <project root>/target/deploy/site/index.html after having built
the project (see below).

JavaDocs are found in "Project Reports" from the according module navigation menu. Please note that
after initial download, you will have no Maven site generated and thus no JavaDocs. So, after each
project update or initial set-up, please build the Maven site by calling:

    mvn site:site site:deploy

from the root directory of this project.