import de.huepattl.playground.validation.Employee;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.ConstraintViolation;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.sql.Date;
import java.util.Set;

/**
 * Abstract test class providing some common methods, such as logging the validation messages.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-02-05
 */
public abstract class AbstractValidationTest<T> {

    private static final Logger LOG = LogManager.getLogger(AbstractValidationTest.class);

    /**
     * Just create and return a bean that is valid according to its constraints defined in the annotations of the bean
     * itself.
     *
     * @return Valid bean.
     */
    public Employee newValidBean() {
        Employee bean = new Employee();
        {
            bean.setId(3);
            bean.setSalary(new BigDecimal("12.34"));
            bean.setActive(true);
            bean.setLastName("abc");
            bean.setFirstName("s");
            bean.setDateOfBirth(Date.valueOf(LocalDate.of(1977, Month.DECEMBER, 9)));
            bean.setStricterValueThanOthers("ABCDE");
            bean.setUsingSeverity("ABCDE");
            bean.setCheckedByCustomAnnotation("ThisStringMustContainTheNumber3");
        }
        return bean;
    }

    /**
     * Just iterates all messages passed and shows them (log/console).
     *
     * @param messages Validation messages to print/log.
     */
    protected void printValidationMessages(Set<ConstraintViolation<T>> messages) {
        messages.stream().forEach((message) -> {
            LOG.info("[@" + message.getConstraintDescriptor().getAnnotation().annotationType().getSimpleName() + "] " +
                    message.getRootBeanClass().getSimpleName() + "#" + message.getPropertyPath() + " (value: "
                    + message.getInvalidValue() + "): " + message.getMessage());
        });
    }

}
