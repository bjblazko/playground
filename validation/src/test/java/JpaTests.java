import de.huepattl.playground.validation.Employee;
import de.huepattl.playground.validation.JpaService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;

/**
 * Database tests to show that Bean Validation also works on storage layer even when not using a validator
 * directly.
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-02-28
 */
@RunWith(Arquillian.class)
public class JpaTests extends AbstractValidationTest {

    private static final Logger LOG = LogManager.getLogger(JpaTests.class);

    @Inject
    JpaService service;

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClass(Employee.class).addClass(JpaService.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsManifestResource("META-INF/persistence-test.xml", "persistence.xml");
        return jar;
    }

    /**
     * It seems that EclipseLink basically works with Bean Validation but does not creat the DB schema
     * accordingly? At least it seems that string lengths are not considere...
     * TODO: Verify and compare w/ Hibernate.
     */
    @Ignore
    public void schema() {
        // Vendor-independent way of retrieving table meta-data?
    }

    /**
     * Persist a valid bean, we do not expect any trouble here...
     *
     * @throws Exception BAM!
     */
    @Test
    public void persistValid() throws Exception {
        final int ID = 1;
        Employee employee = newValidBean();
        employee.setId(ID);

        service.persist(employee);
        employee = service.retrieve(ID);

        assertNotNull(employee);
    }

    /**
     * Create an invalid employee bean and try to persist it. We do not call explicit validation on the bean, but we
     * expect a validation exception because the persistence layer will do so for us.
     *
     * @throws Exception BAM!
     */
    @Test
    public void persistInvalid() throws Exception {
        final int ID = 1;

        Employee employee = newValidBean();
        employee.setId(ID);
        employee.setFirstName("!@#$%ˆ*(");
        employee.setLastName("!");

        try {
            service.persist(employee);
        } catch (ConstraintViolationException | PersistenceException | EJBException ok) {
            // FIXME: Calling logger here causes other exception...
            //LOG.info("Caught exception while trying to persist an invalid bean, that was expected. Caught: "
            //        + ok, ok);
        }
    }

}
