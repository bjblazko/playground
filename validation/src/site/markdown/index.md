# Introduction

In the past, validation in Java was left to framework makers and project programmers. Very often, there were
only constraint validators foreseen in UI frameworks - for the core domain and persistence, the developer was responsible
of implementing validation.

With [JSR 303: Java Bean Validation](http://jcp.org/en/jsr/detail?id=303) and
[JSR 349: Java Bean Validation 1.1](http://jcp.org/en/jsr/detail?id=349) we now have an important standard for bean validation
AND design-by-contract which incorporates great on all tier levels - from persistence to user interface.

# Quick feature overview

Java Bean Validation 1.1 has the following explicit features:

* Java Beans are constraint-annotated on property basis. Example annotations, placed on the bean's fields are:
    * `@NotNull`: Indicate that a property never may be NULL.
    * `@Size(min=3, max=255)`: Ensure that a string is of certain length.
    * `@Pattern(regex="[a-zA-Z]*")`: Ensure that a string has certain contents matched against a regular expression, e.g. email.
    * `@Future`/`@Past`: Ensure that a date is in the past or future.
    * `@DecimalMin(value=1, inclusive=true)`/`@DecimalMax(value=200, inclusive=true)`: Ensure that a number is in a certain range.
    * `@AssertTrue`/`@AssertFalse`: Force booleans.
    * `@Digits(integer = 3, fraction = 2)`: For floating point numbers, define the boundaries.
    * some more...
* Possibility to add custom validation annotations.
* Explicitly validate beans programmatically by using a default or custom validator.
* In CDI/EJB, implicitly beans that arrive in ANY custom methods, e.g.: `public void userAdd(@NotNull @Valid userBean);`
* In CDI/EJB, allow method validation - including return values (see above, or:) `public @NotNull String getActiveUserName(@NotNull uname, @Past dateFrom, @Future dateUntil);`
* Allow different validation contexts/scopes by using validation groups (so that a bean validation e.g. has dfferent strictness depending on business context).
* Validation messages are translatable and offer support for parameters and implementation of custom message text resolvers.

However, there are also de-facto features derived from it:
* Many frameworks, such as JSF 2.x, take advantage of validation annotations in your model - no need for duplicated validation code.
* Persistence, e.g. JPA 2.x with Hibernate or EclipseLink, automatically check constraints and can stop persisting invalid entities.
* Also, in JPA, your generated schema uses constraint information from your entities and thus can directly specify column properties (e.g. column character length).
* JAX-RS also pays attention to marhshalled POJOs with valid contents and also, web-methods can be annotated with constraints.

Java Bean Validation was developed for Java SE and Java EE.

# Examples overview

## JUnit based examples

Most of the examples are demonstrated through JUnit tests. The tests use [Arquillian](http://arquillian.org)
to execute the tests into an embedded JEE container.

## Web based examples

There are two examples that you can test by deploying the WAR into a container of choice (tested with Wildfly and Glassfish).

* Automatic validation in JSF including localisation.
* Using REST and exception mappers to present validation messages to the caller.

