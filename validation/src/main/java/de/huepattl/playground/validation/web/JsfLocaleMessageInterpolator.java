package de.huepattl.playground.validation.web;

import java.util.Locale;
import javax.faces.context.FacesContext;
import javax.validation.MessageInterpolator;
import javax.validation.Validation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This is a custom message interpolator responsible for retrieving validation
 * error messages in context of JSF for the annotations not using hard-coded
 * messages. It is NOT REQUIRED for JSF as the default interpolator will do.
 * However, it will (most likely) not be able to access the JSF context and thus
 * does not know what locale to use. So, this message interpolator asks the JSF
 * context about the current locale and then delegates to the framework's
 * default message interpolator in order not to re-invent the wheel again on how
 * to substitute the messages and on how/where to retrieve the messages.<p>
 * This interpolator needs to be registered in {@code validation.xml} using the
 * {@code <message-interpolator />} element.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-03-03
 */
public class JsfLocaleMessageInterpolator implements MessageInterpolator {

    private static final Logger LOG = LogManager.getLogger(
            JsfLocaleMessageInterpolator.class);

    /**
     * This message call retrieves the final message by delegating to the
     * framework default message interpolator, but asking the JSF context first
     * about the locale to use. What locale that would be depends on how this is
     * defined in the application implementation. It might be delegating to
     * Facelet API which is able to investigate the browser locale or you might
     * have custom language selector in the web app which has to actively set
     * the locale.
     *
     * @param messageTemplate Message template text to interpolate.
     * @param context         Interpolation context as passed in by the
     *                        validator.
     *
     * @return Final, localised and parameterised message text.
     */
    @Override
    public String interpolate(String messageTemplate, Context context) {
        /*
         * Get the default interpolator from the validator framework. It will do
         * the grunt work for us...
         */
        MessageInterpolator delegate = Validation.byDefaultProvider()
                .configure().getDefaultMessageInterpolator();

        /*
         * ...except that we need to override the locale (would be the system's
         * one) with the one taken from the JSF context.
         */
        Locale jsfLoc;
        try {
            jsfLoc = FacesContext.getCurrentInstance()
                    .getViewRoot().getLocale();
        } catch (NullPointerException npe) {
            // Maybe I am running in REST context? Use fixed default.
            // TODO: Retrieve locale from REST service...
            jsfLoc = Locale.ENGLISH;
        }

        LOG.info("Interpolating message with locale from the (JSF?) context: "
                + jsfLoc);

        // Call default interpolator but with the overridden locale.
        return delegate.interpolate(messageTemplate, context, jsfLoc);
    }

    /**
     * In JSF context, the requested locale here will be most probably not
     * correct. Thus, this method ignores that locale and delegates to
     * {@link #interpolate(java.lang.String, javax.validation.MessageInterpolator.Context)},
     * which in turn will aks the JSF context.
     *
     * @param messageTemplate Message template text to interpolate.
     * @param context         Interpolation context as passed in by the
     *                        validator.
     * @param locale          Requested locale, IGNORED in favour of JSF
     *                        context.
     *
     * @return Final, localised and parameterised message text.
     */
    @Override
    public String interpolate(String messageTemplate, Context context,
            Locale locale) {
        LOG.info("Ignoring requested locale " + locale.getLanguage()
                + " and using the one from JSF context.");
        return this.interpolate(messageTemplate, context);
    }

}
