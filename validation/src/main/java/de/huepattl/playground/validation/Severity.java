package de.huepattl.playground.validation;

import javax.validation.Payload;

/**
 * It is a trick! JSR-303 and JSr-349 do not support severity levels such as INFO, ERROR and WARNING directly. However,
 * all validation constraints have a generic attribute of type {@link javax.validation.Payload}. We can 'abuse' this
 * property by defining severities like done here and declare them in the bean's constraint annotations.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @see javax.validation.Payload
 * @since 2014-02-16
 */
public class Severity {

    public static class INFO implements Payload { };

    public static class WARNING implements Payload { };

    public static class ERROR implements Payload { };

}
