package de.huepattl.playground.validation.validator;

import javax.validation.MessageInterpolator;
import javax.validation.Validation;
import java.util.Locale;

/**
 * An example of a custom message interpolator. The Java validation default
 * interpolator requires translations to be in ValidationMessages.properties and
 * uses the system default locale. So, if you want to use correct locales e.g.
 * from a web session (according to user) or have you translations in the
 * database, for example, you need custom message interpolators.<p>
 * It is important to know, that as soon as you do not rely on the default
 * interpolator, you need to:
 * <ul>
 * <li>Provide messages of the standard validation annotations on your own!</li>
 * <li>Take care of dealing with EL etc.</li>
 * </ul>
 * This custom interpolator is cheating, however. It requires a reference to the
 * default validation message interpolator in the constructor and just
 * internally delegates to the default implementation. The only thing THIS
 * interpolator could do is to retrieve the required locale (e.g. from a servlet
 * filter or {@link
 * java.lang.ThreadLocal}) and tell the default interpolator to use that locale.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-02-28
 */
public class CustomMessageInterpolator implements MessageInterpolator {

    private MessageInterpolator defaultInterpolator;
    private Locale forceLocale;

    public CustomMessageInterpolator() {
        /*
         * FIXME - maybe this is a bug...but how to do it? vaidation.xml seems not to offer me any means of using
         * constructor args. And CDI? Is there a way to retrieve the default message interpolator?
         */
        this.defaultInterpolator = Validation.buildDefaultValidatorFactory().getMessageInterpolator();
    }

    public CustomMessageInterpolator(MessageInterpolator mi) {
        this.defaultInterpolator = mi;
    }

    @Override
    public String interpolate(String messageTemplate, Context context) {

        if (forceLocale == null) {
            forceLocale = Locale.getDefault(); // Could be taken from ThreadLocal, web container, etc.
        }
        return defaultInterpolator.interpolate(messageTemplate, context,
                forceLocale);
    }

    @Override
    public String interpolate(String messageTemplate, Context context,
            Locale locale) {
        if (forceLocale != null) {
            return this.interpolate(messageTemplate, context);
        } else {
            return defaultInterpolator.interpolate(messageTemplate, context,
                    locale);
        }
    }

    public void setForceLocale(Locale l) {
        this.forceLocale = l;
    }
}
