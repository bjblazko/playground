package de.huepattl.playground.validation.web;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Path;
import javax.ws.rs.core.Application;

/**
 * This is just a marker class enabling the WAR to also play REST using JAX-RS.
 * By annotating it with {@link ApplicationPath}, we specify the REST basis path
 * underneath the context root. If for example the WAR context root is
 * validation, the REST basis URL might be
 * <a href="http://localhost:8080/validation/rest"></a>, because we specified the
 * REST application path to be /rest.<p>
 * The REST services (or better: resources) declare themselves using
 * {@link Path}, see for example {@link EmployeeRestResource}.<p>
 * For testing, you might use a web browser or even better REST client apps (or
 * REST browser plugins) or tools like wget.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-03-05
 * @see EmployeeRestResource
 */
@ApplicationPath("/rest")
public class RestApplication extends Application {

}
