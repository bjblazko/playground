package de.huepattl.playground.validation.web;

import de.huepattl.playground.validation.CdiValidatingService;
import de.huepattl.playground.validation.Employee;
import java.sql.Date;
import java.time.LocalDate;
import java.time.Month;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Our REST service, or better, REST resource. Together with the base path
 * declared in {@link RestApplication}, append the path from the {@link Path}
 * anootation to interact with this resource using a REST client such as a
 * browser, a browser REST plugin or wget, for example.<p>
 * Currently, this implementation supports JSON<p>
 * By using the {@link Valid} annotation, JSR 349 Bean Validation is applied
 * automatically.<p>
 * If any validation error occurs, JAX-RS would normaly return an HTTP error
 * code (e.g. 500) to the client with no more usable details. By implementing
 * {@link ExceptionMapper} such as {@link ValidationExceptionResponseMapper} you
 * can provide custom responses per exception class containing additional error
 * information, e.g. ur validation messages.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-03-05
 *
 * @see RestApplication
 * @see Valid
 * @see ValidationExceptionResponseMapper
 */
@Path("/employee")
@Stateless
public class EmployeeRestResource {

    private static final Logger LOG = LogManager.getLogger(
            EmployeeRestResource.class);

    @Inject
    CdiValidatingService service;

    /**
     * This is a simple echo replying with a plain text message as received via
     * the {@code say} query parameter.<p>
     * Please note that again we use {@link Path} annotation here in addition to
     * the one declared on class level. This way, we define a sub-path by not
     * preceeding it with a slash. So, we have the (example )URL <a
     * href="http://localhost:8080/validation/employee/echo?say=Heyho!"></a>
     *
     * @param echoString They string to echo, coming from the {@code say} query
     *                   parameter. It may not be empty and must be at least
     *                   three chars long.
     *
     * @return The same value that came in via the incoming parameter.
     */
    @Path("echo")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String echo(@QueryParam("say") @NotNull @Size(min = 3) final String echoString) {
        LOG.info("HTTP request matching echo()");
        return "Echo: >>>" + echoString + "<<<";
    }

    /**
     * Returns an example employee. In real life, you would add an ID argument
     * to specify the employee resource to retrieve. The name of the method is
     * not important (only the HTTP method, here GET, defines the method chosen
     * by JAX-RS depending on the incoming request).
     *
     * @return Employee object marshalled to the representation as supported by
     *         this class (see {@link Produces} and defined by supported types
     *         of the client as indicated by the HTTP header sent, e.g. JSON.
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public //@Valid FIXME: Getting HV000041: Call to TraversableResolver.isReachable() threw an exception' even when removing JPA annotations from Employee... also, added a dummy persistence unit...
            Employee get() {
        LOG.info("HTTP request matching get()");
        Employee emp = new Employee();
        emp.setActive(true);
        emp.setFirstName("First");
        emp.setLastName("Last");
        emp.setDateOfBirth(Date.valueOf(LocalDate.of(2001, Month.APRIL, 19)));

        return emp;
    }

    /**
     * Method accepting any employee representation as supported by
     * {@link Consumes}, e.g. JSON. {@link Employee} properties just have to
     * match the incoming JSON elements, for example:      <code>
     * {
     * "active": true,
     * "dateOfBirth": "1970-01-01T06:33:20",
     * "firstName": "First",
     * "id": 0,
     * "lastName": "Last"
     * }
     * </code> The unmarshalling is applied by the JAX-RS framework. By using
     * {@link Valid}, it also ensures that Java Bean validation is implicitly
     * performed based on the constraint annotations from {@link Employee} - if
     * we have validation errors, we send a custom error response with
     * validation errors by using {@link ValidationExceptionResponseMapper}.
     *
     * @param employee Employee instance provided by JAX-RS, so unmarshalled
     *                 e.g. from the JSON representation.
     *
     * @return HTTP response.
     * @see ValidationExceptionResponseMapper
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response validateEmployee(final Employee employee) {
        LOG.info("Received employee via REST: " + employee);

        /*
         * Our service contains the @Valid annotation. Having the @Valid here
         * did not work for EntityMapper in Wildfly and Glassfish by the time of
         * writing this but seems to work when adding Jersey+Jersey-Validation
         * manually.
         */
        service.doSomething(employee);

        return Response.status(Response.Status.CREATED).entity("string").build();
    }

}
