package de.huepattl.playground.validation;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * This is just a service for showing that validation also takes place when
 * persisting validation-annotated entities even when not explicitly validating
 * it. We cannot do so directly in a test because we need a transaction boundary
 * - so, this service.
 *
 * @since 2014-03-03
 * @author Timo Boewing (bjblazko@gmail.com)
 */
@Named
@Stateless
public class JpaService {

    /**
     * Only required for our JPA tests.
     */
    @PersistenceContext
    EntityManager entityManager;

    /**
     * This is just for our JPA tests to show that the persistence
     * implementation (should) validate enties that are annotated with
     * validation constraints.
     *
     * @param e Employee entity.
     */
    public void persist(Employee e) {
        entityManager.persist(e);
        entityManager.flush();
    }

    /**
     * Retrieve the entity by identifying it with the ID defined during
     * {@link #persist(de.huepattl.playground.validation.Employee)}.
     *
     * @param id Identifier used durng persisting ({@link Employee#id}).
     * @return Employee instance from the database.
     */
    public Employee retrieve(final int id) {
        return entityManager.find(Employee.class, id);
    }

}
