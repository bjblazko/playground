package de.huepattl.playground.validation.annotation;

import de.huepattl.playground.validation.validator.ContainsValidator;

import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * This is a custom constraint validation annotation that you can use just like the default ones (e.g. {@link
 * javax.validation.constraints.NotNull}, {@link javax.validation.constraints.Size} etc.). The annotation itself does
 * not validate but points to a validator implementation doing the actual work - this is referenced by below's {@link
 * javax.validation.Constraint#validatedBy()}.<p>
 * The elements listed in {@link java.lang.annotation.Target} define where this annotation may be used in:
 * <ul>
 * <li>FIELD: A bean's field or according getter/setter.</li>
 * <li>METHOD: A method argument, e.g. of a CDI-managed service.</li>
 * <li>ANNOTATION_TYPE: Within other annotations.</li>
 * </ul>
 * Example usage {@code @Contains(value="foo") private String name;}
 *
 * @author Timo Boewing
 * @see de.huepattl.playground.validation.validator.ContainsValidator
 * @since 2014-02-20
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE}) // Allowed for method args, bean fields or within other annotations.
@Retention(RUNTIME) // Validation takes place during runtime.
@Constraint(validatedBy = ContainsValidator.class) // Refers to the validator implementation!!!
@Documented // Allows that JavaDoc for code using our annotation actually can state this annotation ;)
public @interface Contains {

    /**
     * Specifies the error message when the validation evaluates to false/invalid. You can specify the message string
     * directly or, when enclosed in curly braces, define the key name from an entry in file
     * src/main/resources/ValidationMessages.properties (given by the JSR).<p>
     * Please have a look at the message there, you see that the message value placeholders in order to receive
     * runtime values as passed by the annotation. In this case, the message can retrieve our {@link #value()}
     * value.
     *
     * @return Message to return when value is invalid.
     */
    String message() default "{de.huepattl.playground.validation.annotationmessage.contains}";

    /**
     * Support validation groups, the validator implementation should check this.
     *
     * @return List of groups defined.
     */
    Class<?>[] groups() default {};

    /**
     * Contains references to optional payload passed when using the annotation.
     *
     * @return List of payloads.
     */
    Class<? extends Payload>[] payload() default {};

    /**
     * Our constraint should check to contain a string as defined by this property. It will be retrieved from {@link
     * de.huepattl.playground.validation.validator.ContainsValidator} and will also be substituted in our validation
     * message. Please check ValidationMessages.properties - our message ({@link #message()}) value '${value}' -
     * reflecting this... value.
     *
     * @return Definition of what the validated string should contain.
     */
    String value();
}
