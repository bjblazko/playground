package de.huepattl.playground.validation.web;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Response;

/**
 * This is just a DTO used to represent our validation messages remotely via
 * REST. Here, this is just a wrapper for a {@link List} of strings because directly
 * providing a collection to {@link Response} for marshalling does not work. So,
 * this is just a JAXB entity to be marshalled into JSON or XML in context of JAX-RS.<p>
 * In reality, you would want to make this a much more structured type
 * containing the validated bean name, property etc.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-03-13
 * @see ValidationExceptionResponseMapper
 */
public class RemoteValidationMessage {

    private List<String> messages;

    public RemoteValidationMessage() {
        messages = new ArrayList<>();
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public void addMessage(final String msg) {
        this.messages.add(msg);
    }

}
