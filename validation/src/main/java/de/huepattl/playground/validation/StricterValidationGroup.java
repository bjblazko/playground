package de.huepattl.playground.validation;

/**
 * Validation group defining bean properties being subject for stricter validation. By default, constraint annotations
 * define the implicit default group. However, in real life you want to define different validation scopes depending on
 * your business context. For example you might want your users to persist a bean when only having poor data quality.
 * However, in a later process step the data needs to be 100%. For this purposes, you can define several different
 * groups and specify them in the validator whether they have to apply or not.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-02-16
 */
public interface StricterValidationGroup {
}
