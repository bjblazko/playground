package de.huepattl.playground.validation.web;

import javax.ejb.EJBTransactionRolledbackException;
import javax.enterprise.context.ApplicationScoped;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * When you encounter exceptions in REST service methods, you might of course
 * suffer from an exception. In such cases, you might not want to live with a
 * HTTP 500 or so error but want to define another HTTP error code and
 * especially present details to the client. In our example, we want not a
 * standard HTTP 500 error page but present validation errors via JSON or XML to
 * the client. With JAX-RS, the standard way to do so is to register
 * {@link ExceptionMapper} classes.<p>
 * These mappers allow to provide a custom {@link Response} by giving us the
 * chance to build a custom JAXB serialisable class containing the errors/ The
 * thrown original exception, in this case the {@link ValidationException}, is
 * passed in. Besides implementing {@link ExceptionMapper}, please do not forget
 * to mark this class with {@link Provider} in order to be dicoverable by
 * JAX-RS.<p>
 * FIXME: This is not invoked when running in Glassfish 4.0 FIXME: Wildly also
 * not works as it pases in a ReastEasy exception!
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-03-13
 * @see RemoteValidationMessage
 */
@Provider
@ApplicationScoped
public class ValidationExceptionResponseMapper implements ExceptionMapper<Throwable> {

    private static final Logger LOG = LogManager.getLogger(
            ValidationExceptionResponseMapper.class);

    /**
     * Inject HTTP headers... whoooohoooo!
     */
    @Context
    private HttpHeaders headers;

    /**
     * Whenever a REST service method throws a
     * {@link ConstraintViolationException}, this handler method is invoked to
     * avoid the standard error response and replace it with a custom response -
     * optionally including JSON/XML body by providing a JAXB-compatible entity
     * to be marshalled into the response.
     *
     * @param exception Exception providing details.
     *
     * @return Custom response with HTTP error code and HTTP body.
     */
    @Override
    public Response toResponse(Throwable exception) {
        LOG.error("Received an exception (" + exception.getClass().getSimpleName()
                + "), will add to REST response: " + exception);
        if (exception instanceof ConstraintViolation) {
            return this.handleConstraintViolation(exception);
        } else if (exception instanceof ValidationException) {
            LOG.error("Caught ValidationException: " + exception.getMessage());
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Error: " + exception.getMessage()).build();
        } else if (exception instanceof EJBTransactionRolledbackException) { //&& (exception.getCause() instanceof ConstraintViolation)) 
            LOG.error("Caught TransactionRolledback (JPA): " + exception.getMessage());
            return handleConstraintViolation(exception.getCause());
        } else {
            LOG.error("Received unexpected exception tzpe, will add to REST response: " + exception);
            return Response.serverError().entity(exception.getMessage()).build();
        }
    }

    /**
     * Just moduled out as it is used twice in
     * {@link #toResponse(java.lang.Throwable)}. It marshalls the details of the
     * constraint vilation exception to an external representation using
     * {@link RemoteValidationMessage}.
     *
     * @param exception Constraint vilation exception.
     *
     * @return HTTP response.
     */
    private Response handleConstraintViolation(final Throwable exception) {
        LOG.error("Received a constraint validation exception, will add to REST response: " + exception);
        final ConstraintViolationException cve = (ConstraintViolationException) exception;
        if (MediaType.TEXT_PLAIN_TYPE.equals(headers.getMediaType())) {
            // Build plain text error response.
            StringBuilder sb = new StringBuilder();

            cve.getConstraintViolations().stream().forEach((cv) -> {
                sb.append(cv.getPropertyPath()).append(": ").append(cv.getMessage()).append("\n");
            });
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity(sb.toString()).build();
        } else {
            // Build response w/ JAXB (so depending on requets type, build JSON or XML)...
            // This object will be marshalled via JAXB to JSON or XML and presented to the client.
            RemoteValidationMessage m = new RemoteValidationMessage();

            cve.getConstraintViolations().stream().forEach((cv) -> {
                m.addMessage(cv.getPropertyPath() + ": " + cv.getMessage());
            });
            m.addMessage(exception.toString());
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity(m)
                    .type(headers.getMediaType()).build();
        }

    }

}
