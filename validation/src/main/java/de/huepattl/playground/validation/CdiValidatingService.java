package de.huepattl.playground.validation;

import de.huepattl.playground.validation.annotation.EqualParams;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.validation.groups.ConvertGroup;
import javax.validation.groups.Default;
import java.util.Collection;
import javax.ejb.Stateless;

/**
 * This service example implicitly performs (bean) validation without the need of wiring it with a dedicated validator.
 * By annotating the method parameters, validation is applied as long as this service is CDI/EJB managed by the
 * container.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-02-12
 */
@Stateless
public class CdiValidatingService {

    private static final Logger LOG = LogManager.getLogger(CdiValidatingService.class);

    /**
     * Container chooses implementation.
     */
    @Inject
    Validator validator;

    /**
     * This service method performas validation as long as this instance is managed by CDI/EJB. Validation is applied
     * implicitly in two ways here:
     * <ul>
     * <li>{@link javax.validation.constraints.NotNull} validates the method argument not being null. This is similar
     * to the classic approach to assert the argument and throw a NPE or {@link java.lang.IllegalArgumentException}.
     * Please note that your will get validation exceptions here instead.</li>
     * <li>{@link javax.validation.Valid} cascades validation to inspect the bean's constraints. So, if the bean is
     * present but invalid regarding the bean constraint definitions, you will get validation exceptions here as
     * well.</li>
     * </ul>
     *
     * @param bean Passed bean that will checked for not being NULL and if it is valid.
     */
    public void doSomething(@NotNull @Valid final Employee bean) {
        LOG.info("I am doing nothing except echoing the bean passed:" + bean);
    }

    /**
     * This method performs implicit validation and additionally validates bean properties being groupd with {@link
     * de.huepattl.playground.validation.StricterValidationGroup}, which normally would not have been validated because
     * {@link javax.validation.Valid} used the default group.<br>
     * The trick is to translate the current validation scope to the one matching the bean annotation using the {@link
     * javax.validation.groups.ConvertGroup} annotation.
     * FIXME if you have a better idea (except using Spring with @Validated...).
     *
     * @param bean POJO to validate.
     */
    public void doSomethingWithGroups(
            @NotNull
            @Valid @ConvertGroup(from = Default.class, to = StricterValidationGroup.class)
            final Employee bean) {
        LOG.info("I am doing nothing except echoing the bean passed:" + bean);
    }

    /**
     * This is similar to {@link #doSomething(Employee)} with the difference on how this works when passing
     * an entire collection of beans being subject to implicit validation.
     *
     * @param beans List of beans.
     */
    public void doSomethingManyBeans(@NotNull @Valid final Collection<Employee> beans) {
        LOG.info("I am doing nothing except echoing the beans passed:");
        for (final Employee bean : beans) {
            LOG.info("   - " + bean);
        }
    }

    /**
     * This method allows testing that when {@code NULL} is returned a validation exception is thrown.
     *
     * @return Contrary to contract, {@code NULL} is returned always.
     */
    public
    @NotNull
    String methodReturningNullByAccident() {
        return null;
    }

    /**
     * Let us pretend that this service method is invoked from the user interface. This method is only successfully
     * invoked if the two email addresses provded by the user are equal. A similar thing is common for passwords.<p>
     * The key is using a custom annotation that points to a validator implementation using the {@link
     * javax.validation.constraintvalidation.SupportedValidationTarget} annotation making this possible.
     *
     * @param email        E-mail address.
     * @param confirmation E-mail address that has to match the first one (confirmation).
     *
     * @see de.huepattl.playground.validation.annotation.EqualParams
     * @see de.huepattl.playground.validation.validator.EqualParamsValidator
     * @see javax.validation.constraintvalidation.SupportedValidationTarget
     */
    @EqualParams
    public void specifyEmail(@NotNull final String email, @NotNull final String confirmation) {
        // just use email - confirmation argument is validated and thus no required.
        LOG.info("You should only see this if provided email addresses equal: " + email + " and " + confirmation);
    }

}
