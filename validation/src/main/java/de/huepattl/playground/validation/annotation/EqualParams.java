package de.huepattl.playground.validation.annotation;

import de.huepattl.playground.validation.validator.EqualParamsValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * This annotation example is maybe less usual. It can be used for method
 * arguments being equal. Most probably, you will only find such cases near user
 * interface code in cases where you directly handle input such as e-mail and
 * e-mail conformation or password and password confirmation. Example usage:
 * {@code @EqualParams public void setPassword(final String password, final String confirmation) ...}
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @see de.huepattl.playground.validation.validator.EqualParamsValidator
 * @since 2014-02-20
 */
@Target({METHOD}) // Allowed for methods.
@Retention(RUNTIME) // Validation takes place during runtime.
@Constraint(validatedBy = EqualParamsValidator.class) // Refers to the validator implementation!!!
@Documented // Allows that JavaDoc for code using our annotation actually can state this annotation ;)
public @interface EqualParams {

    /**
     * Specifies the error message when the validation evaluates to
     * false/invalid. You can specify the message string directly or, when
     * enclosed in curly braces, define the key name from an entry in file
     * src/main/resources/ValidationMessages.properties (given by the JSR)
     * .<p>
     *
     * @return Message to return when value is invalid.
     */
    String message() default "{de.huepattl.playground.validation.annotationmessage.equalparams}";

    /**
     * Support validation groups, the validator implementation should check
     * this.
     *
     * @return List of groups defined.
     */
    Class<?>[] groups() default {};

    /**
     * Contains references to optional payload passed when using the annotation.
     *
     * @return List of payloads.
     */
    Class<? extends Payload>[] payload() default {};

}
