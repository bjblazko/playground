package de.huepattl.playground.validation.validator;

import de.huepattl.playground.validation.annotation.EqualParams;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraintvalidation.SupportedValidationTarget;
import javax.validation.constraintvalidation.ValidationTarget;

/**
 * Demonstrate the rather unusual case where we can validate two (or more)
 * method arguments in relation to each other. A common case for this would be
 * to check password and re-typed one.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @see de.huepattl.playground.validation.annotation.EqualParams
 * @see
 * de.huepattl.playground.validation.CdiValidatingService#specifyEmail(String,
 * String)
 * @since 2014-02-20
 */
@SupportedValidationTarget(value = ValidationTarget.PARAMETERS)
public class EqualParamsValidator implements ConstraintValidator<EqualParams, Object[]> {

    private static final Logger LOG = LogManager.getLogger(
            EqualParamsValidator.class);

    @Override
    public void initialize(EqualParams constraintAnnotation) {
        LOG.info("#initialize() has been invoked.");
    }

    @Override
    public boolean isValid(Object[] values, ConstraintValidatorContext context) {
        boolean valid;

        if (values == null || values.length == 0) {
            valid = false; // This is what @NotNull is responsible for...
        } else {
            // TODO: make this work with all...
            valid = values[0].equals(values[1]);
        }

        LOG.info(
                "#isValid() has been invoked and evaluated to \'" + valid + "\' for values: " + values[0] + " and " + values[1]);
        return valid;
    }

}
