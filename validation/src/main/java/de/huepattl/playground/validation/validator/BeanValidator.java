package de.huepattl.playground.validation.validator;

import de.huepattl.playground.validation.Employee;
import de.huepattl.playground.validation.annotation.BeanConstraints;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validator for the {@link de.huepattl.playground.validation.annotation.BeanConstraints} annotation which acts on
 * class-level and thus allows for cross-properties validation.
 *
 * @author Timo Boewing
 * @see de.huepattl.playground.validation.annotation.BeanConstraints
 * @since 2014-02-21
 */
public class BeanValidator implements ConstraintValidator<BeanConstraints, Object> {
    private static final Logger LOG = LogManager.getLogger(BeanValidator.class);

    /**
     * Not used here.
     */
    @Override
    public void initialize(BeanConstraints constraintAnnotation) {

    }

    /**
     * Performs the validation against the entire bean. Normally, we are used to validate single fields, but the
     * annotation used ({@link de.huepattl.playground.validation.annotation.BeanConstraints}) is placed on class-level.
     * Thus, we can check more than one property of a bean, e.g. if their corelation is fine.
     *
     * @param value   The bean to validate, not a single field etc. like we are used to... NULL is regarded as valid.
     * @param context Validation context passed by the runtime.
     *
     * @return True, when the bean is valid regarding this validation check.
     */
    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        if (value instanceof Employee) {
            Employee bean = (Employee) value;
            if (bean.getLastName() != null
                    && bean.getLastName().equalsIgnoreCase(bean.getFirstName())) {
                // TODO/FIXME: Wee need to specify different error messages if we have more checks here...
                LOG.info("Oops, equal values for stringProperty and stringProperty2: " + bean.getLastName());
                return false;
            }
        } else {
            throw new IllegalArgumentException(
                    "Sorry, I do not know how to validate objects of type "
                            + value.getClass().getCanonicalName());
        }

        return true;
    }
}
