package de.huepattl.playground.validation;

import de.huepattl.playground.validation.annotation.BeanConstraints;
import de.huepattl.playground.validation.annotation.Contains;
import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Just a sample Java Bean (POJO) on that we will apply validation. We just
 * utilise JSR standard constraint annotations that are supported by the
 * standard constraint validator - so checking simple constraints comes for
 * free. When you are reading this in JavaDoc, please have a look in the
 * implementation to see the constraints actually set on field level.<p>
 * In case your beans might enpass a toolkit that enhances the bytecode of it,
 * you should favour annotating your getters instead of the fields, because they
 * are not visible via reflection.<p>
 * See the unit tests on how all this validation stuff works. Besides the JSR
 * document(s), a great place of documentation is the <a
 * href="https://docs.jboss.org/hibernate/validator/5.1/reference/en-US/html/">Hibernate
 * Validator documentation</a>.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-02-02
 */
@Entity // for our JPA demonstrations.
@BeanConstraints // Used to validate more than one single field.
public class Employee implements Serializable {

    /**
     * An integer property that is restricted to values between 1 and 100. If
     * you omit the 'message' the default one from the annotation will be used.
     * However, you can manually override the message with your custom one. In
     * this case, the message is hard-coded and not translatable, but please
     * note that using EL we can retrieve actual values by using the according
     * name, e.g. ${value}.
     */
    @DecimalMin(value = "1", inclusive = true,
            message = "The value must be larger than or equal to ${value}")
    @DecimalMax(value = "100", inclusive = true,
            message = "The value must be less than or equal to ${value}")
    @Id // for JPA.
    private int id;

    /**
     * Boolean: must be TRUE. This time, again, we override the default message.
     * But by using the curly braces, we point to a resource bundle identifier.
     * Please see {@link #dateOfBirth} for more details.
     */
    @AssertTrue(message = "{message.active}")
    private boolean active;

    /**
     * A date: must not be NULL and in the past. Pay attention to the message -
     * again, we override it but by enclosing it in curly braces, the validation
     * runtime tries to look it up using it as the key in the originating
     * resource bundle. By default, this is ValidationMessages.properties.<p>
     * Using a {@link javax.validation.MessageInterpolator}, you have the
     * possibility to implement custom mechanisms of loading messages. This one
     * has to be registered in META-INF/validation.xml or can be specified in
     * the validator factory:      <code>
     * ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
     *
     * Validator validator = validatorFactory.usingContext()
     *    .messageInterpolator( new MyMessageInterpolator() )
     *    .traversableResolver( new MyTraversableResolver() )
     *    .getValidator();
     * </code>
     */
    @NotNull(message = "{message.dateOfBirthNotNull}")
    @Past(message = "{message.dateOfBirthPast}")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;

    /**
     * String #1: Not null, length of 3..50.
     */
    @Size(min = 3, max = 50,
            message = "Last name must have a length between {min} and {max} characters.")
    @NotNull(message = "Please specify a value!")
    private String lastName;

    /**
     * String #2: Not null and must contain a number.
     */
    @Pattern(regexp = "^[a-zA-Z0-9]*$",
            message = "Value may only contain letters and numbers - no blanks or special chars!")
    @NotNull(message = "Please specify a value!")
    private String firstName;

    /**
     * A big decimal that may not be null, can not have lengths of no more than
     * three integers and two fractions. The value itslef is limited to a range
     * between 1.00 and 100.0.
     */
    @Digits(integer = 3, fraction = 2,
            message = "No more than three digits and no more than 2 fractions (e.g. 999.99)!")
    @DecimalMin(value = "1", inclusive = true,
            message = "The value must be larger than or equal to 1")
    @DecimalMax(value = "100", inclusive = true,
            message = "The value must be less than or equal to 100")
    @NotNull(message = "Please specify a value!")
    private BigDecimal salary;

    /**
     * Using groups, you can define that a validation should only apply in a
     * given context. When no group is defined, a default group is assumed.<p>
     * When using a validator explicitly, you can just pass the group to apply
     * to the validate method: {@link
     * javax.validation.Validator#validate(Object, Class[])}. Even when invalid,
     * the validation only applies when the group passed to the validator is
     * defined in th annotation.<p>
     * When using implicit validation in methods of CDI/EJB managed method calls
     * using {@link javax.validation.Valid}, there is no elegant solution except
     * to remap groups using {@link javax.validation.groups.ConvertGroup}, see
     * {@link de.huepattl.playground.validation.CdiValidatingService#doSomethingWithGroups(Employee)}
     * for an example.<p>
     * When having a look at the {@link javax.validation.constraints.Size}
     * annotations below, you will notice that it is possible to use the same
     * annotation with different constraints while using different groups. So,
     * it is possible to distinguish constraints depending on the context by
     * defining he group to use when calling the validator manually
     * ({@link javax.validation.Validator#validate(Object, Class[])}) or using {@link
     * javax.validation.groups.ConvertGroup} when implicitly validating in CDI
     * services together wit the {@link
     * javax.validation.Valid} annotation.<p>
     * The constrant annotation has to support this by having the list property,
     * just have a look at e.g. {@link
     * javax.validation.constraints.Size} source code.
     *
     * @see
     * de.huepattl.playground.validation.CdiValidatingService#doSomethingWithGroups(Employee)
     */
    @NotNull
    @Size.List({
        @Size(min = 1, max = 255),
        @Size(min = 3, max = 5, groups = {StricterValidationGroup.class})
    })
    private String stricterValueThanOthers;

    /**
     * String being between 3 and 5 characters long. When invalid, we use the
     * {@link javax.validation.Payload} to define a custom severity level.
     *
     * @see javax.validation.Payload
     * @see de.huepattl.playground.validation.Severity
     */
    @Size(min = 3, max = 5, payload = Severity.WARNING.class)
    private String usingSeverity;

    /**
     * A custom annotation as defined by
     * {@link de.huepattl.playground.validation.annotation.Contains} and
     * implemented by
     * {@link de.huepattl.playground.validation.validator.ContainsValidator}.
     * The connection between annotation and validator implementation is
     * achieved within the annotation by setting it via {@link
     * javax.validation.Constraint#validatedBy()}
     * .<p>
     * In this example, the annotation requires a string to contain a '3'. When
     * having a look into the annotation source, you will notice that this
     * annotation can also be used for method signatures or annotations.
     *
     * @see de.huepattl.playground.validation.annotation.Contains
     * @see de.huepattl.playground.validation.validator.ContainsValidator
     */
    @Contains(value = "3")
    private String checkedByCustomAnnotation;

    /*
     * Do the usual Java boilerplate code (i.e. setters/getters)...
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public String getStricterValueThanOthers() {
        return stricterValueThanOthers;
    }

    public void setStricterValueThanOthers(String stricterValueThanOthers) {
        this.stricterValueThanOthers = stricterValueThanOthers;
    }

    public String getUsingSeverity() {
        return usingSeverity;
    }

    public void setUsingSeverity(String usingSeverity) {
        this.usingSeverity = usingSeverity;
    }

    public String getCheckedByCustomAnnotation() {
        return checkedByCustomAnnotation;
    }

    public void setCheckedByCustomAnnotation(String checkedByCustomAnnotation) {
        this.checkedByCustomAnnotation = checkedByCustomAnnotation;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", active=" + active + ", dateOfBirth=" + dateOfBirth + ", lastName=" + lastName + ", firstName=" + firstName + ", salary=" + salary + ", stricterValueThanOthers=" + stricterValueThanOthers + ", usingSeverity=" + usingSeverity + ", checkedByCustomAnnotation=" + checkedByCustomAnnotation + '}';
    }
    
}
