package de.huepattl.playground.validation.validator;

import de.huepattl.playground.validation.annotation.Contains;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * This is a custom validator which backs our hand-crafted
 * {@link de.huepattl.playground.validation.annotation.Contains}. When using
 * that annotation, the user does not have to call this validator or even know
 * about it. The annotation tells the runtime what backing validator
 * implementation to use using its {@link
 * javax.validation.Constraint#validatedBy()}
 * .<p>
 * This validator is checking that any string having the
 * {@link de.huepattl.playground.validation.annotation.Contains} contains the
 * substring as defined by
 * {@link de.huepattl.playground.validation.annotation.Contains#value()} -
 * passed via {@link
 * #initialize(de.huepattl.playground.validation.annotation.Contains)}.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @see de.huepattl.playground.validation.annotation.Contains
 * @since 2014-02-20
 */
public class ContainsValidator implements ConstraintValidator<Contains, String> {

    private static final Logger LOG = LogManager.getLogger(
            ContainsValidator.class);

    private String requiredContains;

    /**
     * This is called once and allows the validator to inspect the annotation
     * used, e.g. to check its payload, groups etc. Here, our validator actually
     * gets to know what the annotation wants to constrain for by calling its
     * custom
     * {@link de.huepattl.playground.validation.annotation.Contains#value()}
     * property.
     *
     * @param constraintAnnotation Reference to the calling annotation.
     *
     * @see de.huepattl.playground.validation.annotation.Contains
     * @see de.huepattl.playground.validation.annotation.Contains#value()
     */
    @Override
    public void initialize(Contains constraintAnnotation) {
        LOG.info("#initialize() has been invoked.");
        requiredContains = constraintAnnotation.value();
    }

    /**
     * This method is actually invoked by the validator runtime and performs the
     * constraint validation on the value passed. By default, just returning a
     * boolean result is enough, the runtime is building the {@link
     * javax.validation.ConstraintViolationException} with the
     * {@link javax.validation.ConstraintViolation} message. Only if you need
     * more sophisticated control over building that, you can use {@link
     * javax.validation.ConstraintValidatorContext#buildConstraintViolationWithTemplate(String)}.
     *
     * @param value   The string (as defined by the generic) value
     *                to...validate! Depending on the calling annotation, this
     *                might be a bean field, a method argument etc.
     * @param context Validation context passed by the runtime. May have
     *                additional caps depending on the validation implementation
     *                (e.g. Hibernate Validator etc.).
     *
     * @return True when valid.
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        boolean valid;

        if (value == null) {
            valid = false; // This is what @NotNull is responsible for...
        } else {
            valid = value.contains(this.requiredContains);
        }

        LOG.info(
                "#isValid() has been invoked and evaluated to \'" + valid
                + "\' for value: " + value + " (required contains: "
                + requiredContains + ")");
        return valid;
    }

}
