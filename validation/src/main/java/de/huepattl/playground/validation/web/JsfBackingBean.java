package de.huepattl.playground.validation.web;

import de.huepattl.playground.validation.Employee;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Locale;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Just a form backing bean/service for our JSF example. It is directly
 * available in index.xhtml though the {@link Named} annotation.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-03-03
 */
@Named
@SessionScoped
public class JsfBackingBean implements Serializable {

    private static final Logger LOG = LogManager.getLogger(JsfBackingBean.class);

    private Employee employee;

    /**
     * Specify the current locale, e.g. 'en'. It is propagated to the JSF
     * context.
     *
     * @param locale Locale to use, e.g. 'en'.
     */
    public void setLocale(String locale) {
        if (locale != null) {
            /*
             * Update JSF context with locale. This is checked in
             * JsfLocaleMessageInterpolator.
             */
            UIViewRoot jsfRoot = FacesContext.getCurrentInstance().getViewRoot();
            jsfRoot.setLocale(Locale.forLanguageTag(locale));

            LOG.info("Setting locale: " + jsfRoot.getLocale());
        }
    }

    /**
     * Returns the currently set locale, e.g. 'en'
     *
     * @return Current locale string.
     */
    public String getLocale() {
        return FacesContext.getCurrentInstance().getViewRoot()
                .getLocale().getLanguage();
    }

    /**
     * Returns the current employee bean or creates a new one, if NULL.
     *
     * @return Current employee instance.
     */
    public Employee getEmployee() {
        if (employee == null) {
            employee = new Employee();
            employee.setDateOfBirth(Date.valueOf(LocalDate.MIN));
        }
        return employee;
    }

    /**
     * Specify current employee bean instance.
     *
     * @param employee Employee instance.
     */
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

}
