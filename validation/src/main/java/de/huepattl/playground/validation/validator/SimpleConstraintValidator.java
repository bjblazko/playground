package de.huepattl.playground.validation.validator;

import javax.validation.*;
import java.util.Locale;
import java.util.Set;

/**
 * This validation service internally just uses the default constraint validator. In JEE, this depends on the one
 * shipped with your container. For JSE, we use hibernate validator as defined in the POM.
 * Remember, when you are dealing with CDI services, there is no need to have a dedicated validator like this one
 * but it is sufficient to annotate your normal service methods with {@link javax.validation.Valid}. Just have a look
 * at {@link de.huepattl.playground.validation.CdiValidatingService} for an example.<p>
 * Having a dedicated validation service like this is beneficial if you want to check the validation messages
 * explicitly
 * by evaluating the returned {@link javax.validation.ConstraintViolation}.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @param <T> Type param.
 * @see de.huepattl.playground.validation.CdiValidatingService
 * @since 2014-02-02
 */
public class SimpleConstraintValidator<T> {

    private final ValidatorFactory validatorFactory;

    public SimpleConstraintValidator() {
        // This one will do very well, but...
        //validatorFactory = Validation.buildDefaultValidatorFactory();

        //
        /*
         * ... Here we define a custom interpolator. It receives the default one so it can delegate to it.
         * Tge reason is that our own interpolator will check the 'current' locale. Normally, the interpolate
         * will use the system default locale, which e.g. is not the best approach for web applications
         * supporting mutliple languegs.
         */
        Configuration<?> configuration = Validation.byDefaultProvider().configure();
        MessageInterpolator customInterpolator = new CustomMessageInterpolator(configuration.getDefaultMessageInterpolator());
        ((CustomMessageInterpolator)customInterpolator).setForceLocale(Locale.ENGLISH);
        validatorFactory = configuration.messageInterpolator(customInterpolator).buildValidatorFactory();
    }

    /**
     * Just pass any POJO as an argument. If the bean value constraint annotations (e.g. such as {@link
     * javax.validation.constraints.NotNull}, {@link javax.validation.constraints.Size} etc.) and the according field
     * values violate these definitions, according reports are returned.
     *
     * @param bean Any bean containing constraint annotations to validate.
     *
     * @return Validation reports or empty set.
     * @see #validate(Object, Class[])
     */
    public Set<ConstraintViolation<T>> validate(final T bean) {
        Validator validator = validatorFactory.getValidator();
        return validator.validate(bean);
    }

    /**
     * Same like {@link #validate(Object)} but with additional option to specify zero or more groups. Groups allow to
     * specify another context for validation, such as stricter validation or depending on business process to apply
     * more or less validations.
     *
     * @param <T>    Type param.
     * @param bean   Annotated POJO to validate.
     * @param groups Optional list of groups to apply. Groups are just custom marker interfaces you pass here and also
     *               include in the according constraint annotations. For example, see {@link
     *               de.huepattl.playground.validation.StricterValidationGroup} usage for example.
     *
     * @return Validation reports or empty set.
     * @see #validate(Object)
     */
    public <T> Set<ConstraintViolation<T>> validate(final T bean, Class<?>... groups) {
        Validator validator = validatorFactory.getValidator();
        return validator.validate(bean, groups);
    }
}
