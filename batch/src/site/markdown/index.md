# Introduction

For batch jobs, there never really was a standard in Java. However, there always was a strong demand for
handling batch jobs. One of the first ones to establish a real framework for this
was [Spring Source](http://spring.io/). Together with [Accenture](http://newsroom.accenture.com/article_display.cfm?article_id=4703),
they built a great batch framework that was built on top of the sophisticated
[Spring Framework](http://projects.spring.io/spring-framework/). No surprise, this framework is
called [Spring Batch](http://projects.spring.io/spring-batch/).

In the Java Standards world, however, there still was no comparable thing. Until 2013 - the JCP
presented [JSR 352: Batch Applications for the Java Platform](http://jcp.org/en/jsr/detail?id=352).

JSR-352, led by IBM, is very very close to Spring Batch - but it is important to exist, so that
there a more implementations available for use. However, Spring Batch still is more advanced
in my point of view because of mainly two reasons:

* It offers more and more compatibility to JSR-352 and can thus be used as an implementation of this JSR.
* It offers a nice variety of standard implementation for readers and writers such as for CSV or cursor-based JPA.

But what is batch? It certainly depends on your definition, but my one is:

* A process that needs to be transactional but tends to take some time.
* Typically retrieves data from another system or provides such.
* May be used for sysem-internal calculation runs or data transformation.
* Often requires external integration such as file handling and processing (CSV, XML etc.).
* Typically executed asynchronously.

# How Java Batch works

Java Batch was developed for Java SE and Java EE. If you plan to deploy your batch application
into a JEE 7 container, you already have it. For older ones, pure Servlet containers or a
standalone application, you need to integrate the framework on your own (for example use
Spring Batch with or without JSR-352 features or use the [JSR-352 reference implementation](https://java.net/projects/jbatch/sources/jsr-352-git-repository/show)).

It all begins with XML, still. The main entry point for a job is a XML file that describes the
flow of the job. Per job, you have one XML file, they have to be located in `META-INF/batch-jobs`.

A job is basically comprised of steps, each implementing a logical unit of work. There are two kinds of
steps possible:

* A simple step consisting of a single processing class, called a Batchlet.
* A threefold step consisting of a reader, processor and writer.

More on this later.
Batchlets, readers, processors and writers are classes based on certain interfaces that you have to
implement. There are several ways for the XML to reference the implementations:

* Directly name the implementation with its fully qualified class name. The batch runtime then handles the lifecycle of these.
* Use references (alias names). The mapping between class name and alias is realised in optional file `META-INF/batch.xml`, lifecycle still is maintained by batch framework.
* Use CDI injection when having CDI available (e.g. JEE container). Use the @Named annotations in the classes and refer to that name in XML.

Here a simple example XML to get a feeling of a simple job that read does the following:

* Step 1: A batchlet that provides a file (e.g. by fetching it via FTP etc. from a remote system).
* Step 2: Process the data:
    * A CSV reader reads one record after another from the provided CSV file. Each record is mapped to a custom POJO.
    * A processor receives one POJO after another form the reader and performs some actions on it, e.g. validate.
    * A writer persists the records to another location, for example as a local XML file.

```xml
<?xml version="1.0" encoding="UTF-8"?>

<job id="simplejob" xmlns="http://xmlns.jcp.org/xml/ns/javaee" version="1.0">

    <step id="getData" next="process">
        <batchlet ref="example.batch.RetrieveBatchlet">
            <properties>
                <property name="source" value="ftp://example.org/pub/employees.csv"/>
                <property name="dest" value="/tmp/employees.csv"/>
            </properties>
        </batchlet>
    </step>

    <step id="process">
        <chunk checkpoint-policy="item" item-count="20">
            <reader ref="example.batch.CsvFileReader">
                <properties>
                    <property name="fileName" value="/tmp/employees.csv"/>
                </properties>
            </reader>

            <processor ref="example.batch.ItemValidationProcessor"/>

            <writer ref="example.batch.XmlFileWriter" >
                <properties>
                    <property name="fileName" value="/tmp/employees.xml"/>
                </properties>
            </writer>
        </chunk>
    </step>
</job>
```

This example is not very sophisticated but I think it nicely shows a simple usecase. The XML allows
for additionally defining how to deal with errors, define custom flow, parallelisation etc. and also
provides dynamical values (e.g. external parameters instead of hardcoded values such as the filenames here
via the Job Specification Language (JSL).

The `ref`s above point to Java class names in this case but may also point to CDI names from the `@Named`
annotation, for example.

# Known issues

* There is most probably a bug in `JpaPagingReader.java`... cannot find. Maybe that thing should be done from scratch.
* This documentation: find out why TOC and enumerated headings are not working.

# Missing features/examples

This is more a reminder list, because there are still tons of other features missing.

* Advanced showcases for partition mapper and decider.
* Show if and how we can start a job on incoming web service messages.
* Implement checkpoint examples.
* ...?

# Examples overview

Basically, the best starting point for understanding is to start looking at the several job XML
files located in `META-INF/batch-jobs/`. You will find how to execute the according job in
the chapters below.

## JUnit based examples

Most of the examples are demonstrated through JUnit tests. The tests use [Arquillian](http://arquillian.org)
to execute the tests into an embedded JEE container.

Example | Job XML | JUnit test | Shows | Keywords
--- | --- | --- | --- | ---
[Simple](#job_simple) | `simple.job.xml` | `SimpleBatchJobTest: simpleJob()` | Quite the most basic job showing <ul><li>Batchlet (single step with single-class logic)</li><li>ItemReader reading from a CSV file and maps to POJO.</li><li>ItemProcessor receiving POJO from reader and doing some simple stuff on the data.</li><li>ItemWriter writing the POJOs as XML to another file.</li></ul> | simple, Batchlet, ItemReader, ItemProcessor, ItemWriter, reader, processor, writer, CSV, XML
[JPA paged reader](#job_jpa) | `justReadAndWrite.job.xml` | `ComplexBatchJobTest: testJustReadAndWrite()` | Very simple job that generates data into a transient database. This basically shows one way using JPA and paged reading. | JPA, paging
[_*Skip*_ malicious items](#job_skip) | `skipItems.job.xml` | `ComplexBatchJobTest: testSkipItemsDueException()` | Normally, an exception causes a job to fail. This shows how to define exceptions that cause items to be skipped instead of causing a stop. | skip, exception, skip-limit
[_*Retry*_ malicious items](#job_retry) | `retryItems.job.xml` | `ComplexBatchJobTest: retryItemsDueException()` | Normally, an exception causes a job to fail. This shows how to define exceptions that cause items to be retried again upon exceptions instead of causing a stop. | retry, exception, retry-limit
[Parallel, split execution](#job_split) | `parallelSplit.job.xml` | `ComplexBatchJobTest: testParallelSplit()` | Using splits, you can execute parallel, disjunct tasks. If you want to want same kind of data concurrently, check out the partitioning example. | split, parallel, concurrency
[Parallel, partitioned execution](#job_partition) | `parallelPartitions.job.xml` | `ComplexBatchJobTest: testParallelPartitions()` | With partitioning, you can parallelise work on the same kind of data but different area. For example, all employees from A-K are handled in one thread, while the other handles L-Z. If you want to execute disjunct stuff in parallel, see the split example. | partitioning, parallel, concurrency
[Decisions](#job_decision) | `decisions.job.xml` | `ComplexBatchJobTest: testDecisions()` | While the execution order of job elements are defined using a fixed `next` target, implementing a decider allows to dynamically control the flow of a job execution. | decider, conditional

## WAR

Besides the JUnit based demonstrations, the project also comes with an example WAR file that you can deploy
into your JEE container of choice. It was tested with Glassfish 4.x and Wildfly 8 (former JBoss AS).
Before you deploy, you probably may need to adapt the *src/main/resources/META-INF/persitence.xml* file and build again,
it should however work out-of-the-box with Wildfly. If you want to deploy into another JEE container, just regard the
commented stuff as it should give you some hints on how to configure for another container or maybe already contain
the right stuff.

The WAR does not offer any UI (yet), but you can launch jobs via JMX (e.g. using JConsole).

# Examples details

## <a name="job_simple" />Simple

Summary: Converts CSV data into XML using a batchlet, file reader, processor and XML writer (JAXB).

* `SimpleBatchJobTest.java`: JUnit to showcase how to use/control a job.
* `SimpleJob.java`: Service called by the JUnit test.
* `META-INF/batch-jobs/simple.job.xml`: Job flow definition, wires everything together, referred to by `SimpleJob.java`.
    * Step 1: `ProvideDataBatchlet.java`: Generates example CSV file with help from `EmployeeGenerator.java`.
    * Step 2: Iterated per data entity (i.e. employee CSV record)
        * Read/parse CSV file: `CsvFileReader.java` reads file and returns one `Employee.java` instance per record.
        * Process data (optional): `ItemProcessor.java` receives `Employee` bean applies some dummy changes to the `Employee` bean. Resulting `Employee` bean is passed to writer.
        * Write XML file: `XmlFileWriter.java` receives (modified) `Employee` bean (list to allow buffered writing) from `ItemProcessor` and uses JAXB streaming to write record to XML file.

## <a name="job_jpa" />JPA paging

Summary: Shows JPA based database access with paged reading.

* `ComplexBatchJobTest.java#testJustReadAndWrite`: JUnit to showcase how to use/control a job.
* `ComplexJob.java`: Service called by JUnit test.
* `META-INF/batch-jobs/justReadAndWrite.job.xml`: Job flow definition.
    * Step 1: `GenerateDataBatchlet.java` creates random `Employee` beans and persist them into the database created on-the-fly (see `src/test/resources/persistence-test.xml`).
    * Step 2: Iteration.
        * Read from database: `JpaPagingReader.java` reads one `Employee` entity after another.
        * Process data: `Processor.java` just adds a remark/comment to the bean.
        * Write data back to database: `JpaPagingWriter.java` persist the modified bean (just calls flush...).

## <a name="job_skip" />Skip malicious items + listener

Summary: Shows how to deal with errors by skipping defective records.

* `ComplexBatchJobTest.java#testSkipItemsDueException`: JUnit to showcase how to use/control a job.
* `ComplexJob.java`: Service called by JUnit test.
* `META-INF/batch-jobs/skipItems.job.xml`: Job flow definition.
    * Step 1: `GenerateDataBatchlet.java` creates random `Employee` beans and persist them into the database created on-the-fly (see `src/test/resources/persistence-test.xml`).
    * Step 2: Iteration with defined exceptions thrown.
        * Read from database: `JpaPagingReader.java` reads one `Employee` entity after another.
        * Process data: `Processor.java` just adds a remark/comment to the bean.
        * Write data back to database: `JpaPagingWriter.java` persist the modified bean.
            * The writer has a built-in functionality to throw exceptions (to emulate real ones).
            * For example, it throws a `NullPointerException` or `PersistenceException`.
            * The exception is thrown at item level as defined in `skipItems.job.xml` using the `throwException` property.
            * After the exception to throw, the @number defines the item index to throw the exception.
* Normally, exceptions would cause the job to fail. By providing a whitelist (blacklist approach is also possible) of exceptions via `<skippable-exceptions-classes />` in the job XML, you can define exceptions to be ignored.
* Ignoring the exception means that the item in which a processing error occurred will be skipped.
* So, the job just advances to the next item and the skipped item is counted in the job summary.
* Instead of skipping, you can also tell the job engine to try again - this is shown in the next example.
* A listener is invoked using aspects and can additionally react on skipped items. In this case, `SkipWriteListener.java` is just logging the skipped item for further diagnosis.

## <a name="job_retry" />Retry malicious items

Summary: Shows how to deal with errors by retrying defective records again.

* `ComplexBatchJobTest.java#testRetryItemsDueException`: JUnit to showcase how to use/control a job.
* `ComplexJob.java`: Service called by JUnit test.
* `META-INF/batch-jobs/retryItems.job.xml`: Job flow definition.
    * Step 1: `GenerateDataBatchlet.java` creates random `Employee` beans and persist them into the database created on-the-fly (see `src/test/resources/persistence-test.xml`).
    * Step 2: Iteration with defined exceptions thrown.
        * Read from database: `JpaPagingReader.java` reads one `Employee` entity after another.
        * Process data: `Processor.java` just adds a remark/comment to the bean.
        * Write data back to database: `JpaPagingWriter.java` persist the modified bean.
            * The writer has a built-in functionality to throw exceptions (to emulate real ones).
            * For example, it throws a `NullPointerException` or `PersistenceException`.
            * The exception is thrown at item level as defined in `retryItems.job.xml` using the `throwException` property.
            * After the exception to throw, the @number defines the item index to throw the exception.
* Normally, exceptions would cause the job to fail. By providing a whitelist (blacklist approach is also possible) of exceptions via `<retryable-exception-classes />` in the job XML, you can define exceptions to be ignored.
* When an exception is thrown while processing the item and it is fine for the exception to retry again, the job engine resets processing of the current item and tries again.
* The `retry-limit` setting in the `<chunk />` tells how often to repeat in order to avoid an endless loop.
* If no mixed with skipping (see previous example), the job will fail if the exception still occurs and the retry-limit is exhausted.

## <a name="job_split" />Parallel, split execution

Summary: Shows how to execute ("unrelated") steps in parallel.

* `ComplexBatchJobTest.java#testParallelSplit`: JUnit to showcase how to use/control a job.
* `ComplexJob.java`: Service called by JUnit test.
* `META-INF/batch-jobs/parallelSplit.job.xml`: Job flow definition.
    * Step 1: `GenerateDataBatchlet.java` creates random `Employee` beans and persist them into the database created on-the-fly (see `src/test/resources/persistence-test.xml`).
    * Step 2: Executes two parallel tasks (`<flow />`) using a `<split />` definition.
        * Task (flow) "threadA" reads/processes/writes some items from and to the database.
        * Task (flow) "threadB" reads/processes/writes the same items from and to the database. NB: Yes, I need to find a different use case here.
    * Step 3: Just shows the changes in console by re-reading it from the database (using `SelectAllDataBatchlet.java`).
* Each flow is executed in parallel. In most cases, you might want to execute stuff in parallel that does not operate on the same data to avoid concurrency issues.
* Split is intended for parallel execution of tasks that e.g. do different calculation no related to each other.
* If you want to parallelise the same execution on different ranges of the source data, use partitioning instead (see next example).

## <a name="job_partition" />Parallel, partitioned execution

Summary: Shows how to execute the same task in parallel on different ranges of the source data (items).

* `ComplexBatchJobTest.java#testParallelPartitions`: JUnit to showcase how to use/control a job.
* `ComplexJob.java`: Service called by JUnit test.
* `META-INF/batch-jobs/parallelPartitions.job.xml`: Job flow definition.
    * Step 1: `GenerateDataBatchlet.java` creates random `Employee` beans and persist them into the database created on-the-fly (see `src/test/resources/persistence-test.xml`).
    * Step 2: Read/process/write parallel by partitioning the items from the reader.
        * The read/proc/write definition is done as usual.
        * We need a special item reader that supports providing items by start/end index, see `JpaPartitionReader.java`.
        * We use the `<partition />` and `<plan />` elements in he job XML to configure execution:
            * Number of threads (and thus partitions) to create.
            * Define which data partition is responsible for which items (by number index, e.g. partition 1 is responsible for items
    * Step 3: Just shows the changes in console by re-reading it from the database (using `SelectAllDataBatchlet.java`).
* A custom mapper implementationIt could, for example, consult the database and find out how many items exist and build e.g. the partitions by finding out the from/to values, e.g. by dividing the number of items by 2.

## <a name="job_decision" />Decisions

Summary: Shows how to define a dynamic job flow (step addressing) depending on previous step outcome.

* `ComplexBatchJobTest.java#testDecisions`: JUnit to showcase how to use/control a job.
* `ComplexJob.java`: Service called by JUnit test.
* `META-INF/batch-jobs/decisions.job.xml`: Job flow definition.
    * Step 1: A stupid batchlet (`EchoBatchlet.java`) is invoked that echos an XML property to the log/console and sets a certain result.
    * Then: The step that previously called the echo batchlet does not address the next step directly, but delegates to a decider.
        * A custom decider implementation (`SimpleDecider.java`) is registered in the XML using the <decision /> element.
        * You can do whatever logic you need to in a decider, this one is just checking the step name just executed (defined by `<step id="x" .../>`).
        * Depending on the step name, it returns the next step ID a a result of the `decide` method.
    * Step 2: The job engine executes the according step ID as defined by `SimpleDecider.java#decide` or finishes execution.
