package de.huepattl.playground.batch.complex;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.batch.api.BatchProperty;
import javax.batch.api.Batchlet;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Simple batchlet printing and logging a message specified via parameter.
 * We use this for demonstrating decisions.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-11-27
 */
@Named
public class EchoBatchlet implements Batchlet {

    private final Logger LOG = LogManager.getLogger(EchoBatchlet.class.getName());

    /**
     * Provides the message to log and display.
     */
    @Inject
    @BatchProperty(name = "message")
    String message;

    /**
     * Will just log and print (to STDOUT) the message as provided via parameter {@link #message}.
     *
     * @throws Exception BAM!
     */
    @Override
    public String process() throws Exception {
        LOG.info(message);
        System.out.println(EchoBatchlet.class.getSimpleName() + " sez: " + message);
        return "COMPLETED";
    }

    /**
     * Not supported here... makes no sense at all...
     *
     * @throws Exception BAM!
     */
    @Override
    public void stop() throws Exception {
        // Not now...
    }
}
