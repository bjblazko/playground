package de.huepattl.playground.batch.complex;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.batch.api.Decider;
import javax.batch.runtime.StepExecution;
import javax.inject.Named;

/**
 * Well... it is doing... decisions. The decider returns a custom string that is
 * evaluated in {@code <decision />} elements using its
 * {@code <next />}, {@code <end />}, {@code <stop />} or {@code <fail />}
 * control blocks.
 * <p>
 * The {@link #decide(javax.batch.runtime.StepExecution[])} method can evaluate
 * information from the passed {@link javax.batch.runtime.StepExecution},
 * decider parameters passed from the job XML or by retrieving other attributes
 * (transient or not) bound to the job execution context.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-11-27
 */
@Named
public class SimpleDecider implements Decider {

    private final Logger LOG = LogManager.getLogger(
            SimpleDecider.class.getName());

    private static final String S1 = "iAm";
    private static final String S2 = "locutus";
    private static final String D1 = "DECISION_1";
    private static final String D2 = "DECISION_2";
    private static final String FAIL = "FAIL";

    /**
     * The resulting string of this operation is used to address the next
     * step/flow/split in the job or to end, stop or fail the job. Example:      {@code
     * <decision id="decider">
     * <next on="CONTINUE_A" to="step2" />
     * <next on="CONTINUEB_B" to="step3" />
     * <end on="OK" />
     * <stop on="I_AM_TIRED" />
     * <fail on="ERROR" />
     * </decision>
     * }
     * <p>
     * So, the 'on' expression if tested and is the outcome of this method. For
     * {@code <next />}, 'to' is the id of the next step/flow/exit, while the others
     * cause the job to end successfully, stop or even fail accordingly.
     *
     * @param executions Execution context as passed by the job runtime.
     *
     * @return String defining further job flow as defined by {@code <decision />} in
     *         job XML.
     * @throws Exception BAM!
     */
    @Override
    public String decide(StepExecution[] executions) throws Exception {
        final String stepName = executions[0].getStepName();
        String decision;

        switch (stepName) {
            case S1:
                decision = D1;
                break;
            case S2:
                decision = D2;
                break;
            default:
                decision = FAIL;
        }

        LOG.info("My decision is: " + decision);

        return decision;
    }
}
