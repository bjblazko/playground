package de.huepattl.playground.batch.complex;

import de.huepattl.playground.batch.Employee;
import javax.batch.api.BatchProperty;
import javax.batch.api.chunk.ItemProcessor;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * This is a very stupid item processor: it consumes {@link Employee} items and
 * performs a simple modification to that item. It will update the
 * {@link Employee#remark} property to whatever is configured for this processor
 * in the step (in justReadAndWrite.job.xml file), coming in via the
 * {@link #changeRemarkTo} parameter.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-11-07
 *
 * @see JpaPagingReader
 * @see JpaPartitionReader
 * @see JpaPagingWriter
 */
@Named("processor")
public class Processor implements ItemProcessor {

    /**
     * This comes from the property specified in justReadAndWrite.job.xml. Whatever you
     * provide here - each {@link Employee} will have it in the remarks field.
     * This makes few sense in real life, but we use it here to indicate changes
     * applied for the current step purpose.
     */
    @Inject
    @BatchProperty(name = "changeRemarkTo")
    public String changeRemarkTo;

    /**
     * Receives an item (employee) from the item reader, changes the remark
     * property of it to the value specified by {@link #changeRemarkTo} and
     * returns the modified item in order to be consumed by the item writer.
     *
     * @param item {@link Employee} instance coming from the reader.
     * @return Modified {@link Employee} instance (updated remark property),
     *         consumed by the writer.
     *
     * @throws Exception BAM!
     */
    @Override
    public Object processItem(Object item) throws Exception {
        Employee e = (Employee) item;
        e.setRemark(changeRemarkTo);
        return e;
    }

}
