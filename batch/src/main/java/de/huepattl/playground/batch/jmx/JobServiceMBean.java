package de.huepattl.playground.batch.jmx;

import javax.management.MXBean;

/**
 * JMX MBean interface for an example job launcher. If you deploy the example WAR file to
 * an according (JEE) container, you can use the JConsole to start the jobs.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-11-22
 */
@MXBean
public interface JobServiceMBean {

    /**
     * Starts the 'simple' job as per definition from the unit tests.
     *
     * @param folder File system folder to use for processing, e.g. /tmp
     * @return Job execution ID, can be referenced by {@link #getJobStatus(long)}.
     */
    long startSimple(final String folder);

    /**
     * Start one of the 'complex' job examples by job name. The job name can be found in any
     * of the job XMLs in META-INF/batch-jobs/.
     *
     * @param jobName Name of the job to start, just have a look at the unit tests or job XMLs.
     * @return Job execution ID, can be referenced by {@link #getJobStatus(long)}.
     */
    long startComplex(final String jobName);

    /**
     * Returns job information in string representation for a giving job execution.
     *
     * @param id Job execution ID as returned by {@link #startSimple(String)} and
     *           {@link #startComplex(String)}.
     * @return String representation of job execution, do not try to parse...
     */
    String getJobStatus(long id);
}
