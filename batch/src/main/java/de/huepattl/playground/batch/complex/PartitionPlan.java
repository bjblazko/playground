package de.huepattl.playground.batch.complex;

import java.util.Properties;

/**
 * Container object to contain a partition plan, this is used by the {@link de.huepattl.playground.batch.complex.PartitionMapper}.
 * FIXME: Ain't there a default implementation?
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-11-20
 * @deprecated Not used in current code, leaving it here for demonstration purpose.
 */
@Deprecated
public class PartitionPlan implements javax.batch.api.partition.PartitionPlan {
    private int pcount;
    private int tcount;
    private boolean poverride;
    private Properties[] pprops;

    @Override
    public void setPartitions(int count) {
        pcount = count;
    }

    @Override
    public void setPartitionsOverride(boolean override) {
        poverride = override;
    }

    @Override
    public boolean getPartitionsOverride() {
        return poverride;
    }

    @Override
    public void setThreads(int count) {
        tcount = count;
    }

    @Override
    public void setPartitionProperties(Properties[] props) {
        pprops = props;
    }

    @Override
    public int getPartitions() {
        return pcount;
    }

    @Override
    public int getThreads() {
        return tcount;
    }

    @Override
    public Properties[] getPartitionProperties() {
        if (this.pprops == null) {
            return new Properties[0];
        } else return pprops;
    }
}
