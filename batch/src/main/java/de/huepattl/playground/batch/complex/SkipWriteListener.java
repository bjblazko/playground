package de.huepattl.playground.batch.complex;

import de.huepattl.playground.batch.Employee;
import java.util.List;
import javax.batch.runtime.context.StepContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A listener reacting on skips from a writer. Skips are exceptions thrown in
 * the writer that are declared as skippable, see according
 * {@code <skippable-exception-classes>} in job XML.
 *
 * @author blazko
 * @since 2013-11-10
 */
@Named("skipWriteListener")
public class SkipWriteListener implements
        javax.batch.api.chunk.listener.SkipWriteListener {

    private final Logger LOG = LogManager.getLogger(
            SkipWriteListener.class.getName());

    @Inject
    StepContext stepContext;

    /**
     * Called by the framework whenever an exception occurs in the writer that
     * is declared as skippable. Skips are limited in the chunk definition with
     * the 'skip-limit' property.
     *
     * @param items Items as passed to the writer.
     * @param ex    The exception that occurred.
     *
     * @throws Exception BAM!
     */
    @Override
    @SuppressWarnings("unchecked")
    public void onSkipWriteItem(List<Object> items, Exception ex) throws
            Exception {
        LOG.error("Ooooops! Caught exception from writer: " + ex);
        LOG.info("Have to redo items with (employee) ID from "
                + ((Employee) items.get(0)).getId() + " until "
                + ((Employee) items.get(items.size() - 1)).getId());

        LOG.info("Items that caused the excption:");
        List<Employee> failedItems = (List<Employee>) stepContext.getTransientUserData();
        for (final Employee e : failedItems) {
            LOG.info("   - Failed: " + e.toString());
        }
    }

}
