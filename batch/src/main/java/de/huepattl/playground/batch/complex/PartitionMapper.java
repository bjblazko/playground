package de.huepattl.playground.batch.complex;

import de.huepattl.playground.batch.EmployeeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.batch.api.partition.PartitionPlan;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * The partition mapper actually maps the partition properties to the according thread. In our example,
 * it defines the start/end item index for each item reader per thread. So, the reader of threadA is responsible
 * for dealing with items 0..99 while the one of threadB for 100..199.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-11-20
 * @deprecated Not used in current code, leaving it here for demonstration purpose.
 */
//@Named("partitionMapper")
@Deprecated
public class PartitionMapper implements javax.batch.api.partition.PartitionMapper {

    final Logger LOG = LogManager.getLogger(PartitionMapper.class.getName());

    /**
     * Our JPA employee service.
     */
    @Inject
    EmployeeService employeeService;

    /**
     * See class comment above, this is not actual code but left for demonstration purposes.
     * @return Partition plan containing the mappings per thread.
     * @throws Exception BAM!
     */
    @Override
    public PartitionPlan mapPartitions() throws Exception {
        List<Properties> partitionProperties = new ArrayList<>();

        de.huepattl.playground.batch.complex.PartitionPlan pp =
                new de.huepattl.playground.batch.complex.PartitionPlan();

        int partitions = 2;//(employeeService.getAmount() / 100) + 1;
        pp.setPartitions(partitions);

        for (int i = 0; i < partitions; i++) {
            Properties currentProperties = new Properties();
            currentProperties.setProperty("offsetStart", "0");
            currentProperties.setProperty("offsetEnd", "100");
            partitionProperties.add(currentProperties);
        }

        pp.setPartitionProperties(partitionProperties.toArray(
                new Properties[partitionProperties.size()]));

        return pp;
    }
}
