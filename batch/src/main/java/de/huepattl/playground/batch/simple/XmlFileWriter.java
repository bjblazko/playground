package de.huepattl.playground.batch.simple;

import de.huepattl.playground.batch.Employee;

import java.io.FileOutputStream;
import javax.batch.api.chunk.ItemWriter;
import java.io.Serializable;
import java.util.List;
import javax.batch.api.BatchProperty;
import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This writer receives modified {@link Employee} instances from the item processor and writes as XML into the file as
 * configured in simple.job.xml. We use JAXB streaming for this to allow writing of huge files.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @see CsvFileReader
 * @see ItemProcessor
 * @since 2013-10-03
 */
public class XmlFileWriter implements ItemWriter {

    private final Logger LOG = LogManager.getLogger(XmlFileWriter.class.getName());

    /**
     * JAXB marshaller which will use the following writer for writing
     * {@link Employee} as XML to the file.
     */
    private Marshaller m;

    /**
     * Streaming writer used by JAXB to write XML to the file.
     */
    private XMLStreamWriter streamWriter;

    /**
     * This comes from the property specified in simple.job.xml
     */
    @Inject
    @BatchProperty(name = "fileName")
    private String fileName;

    /**
     * Called once by the infrastructure in order to open/create the XML file.
     *
     * @param checkpoint Not used here.
     *
     * @throws Exception BAM!
     */
    @Override
    public void open(Serializable checkpoint) throws Exception {
        LOG.info("Using/creating destination file: " + fileName);

        /*
         Create JAXB context and set options. We specify a stream writer
         so that we can append to the XML file in several calls (JAXB default is to
         create XML file in one bunch, but for huge data this is not working and
         also would not play well with the processing model of JSR 352).
         */
        JAXBContext context = JAXBContext.newInstance(Employee.class);
        m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

        streamWriter = XMLOutputFactory.newFactory().createXMLStreamWriter(new FileOutputStream(fileName));
        streamWriter.writeStartDocument(); // <?xml version="1.0" ...?>
        streamWriter.writeStartElement("employees"); // <employees>
    }

    /**
     * Called once by the infrastructure, allowing us to finalise the XML
     * document (write final element) and close the file writer.
     *
     * @throws Exception BAM!
     */
    @Override
    public void close() throws Exception {
        LOG.info("Closing XML file.");

        streamWriter.writeEndElement(); // </employees>
        streamWriter.writeEndDocument();
        streamWriter.close();
    }

    /**
     * Called as often has the job has data, this will write all items passed to the result XML document. We have a
     * list interface, because we write in bunches for performance reasons. But still, this method is called
     * multiple times and not once - the collection does not contain ALL employees as read by the item reader.
     * <p>
     * The size of the bunch (i.e. the amount of items used per write) is configured in the job XML (item-count property
     * in chunk).
     *
     * @param objects List of items to write per call.
     *
     * @throws Exception BAM!
     */
    @Override
    public void writeItems(List<Object> objects) throws Exception {
        LOG.debug("Bunched WRITE:");

        for (final Object current : objects) {
            final Employee e = (Employee) current;
            if (LOG.isDebugEnabled()) {
                LOG.debug("-> Write XML: " + e);
            }

            JAXBElement<Employee> ele = new JAXBElement<>(
                    QName.valueOf("employee"), Employee.class, e);
            m.marshal(ele, streamWriter);
        }
        LOG.debug("End bunched write.");
    }

    /**
     * Not used in this example.
     *
     * @return Always NULL here.
     * @throws Exception BAM!
     */
    @Override
    public Serializable checkpointInfo() throws Exception {
        return null;
    }
}
