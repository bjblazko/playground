package de.huepattl.playground.batch;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * A simple service to deal with {@link de.huepattl.playground.batch.Employee} instances. This example service
 * is backed by JPA and offers service methods to persist and retrieve instances.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-11-06
 */
@Stateless
@Local
public class EmployeeService {

    @PersistenceContext
    EntityManager em;

    /**
     * Persists the given employee instance to the database configured. If the employee has no unique ID
     * generated yet, one will be created.
     *
     * @param e Employee bean to persist (save or update).
     * @see #flush()
     */
    public void save(Employee e) {
        if (e.getId() != 0) {
            e = em.merge(e);
        } else {
            em.persist(e);
        }
    }

    /**
     * Retrieves a list of all {@link de.huepattl.playground.batch.Employee} persisted in the database.
     * Please use with care as it would return all known instances... this might cause out-of-memory and
     * performance issues!
     *
     * @return List of ALL employees.
     * @see #findAll(int, int)
     * @see #getAmount()
     */
    public List<Employee> findAll() {
        return em.createQuery("select e from Employee e", Employee.class)
                .getResultList();
    }

    /**
     * This method returns the number of ALL employees stored in the database. The number equals the list size
     * of a call to {@link #findAll()} but is much more efficient as it does retrieve the employee instances.
     *
     * @return Number of all known employees stored in the database.
     */
    public int getAmount() {
        return em.createQuery("select count(e) from Employee e", Integer.class)
                .getSingleResult();
    }

    /**
     * In contrast to {@link #findAll()}, this version is capable of retrieving employees from the datastore
     * by offset, for example to allow pagination.
     *
     * @param from Offset to start retrieval from, e.g. prviding a number of 10 here would skip the first 9 results
     *             from the database selection.
     * @param len  Number of employees to select from the database after #from.
     * @return List of found employees. The number of entries should normally equal #len, but may be less of there
     * are no or less entries remaining in selection.
     */
    public List<Employee> findAll(int from, int len) {
        // FIXME: we should sort here...
        TypedQuery<Employee> pagedQuery
                = em.createQuery("select e from Employee e", Employee.class);
        pagedQuery.setFirstResult(from);
        pagedQuery.setMaxResults(len);
        return pagedQuery.getResultList();
    }

    /**
     * Ensure that all pending updates are written to the database. Normally not required in JPA but we have need
     * for it in testing.
     */
    public void flush() {
        em.flush();
    }
}
