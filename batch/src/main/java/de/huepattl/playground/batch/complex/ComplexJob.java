package de.huepattl.playground.batch.complex;

import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.JobExecution;
import javax.ejb.Stateless;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Together with META-INF/batch-jobs/justReadAndWrite.job.xml, this service defines the
 * basic control of the job.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-11-06
 */
@Stateless
public class ComplexJob {

    private static final Logger LOG = LogManager.getLogger(ComplexJob.class.getName());

    /**
     * Well, start a (new) job execution.
     *
     * @param name Name of the job, basically the XML file name but without the .job.xml suffix.
     * @return Job execution identifier that can later be used to retrieve
     *         information on the job, restart it etc.
     */
    public long startJob(final String name) {
        JobOperator jobOperator = BatchRuntime.getJobOperator();

        /*
         Define job properties. These properties can be used in the job XML
         (see below) using EL (Expression Language).
         */
        Properties props = new Properties();

        /*
         This points to the META-INF/batch-jobs/justReadAndWrite.job.xml
         */
        LOG.info("Starting job " + name);
        final long executionId = jobOperator.start(name + ".job", props);
        LOG.info("Execution ID is: " + executionId);
        return executionId;
    }

    /**
     * Get status information.
     *
     * @param executionId Execution identifier as returned by
     *                    {@link #startJob(java.lang.String)}.
     * @return Record containing the details.
     */
    public JobExecution getJobExecutionDetails(long executionId) {
        JobOperator jobOperator = BatchRuntime.getJobOperator();
        JobExecution jobExecution = jobOperator.getJobExecution(executionId);
        return jobExecution;
    }

    /**
     * Restart a stopped or failed previous job execution. You have to pass the
     * original job execution ID as returned by
     * {@link #startJob(java.lang.String)}.
     *
     * @param executionId Original job execution ID.
     * @return ID of the new job execution.
     */
    public long restartJob(long executionId) {
        Properties jobProperties = new Properties();
        long newExecutionId
                = BatchRuntime.getJobOperator().restart(executionId, jobProperties);
        return newExecutionId;
    }
}
