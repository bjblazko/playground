package de.huepattl.playground.batch.complex;

import de.huepattl.playground.batch.Employee;
import de.huepattl.playground.batch.EmployeeService;
import java.io.Serializable;
import java.util.List;
import javax.batch.api.BatchProperty;
import javax.batch.api.chunk.ItemReader;
import javax.batch.runtime.context.StepContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Simple item reader using JPA internally. This implementation uses paging
 * internally, which means that a call to {@link #readItem()} is not invoking a
 * database access each time.<p>
 * Instead, this reader reads-ahead a bunch (page) of several employee items
 * (say, 20) and stores them into a local collection. Each call to
 * {@link #readItem()} just causes a counter to be incremented, used for index
 * accss to the list, so the indexed item is returned from that internal page
 * cahce.<p>
 * If the page end is reached, the next page will be read from the database.
 * This is not done directly here but in {@link EmployeeService}, which executes
 * a JPA query and uses JPA's built-in pagination using
 * {@link Query#setFirstResult(int)} and {@link Query#setMaxResults(int)}
 * .<p>
 * Sorry, the code is not so beautiful and a quick hack...
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-11-07
 *
 * @see EmployeeService
 * @see Processor
 * @see JpaPagingWriter
 */
@Named("jpaPagingReader")
public class JpaPagingReader implements ItemReader {

    private final Logger LOG = LogManager.getLogger(
            JpaPagingReader.class.getName());

    /**
     * Contains information on the current step within the batch, including
     * ID/name and summary information (commits, items read etc.).
     */
    @Inject
    StepContext stepContext;

    /**
     * This reader internally caches some items and returns a single item in
     * {@link #readItem()} per index. This parameter specifies the amount of
     * items to read-ahead (internal pagination page size). FIXME: We use string
     * and pass it to {@link #_pageSize} because using integer directly caused a
     * runtime error in WELD/Glassfish contrary to the spec (which shall cast
     * simple types accordingly...)!
     *
     * @see #_pageSize
     */
    @Inject
    @BatchProperty(name = "pageSize")
    private String pageSize;

    /**
     * Transaction service facade really doing the JPA access.
     */
    @Inject
    EmployeeService employeeService;

    /**
     * @see #pageSize
     */
    private int _pageSize;

    /**
     * Current page index. Each page contains at max {@link #_pageSize} items.
     */
    private int currentPage = 0;

    /**
     * The current item within the current page ({@link #currentPageObjects}).
     */
    private int currentItemIndexInPage = -1;

    /**
     * Simple overall counter. At the end, this value should equal the amount of
     * read items from the step metrics ({@link StepContext#getMetrics()}).
     */
    private int currentItemOverall;

    /**
     * List of employees for current paged cache.
     *
     * @see #currentPage
     */
    private List<Employee> currentPageObjects;

    /**
     * Open the reader. Nothing special here except evaluating/defaulting the
     * page size ({@link #_pageSize}).
     *
     * @param checkpoint Unused checkpoint information.
     *
     * @throws Exception BAM!
     */
    @Override
    public void open(Serializable checkpoint) throws Exception {
        LOG.debug("Using employee service: " + employeeService);
        LOG.info("Step: " + stepContext.getStepExecutionId()
                + " (" + stepContext.getStepName() + ")");

        _pageSize = Integer.parseInt(pageSize);
        if (_pageSize == 0) {
            _pageSize = 20;
        }

        LOG.info("Reader opened, starting processing with a cache size of "
                + _pageSize + "...");
    }

    /**
     * Called by the batch runtime in a loop until NULL is returned (no further
     * items). Each call returns the next item from the internal cache. If the
     * last item of the cache has been reached, the next page (bunch) of items
     * (employees) will be read from the JPA service.
     *
     * @return Next employee from database or NULL if no further ones available.
     *
     * @throws Exception BAM!
     */
    @Override
    public Object readItem() throws Exception {
        currentItemIndexInPage++;  // next employee
        currentItemOverall++;

        /*
         Invoke:
         - First call (no employees collection)
         - Next employee exceeds current page
         */
        if (currentPageObjects == null || currentItemIndexInPage >= _pageSize) {
            readNextPage();
        }

        if (currentItemIndexInPage >= currentPageObjects.size()) {
            return null; // the very last item at all! Finish reader.
        } else {
            LOG.debug("Read item: " + currentPageObjects.get(
                    currentItemIndexInPage));
            return currentPageObjects.get(currentItemIndexInPage);
        }
    }

    /**
     * Not implemented - normally allows you to obtain custom progress
     * information.
     *
     * @return NULL
     * @throws Exception BAM!
     */
    @Override
    public Serializable checkpointInfo() throws Exception {
        return null;
    }

    /**
     * Called by the runtime after {@link #readItem()} returned NULL, i.e. the
     * very last item has been read.
     *
     * @throws Exception BAM!
     */
    @Override
    public void close() throws Exception {
        LOG.info("Reader closed.");
    }

    /**
     * Triggers the JPA service to read the next bunch of items from the
     * database. Resets current page counters for {@link #readItem()}.
     */
    private void readNextPage() {
        currentItemIndexInPage = 0;
        int fromIndex = currentPage * _pageSize;
        currentPage++;

        LOG.info("Reading next page of " + _pageSize
                + " items, starting at result position: " + fromIndex);

        currentPageObjects = employeeService.findAll(
                fromIndex, _pageSize);
    }

}
