package de.huepattl.playground.batch.simple;

import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.JobExecution;
import javax.ejb.Stateless;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Together with META-INF/batch-jobs/simple.job.xml, this service defines the
 * basic control of the job. Please have a look in the tests of this project to have a look for the usage
 * of this service.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-10-03
 */
@Stateless
public class SimpleJob {
    
    private final Logger LOG = LogManager.getLogger(SimpleJob.class.getName());

    /**
     * Well, start a (new) job execution.
     *
     * @param dataFolder (Yet empty) Processing folder that will later contain
     *                   the CSV file (generated) and the resulting XML file.
     * @return Job execution identifier that can later be used to retrieve
     *         information on the job, restart it etc.
     */
    public long startJob(final String dataFolder) {
        JobOperator jobOperator = BatchRuntime.getJobOperator();

        /*
         Define job properties. These properties can be used in the job XML
        (see below) using EL (Expression Language).
         */
        Properties props = new Properties();
        props.setProperty("dataFolder", dataFolder);

        /*
         This points to the META-INF/batch-jobs/simple.job.xml
         */
        LOG.info("Starting job \'simple.job\'");
        final long executionId = jobOperator.start("simple.job", props);
        LOG.info("Done, execution ID is: " + executionId);
        return executionId;
    }

    /**
     * Get status information.
     *
     * @param executionId Execution identifier as returned by
     *                    {@link #startJob(java.lang.String)}.
     * @return Record containing the details.
     */
    public JobExecution getJobExecutionDetails(long executionId) {
        JobOperator jobOperator = BatchRuntime.getJobOperator();
        JobExecution jobExecution = jobOperator.getJobExecution(executionId);
        return jobExecution;
    }

    /**
     * Restarts a stopped or failed previous job execution. You have to pass the
     * original job execution ID as returned by
     * {@link #startJob(java.lang.String)}.
     *
     * @param executionId Original job execution ID.
     * @return ID of the new job execution.
     */
    public long restartJob(long executionId) {
        Properties jobProperties = new Properties();
        long newExecutionId
                = BatchRuntime.getJobOperator().restart(executionId, jobProperties);
        return newExecutionId;
    }
}
