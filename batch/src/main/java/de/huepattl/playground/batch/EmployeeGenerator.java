package de.huepattl.playground.batch;

import java.security.SecureRandom;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Utility class that will generate some test data for us. By using the {@link #generateEmployees(long)} method,
 * we can obtain any number of random employee stances for testing.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-10-03
 */
public class EmployeeGenerator {

    /**
     * List of first names.
     */
    private static final String[] firstNames = {"Jane", "John", "Neo", "Sid", "Mary-Jane", "Sarah", "Michael", "Tim", "Christina", "Mitch", "Jennifer", "Frank", "Joe", "Anna", "Walter", "Bart", "Lisa", "Herbert", "Bruce", "Edward", "Chris", "Mel", "Jean", "Maria", "Rob", "Simon", "Hans", "Gretel"};

    /**
     * List of last names.
     */
    private static final String[] lastNames = {"Miller", "Doe", "Bosch", "Vicious", "Smith", "Buchanan", "Stuart", "von Billerbeck", "Berg", "Milhouse", "Simpson", "Black", "White", "Anna", "Pink", "Schulz", "Macmillian", "Gruber", "Grosse", "Robertson"};

    /**
     * Generate some random employees for us.
     *
     * @param number Number of records to create.
     * @return List of employee beans.
     */
    public static List<Employee> generateEmployees(final long number) {
        List<Employee> result = new ArrayList<>();
        Random r = new Random();

        Date b;
        for (int i = 0; i < number; i++) {
            Employee e = new Employee();
            e.setId(i);
            e.setLastName(lastNames[r.nextInt(lastNames.length)]);
            e.setFirstName(firstNames[r.nextInt(firstNames.length)]);
            b = new Date(java.sql.Date.valueOf(LocalDate.of(
                    r.nextInt(100) + 1900, r.nextInt(12) + 1, r.nextInt(28) + 1)).getTime());

            e.setBirthDate(b);
            e.setExternal(r.nextBoolean());
            result.add(e);
        }

        return result;
    }

}
