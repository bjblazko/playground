package de.huepattl.playground.batch.complex;

import de.huepattl.playground.batch.Employee;
import de.huepattl.playground.batch.EmployeeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.batch.api.Batchlet;
import javax.batch.runtime.context.JobContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * A simple batchlet that selects all data from the database and prints it. Intended for debugging purposes
 * after a job has run to show the changes made. Target is STDOUT.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-11-19
 */
@Named("selectAllDataBatchlet")
public class SelectAllDataBatchlet implements Batchlet {

    final Logger LOG = LogManager.getLogger(SelectAllDataBatchlet.class.getName());

    /**
     * Provides full access to the job context.
     */
    @Inject
    JobContext jobContext;

    @Inject
    EmployeeService employeeService;

    @Override
    public String process() throws Exception {
        employeeService.flush();
        List<Employee> res = employeeService.findAll();
        for (final Employee e : res) {
            LOG.info(e);
        }

        return "SUCCESS";
    }

    @Override
    public void stop() throws Exception {
        // Not supported here...
    }

}
