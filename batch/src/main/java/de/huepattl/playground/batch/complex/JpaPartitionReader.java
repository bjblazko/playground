package de.huepattl.playground.batch.complex;

import de.huepattl.playground.batch.Employee;
import de.huepattl.playground.batch.EmployeeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.batch.api.BatchProperty;
import javax.batch.api.chunk.ItemReader;
import javax.batch.runtime.context.StepContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * This reader implements partitioned reading via JPA. Partition reading purpose
 * is to enhance database access performance by not opening a cursor per single
 * read. So this reader reads an entire chunk but only returns a single item via
 * {@link #readItem()}. Thus, this item reader keeps state.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-11-20
 */
@Named("jpaPartitionReader")
public class JpaPartitionReader implements ItemReader {

    private final Logger LOG = LogManager.getLogger(
            JpaPartitionReader.class.getName());

    /**
     * Contains information on the current step within the batch, including
     * ID/name and summary information (commits, items read etc.).
     */
    @Inject
    StepContext stepContext;

    @Inject
    @BatchProperty(name = "offsetStart")
    private String offsetStart;

    @Inject
    @BatchProperty(name = "offsetEnd")
    private String offsetEnd;

    /**
     * Transaction service facade really doing the JPA access.
     */
    @Inject
    EmployeeService employeeService;

    /**
     * @see #offsetStart
     */
    private int _start;

    /**
     * @see #offsetEnd
     */
    private int _end;

    /**
     * List of employees for current paged cache.
     */
    private List<Employee> partition;

    private int currentIndex = 0;

    /**
     * Open the reader.
     *
     * @param checkpoint Unused checkpoint information.
     *
     * @throws Exception BAM!
     */
    @Override
    public void open(Serializable checkpoint) throws Exception {
        LOG.debug("Using employee service: " + employeeService);
        LOG.info("Step: " + stepContext.getStepExecutionId()
                + " (" + stepContext.getStepName() + ")");

        _start = Integer.parseInt(offsetStart);
        _end = Integer.parseInt(offsetEnd);

        LOG.info("Reader opened, will read items "
                + _start + "-" + _end);
    }

    /**
     * Called by the batch runtime in a loop until NULL is returned (no further
     * items). Each call returns the next item from the internal cache. If the
     * last item of the cache has been reached, the next page (bunch) of items
     * (employees) will be read from the JPA service.
     *
     * @return Next employee from database or NULL if no further ones available.
     * @throws Exception BAM!
     */
    @Override
    public Object readItem() throws Exception {
        if (partition == null) {
            partition = employeeService.findAll(
                    _start, (_end - _start));
            currentIndex = -1;
        }
        currentIndex++;
        if (currentIndex < _end && currentIndex < partition.size()) {
            return partition.get(currentIndex);
        } else {
            return null;
        }
    }

    /**
     * Not implemented, allows you normally to retrieve custom information on
     * the current progress.
     *
     * @return NULL
     * @throws Exception BAM!
     */
    @Override
    public Serializable checkpointInfo() throws Exception {
        return null;
    }

    /**
     * Called by the runtime after {@link #readItem()} returned NULL, i.e. the
     * very last item has been read.
     *
     * @throws Exception BAM!
     */
    @Override
    public void close() throws Exception {
        LOG.info("Reader closed.");
    }

}
