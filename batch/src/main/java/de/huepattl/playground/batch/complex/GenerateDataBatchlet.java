package de.huepattl.playground.batch.complex;

import de.huepattl.playground.batch.Employee;
import de.huepattl.playground.batch.EmployeeGenerator;
import de.huepattl.playground.batch.EmployeeService;
import java.util.List;
import javax.batch.api.BatchProperty;
import javax.batch.api.Batchlet;
import javax.batch.runtime.context.JobContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A simple batchlet that is generating test data and persisting it into a
 * database.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-11-06
 */
@Named("generateDataBatchlet")
public class GenerateDataBatchlet implements Batchlet {

    private final Logger LOG = LogManager.getLogger(GenerateDataBatchlet.class.getName());

    private long _numberToGenerate = 1000;
    private static final long STEP_SIZE = 10;

    /**
     * Optional: define the number of employees to generate. If not specified, 1000 will be used as default.
     */
    @Inject
    @BatchProperty(name = "amount")
    private String amount;

    /**
     * ...to allow aborting running process, see {@link #stop()}.
     */
    private boolean doContinue = true;

    /**
     * Provides full access to the job context.
     */
    @Inject
    JobContext jobContext;

    @Inject
    EmployeeService employeeService;

    @Override
    public String process() throws Exception {
        if (amount != null) {
            _numberToGenerate = Integer.parseInt(amount);
        }
        for (int i = 0; i < _numberToGenerate; i += STEP_SIZE) {
            if (doContinue) {
                List<Employee> employees
                        = EmployeeGenerator.generateEmployees(STEP_SIZE);
                for (Employee currentEmployee : employees) {
                    /*
                     Our generator provides (redundant) IDs in this case
                     because we call it multiple times (it always increments
                     from zero). We use JPA with auto-generation and thus
                     reset the ID here to zero.
                     */
                    currentEmployee.setId(0);
                    employeeService.save(currentEmployee);
                }
            } else {
                LOG.warn("Stop REQUESTED, halting...");
                return "STOP";
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.info("Created the following instances:");
            List<Employee> res = employeeService.findAll();
            for (final Employee e : res) {
                LOG.debug("Generated: " + e);
            }
        }
        return "SUCCESS";
    }

    @Override
    public void stop() throws Exception {
        this.doContinue = false;
    }

}
