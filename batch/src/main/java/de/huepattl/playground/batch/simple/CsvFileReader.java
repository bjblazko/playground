package de.huepattl.playground.batch.simple;

import de.huepattl.playground.batch.Employee;

import java.io.BufferedReader;

import javax.batch.api.BatchProperty;
import javax.batch.api.chunk.ItemReader;
import javax.inject.Inject;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.FileSystems;
import java.nio.file.Files;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This item reader opens a CSV file as configured in simple.job.xml (passed via 'fileName' property) in the {@link
 * #open(java.io.Serializable)} method. The batch infrastructure will then call {@link #readItem()} until it returns
 * null (i.e. no more items). Per call to {@link #readItem()}, the method reads a single line from the CSV file, parses
 * the record and builds an instance of {@link Employee} from it, which is then returned. The infrastructure will
 * then pas it on to an item processor ({@link ItemProcessor}).
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @see ItemProcessor
 * @see XmlFileWriter
 * @since 2013-10-03
 */
public class CsvFileReader implements ItemReader {

    private final Logger LOG = LogManager.getLogger(CsvFileReader.class.getName());

    /**
     * The cursor marks the current processing record within the file, i.e. line.
     */
    private long cursor = 0;

    /**
     * Data may be large, we do buffered reading of the CSV file.
     */
    private BufferedReader reader;

    /**
     * This comes from the property specified in simple.job.xml.
     */
    @Inject
    @BatchProperty(name = "fileName")
    private String fileName;

    /**
     * This method is called once per job execution from the infrastructure and
     * opens the CSV file for reading.
     *
     * @param checkpoint Unused argument here.
     *
     * @throws Exception BAM!
     */
    @Override
    public void open(Serializable checkpoint) throws Exception {
        LOG.info("Picking up file " + fileName + " for parsing.");
        Path path = FileSystems.getDefault().getPath(this.fileName);
        reader = Files.newBufferedReader(path, Charset.defaultCharset());
    }

    /**
     * Invoked once when the step finishes. Here, we have the chance to close
     * the buffered reader and release the file handle.
     *
     * @throws Exception BAM!
     */
    @Override
    public void close() throws Exception {
        LOG.info("Done parsing, closing access to file " + this.fileName);
        reader.close();
    }

    /**
     * The actual reading logic, called as long by the infrastructure as long it does not return NULL. Each invocation
     * will read a single line/record from the CSV file. That record is parsed and an instance of {@link Employee}
     * is created and returned, when there are no more lines to read, we have reached EOF and NULL is returned. The
     * returned employee will be forwarded to the {@link ItemProcessor}.
     *
     * @return Employee instance from parsed current line or NULL when we have
     * reached EOF.
     * @throws Exception BAM!
     */
    @Override
    public Object readItem() throws Exception {
        cursor++;
        final String line = reader.readLine();

        if (line != null && line.contains(",")) {
            Employee e = new Employee();
            String[] elements = line.split(",");
            try {
                e.setId(Long.parseLong(elements[0]));
                e.setLastName(elements[1]);
                e.setFirstName(elements[2]);
                // FIXME
                //e.setBirthDate(DateTime.parse(elements[3]).toDate());
                e.setExternal(Boolean.parseBoolean(elements[4]));
            } catch (Exception ex) {
                LOG.error("Ooops! I had issues parsing a CSV record!", ex);
            }
            LOG.debug("-> Single CSV READ: " + e);
            return e;
        } else {
            return null; // EOF
        }
    }

    /**
     * Depends on chosen strategy in the job XML. Not used here.
     *
     * @return Always NULL here.
     * @throws Exception BAM!
     */
    @Override
    public Serializable checkpointInfo() throws Exception {
        return null;
    }
}
