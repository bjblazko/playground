package de.huepattl.playground.batch.simple;

import de.huepattl.playground.batch.Employee;
import de.huepattl.playground.batch.EmployeeGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.batch.api.BatchProperty;
import javax.batch.api.Batchlet;
import javax.batch.runtime.context.JobContext;
import javax.inject.Inject;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;

/**
 * There are two ways to implement batch logic: steps composed of modular logic in* form of reader, processor and
 * writer - or in single batchlets. A batchlet, for example, can be used for processing tasks that cannot be split into
 * iteration logic or for processes that you have no detailed access to. In this case, we use the batchlet within the
 * first step of the example job to generate the example data. In real life, you would for example use such batchlet to
 * retrieve data from an external system for further processing, such as fetching a remote data file via SCP or FTP.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-10-03
 */
public class ProvideDataBatchlet implements Batchlet {

    private final Logger LOG = LogManager.getLogger(ProvideDataBatchlet.class.getName());

    private static final long EMPLOYEES_TO_GENERATE = 503;

    /**
     * ...to allow aborting running process, see {@link #stop()}.
     */
    private boolean doContinue = true;

    /**
     * Provides full access to the job context.
     */
    @Inject
    JobContext jobContext;

    /**
     * This comes from the property specified in simple.job.xml for this very
     * batchlet by using the same name (field name here and property in XML) or
     * by specifying the name manually.
     */
    @Inject
    @BatchProperty(name = "provideFileName")
    private String fileName;

    /**
     * This invokes the batchlet execution, implement your main logic here.
     *
     * @return Flow control information, e.g. SUCCESS.
     * @throws Exception BAM!
     */
    @Override
    public String process() throws Exception {

        /*
         * Allocate and create file.
         */
        LOG.info("Processing file: " + fileName);
        Path path = FileSystems.getDefault().getPath(fileName);
        try (BufferedWriter writer = Files.newBufferedWriter(
                path, Charset.defaultCharset(), StandardOpenOption.CREATE)) {
            for (final Employee e : EmployeeGenerator.generateEmployees(EMPLOYEES_TO_GENERATE)) {
                if (doContinue) {
                    // Build CSV line
                    StringBuilder sb = new StringBuilder()
                            .append(e.getId()).append(",")
                            .append(e.getLastName()).append(",")
                            .append(e.getFirstName()).append(",")
                            .append(e.getBirthDate()).append(",")
                            .append(e.isExternal());

                    // Write to file, format: [ID],[last name],[first name],[birthday],[external]
                    writer.write(sb.append('\n').toString());
                }
            }
            writer.flush();
        }

        printWrittenFile();
        return "SUCCESS";
    }

    /**
     * If a job is signaled to stop and if this batchlet is currently
     * processing, you can use this method to stop {@link #process()} method,
     * e.g. in real life to cancel a running SCP fetch of a remote file. Not
     * supported here.
     *
     * @throws Exception BAM!
     * @see #process()
     */
    @Override
    public void stop() throws Exception {
        doContinue = false;
    }

    /**
     * Just log/print the contents of the generated data file.
     *
     * @throws IOException BAM!
     */
    private void printWrittenFile() throws IOException {
        Path path = FileSystems.getDefault().getPath(fileName);
        List<String> lines = Files.readAllLines(path, Charset.defaultCharset());

        if (LOG.isDebugEnabled()) {
            LOG.debug("File contents:");
            for (final String currentLine : lines) {
                LOG.debug(currentLine);
            }
        }

        LOG.info("Generated test file has " + lines.size() + " lines.");
    }
}
