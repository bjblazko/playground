package de.huepattl.playground.batch.simple;

import de.huepattl.playground.batch.Employee;

/**
 * The item processor is our man in the middle - it receives an item (here:
 * {@link Employee} from the reader, can modify or validate it and then pass it
 * to the item writer. Sorry kids, no generics:
 * https://java.net/projects/jbatch/lists/public/archive/2013-03/message/141
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @see CsvFileReader
 * @see XmlFileWriter
 * @since 2013-10-03
 */
public class ItemProcessor implements javax.batch.api.chunk.ItemProcessor {

    /**
     * Our use case is to rename all employees to bear 'Darth' as additional
     * first name. So Sarah becomes Darth Sarah...
     *
     * @param item Item as coming from the item reader, here {@link CsvFileReader}.
     *             Will be an instance of {@link de.huepattl.playground.batch.Employee}.
     * @return Modified item, same class in this example ({@link de.huepattl.playground.batch.Employee}).
     * @throws Exception BAM!
     */
    @Override
    public Object processItem(Object item) throws Exception {
        Employee e = (Employee) item;
        e.setFirstName("Darth " + e.getFirstName());
        return e;
    }
}
