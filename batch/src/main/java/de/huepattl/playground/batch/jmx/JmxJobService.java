package de.huepattl.playground.batch.jmx;

import de.huepattl.playground.batch.complex.ComplexJob;
import de.huepattl.playground.batch.simple.SimpleJob;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.batch.runtime.JobExecution;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

/**
 * Simple, example JMX service, see interface documentation for details on usage etc.
 * TODO: add operation and parameter descriptions and use JMX annotations in order
 * to make interface obsolete.
 *
 * TODO: Add notification via {@link javax.management.MBeanNotificationInfo}.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-11-22
 */
@Singleton
@Startup
public class JmxJobService implements JobServiceMBean {

    /**
     * Service for simple job.
     */
    @Inject
    private SimpleJob simpleJob;

    /**
     * Service for the more complex examples.
     */
    @Inject
    private ComplexJob complexJob;

    private MBeanServer platformMBeanServer;
    private ObjectName objectName = null;

    /**
     * Register this as an JMX MBean against the MBean Server of the container just
     * after instantiation of this class.
     */
    @PostConstruct
    public void registerBean() {
        try {
            objectName = new ObjectName("JobService:type=" + this.getClass().getName());
            platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
            platformMBeanServer.registerMBean(this, objectName);
        } catch (Exception e) {
            throw new IllegalStateException("Problem during registration of JMX bean:" + e);
        }
    }

    /**
     * Unregister this JMX MBean from the MBean Server of the container just
     * before garbage collection.
     */
    @PreDestroy
    public void unregisterBean() {
        try {
            platformMBeanServer.unregisterMBean(this.objectName);
        } catch (Exception e) {
            throw new IllegalStateException("Problem while unregistering from MBean Server:" + e);
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public long startSimple(final String folder) {
        if (folder == null) {
            return simpleJob.startJob("/tmp");
        } else {
            return simpleJob.startJob(folder);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long startComplex(final String jobName) {

        return complexJob.startJob("justReadAndWrite");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJobStatus(long id) {
        JobExecution jobExecution = complexJob.getJobExecutionDetails(id);
        return JmxJobService.jobExecutionToString(jobExecution);
    }

    /**
     * Just generates a string from the passed job execution for convenience. It can be used
     * for example for outputtung it in the JConsole.
     *
     * @param je {@link javax.batch.runtime.JobExecution} instance to return as string.
     * @return String representation, but do not try to parse it.
     */
    private static String jobExecutionToString(final JobExecution je) {
        StringBuilder sb = new StringBuilder();
        sb.append("Execution ID: ").append(je.getExecutionId()).append("\n");
        sb.append("Created at: ").append(je.getCreateTime()).append("\n");
        sb.append("Started at: ").append(je.getStartTime()).append("\n");
        sb.append("Finished at: ").append(je.getEndTime()).append("\n");
        sb.append("Batch status: ").append(je.getBatchStatus().name()).append("\n");
        sb.append("Exit status: ").append(je.getExitStatus()).append("\n");
        sb.append("Job parameters: ").append(je.getJobParameters()).append("\n");
        return sb.toString();
    }
}
