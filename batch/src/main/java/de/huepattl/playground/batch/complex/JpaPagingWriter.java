package de.huepattl.playground.batch.complex;

import de.huepattl.playground.batch.Employee;
import de.huepattl.playground.batch.EmployeeService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import javax.batch.api.BatchProperty;
import javax.batch.api.chunk.ItemWriter;
import javax.batch.runtime.Metric;
import javax.batch.runtime.context.StepContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Item writer using JPA. For demonstration purposes, this writer supports
 * artificial exception requests to be defined in the job XML using properties.
 * For example, you can define to throw a NPE for item with employee ID 42 and a
 * PersistenceException for item with ID 101: null {@code
 * <writer ref="jpaWriter">
 *    <properties>
 *       <property name="throwExceptions" value="java.lang.NullPointerException@83, javax.persistence.PersistenceException@101" />
 *    </properties>
 * </writer>
 * }
 * <p>
 * This allows for demonstrating exception handling such as skips and retries.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-11-07
 *
 * @see Processor
 * @see JpaPagingReader
 * @see EmployeeService
 */
@Named("jpaWriter")
public class JpaPagingWriter implements ItemWriter {

    private final Logger LOG = LogManager.getLogger(JpaPagingWriter.class.getName());

    @Inject
    @BatchProperty(name = "throwExceptions")
    private String throwExceptionNames;
    /**
     * Optional map defining what exception to be thrown at what (absolute)
     * item. For example 101=java.lang.NullPointerException causes an NPE to be
     * thrown during writing the 101st item.
     */
    private final Map<Long, Class<Exception>> exceptions = new HashMap<>();

    /**
     * Contains information on the current step within the batch, including
     * ID/name and summary information (commits, items read etc.).
     */
    @Inject
    StepContext stepContext;

    /**
     * Our JPA employee service.
     */
    @Inject
    EmployeeService employeeService;

    /**
     * This collection will contain items that caused a processing exception. It
     * is stored into {@link StepContext#setTransientUserData(java.lang.Object)}
     * in order to be available in a listener for further treatment (e.g.
     * logging or emailing it etc.).
     */
    private final List<Employee> failedItems = new ArrayList<>();

    /**
     * Called by the batch infrastructure before writing, may be used for
     * initialisation.
     *
     * @param checkpoint Custom checkpoint descriptor, not used here.
     * @throws Exception BAM!
     */
    @Override
    public void open(Serializable checkpoint) throws Exception {
        LOG.info("Writer opened.");

        /*
         Will be available to other actors (e.g. listeners) within this step.
         */
        stepContext.setTransientUserData(failedItems);

        /*
         If wanted, create some exception definitions to throw randomly
         (emulating real ones and to test exception handling by the job).
         */
        if (throwExceptionNames != null) {
            if (throwExceptionNames.contains(",")) {
                /*
                 String contains comma-separated exception names:
                 java.lang.NullPointerException@42, javax.persistence.PersistenceExceptions@101
                 */
                for (final String current : throwExceptionNames.split(",")) {
                    addExceptionRequest(current);
                }
            } else {
                /*
                 String only contains one exception:
                 javax.persistence.PersistenceExceptions@101
                 */
                addExceptionRequest(throwExceptionNames);
            }
            LOG.warn(
                    "Will throw exception during run for demonstrating exception handling:");
            //for (Class<Exception> ex : exceptions) {
            //    LOG.warn("   -  " + ex.getCanonicalName());
            //}
            for (long itemIndex : exceptions.keySet()) {
                LOG.warn(
                        "   - " + exceptions.get(itemIndex) + " for item with ID "
                        + itemIndex);
            }
        }
    }

    /**
     * Adds a definition of what exception to be thrown at what item index to the
     * definition collection ({@link #exceptions}) based on a string in format
     * [exception@itemIndex].
     *
     * @param exceptionAtItem Item index/exception pair, for example:
     *                        java.lang.NullPointerException@101
     * @throws ClassNotFoundException
     */
    @SuppressWarnings("unchecked")
    private void addExceptionRequest(final String exceptionAtItem) throws
            ClassNotFoundException {
        if (exceptionAtItem == null || !exceptionAtItem.contains("@")) {
            throw new IllegalArgumentException(
                    "If exception emulation is requested, definitions must be in format: "
                    + "[exception@itemIndex], e.g.: java.lang.NullPointerException@101");
        }

        String[] pair = exceptionAtItem.trim().split("@");
        Class<Exception> ex;
        ex = (Class<Exception>) Class.forName(pair[0]);
        long atItem = Integer.parseInt(pair[1]);
        exceptions.put(atItem, ex);
    }

    /**
     * Write the accumulated {@link Employee} items. They come from the item
     * processor {@link Processor#processItem(java.lang.Object)} and have been
     * accumulated to a list until the chunk-size has been reached (configured
     * in justReadAndWrite.job.xml, item-count in chunk).
     *
     * @param items Bunch of {@link Employee} items coming from the item
     *              processor.
     * @throws Exception BAM!
     */
    @Override
    public void writeItems(List<Object> items) throws Exception {
        LOG.info(
                "Writing (next) " + items.size() + " items, employee ID from "
                + ((Employee) items.get(0)).getId() + " to "
                + ((Employee) items.get(items.size() - 1)).getId());

        for (Object current : items) {
            /**
             * Throw exception for testing purposes?
             */
            if (!exceptions.isEmpty()) {
                Employee emp = (Employee) current;
                Class<Exception> ex = exceptions.get(emp.getId());
                if (ex != null) {
                    LOG.warn("We reached item with employee ID " + emp.getId()
                            + ", throwing emulated exception as requested: "
                            + ex.getCanonicalName());
                    LOG.warn("Affected item: " + current);

                    emp.setRemark("This item caused an exception!");
                    failedItems.add(emp);
                    throw ex.getConstructor(String.class).newInstance(
                            "Emulated exception thrown by request (configured in job XML!.)");
                }
            }
        }

        /*
         Remember! This is JPA. Since we are still in the transaction as opened
         in the reader, our entities are still attached to the persistence context.
         There is no need to call for persistence explicitly ;)
         However, before the transaction is finished, we ensure writing by calling
         EntityManager.flush() via our service.
         */
        employeeService.flush();

        // Debugging? OK, log our each written employee...
        if (LOG.isDebugEnabled()) {
            for (Object current : items) {
                Employee e = (Employee) current;
                LOG.debug("WRITE: " + e);
            }
        }
    }

    /**
     * Logs summary as retrieved from the step metrics. Contains for example
     * COMMIT_COUNT, READ_SKIP_COUNT, ROLLBACK_COUNT, WRITE_COUNT, FILTER_COUNT,
     * WRITE_SKIP_COUNT, READ_COUNT and PROCESS_SKIP_COUNT.
     *
     * @throws Exception BAM!
     */
    @Override
    public void close() throws Exception {
        LOG.info("Writer closed, step summary:");
        for (Metric m : stepContext.getMetrics()) {
            LOG.info("   - " + m.getType().name() + ": " + m.getValue());
        }
    }

    @Override
    public Serializable checkpointInfo() throws Exception {
        return null;
    }

}
