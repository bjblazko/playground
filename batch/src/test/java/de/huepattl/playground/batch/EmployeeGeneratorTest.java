package de.huepattl.playground.batch;

import org.junit.Test;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Test for our employee test data generator.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2013-10-12
 * @see EmployeeGenerator
 */
public class EmployeeGeneratorTest {
    
    private final Logger LOG = LogManager.getLogger(EmployeeGeneratorTest.class.getName());

    /**
     * Generate 100 employees and just check if the resulting amount is fine.
     * Prints the generated employees as well.
     */
    @Test
    public void testGenerator() {
        final long NUMBER = 100;
        LOG.info("Generating " + NUMBER + " test employees.");
        
        List<Employee> employees = EmployeeGenerator.generateEmployees(NUMBER);
        assertNotNull(employees);
        assertEquals(NUMBER, employees.size());
        
        for (final Employee e : employees) {
            LOG.info("-> Generated: " + e.toString());
        }
        LOG.info("Done.");
    }
}
