package de.huepattl.playground.batch.simple;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;

import javax.batch.runtime.BatchStatus;
import javax.batch.runtime.JobExecution;
import javax.inject.Inject;

/**
 * This demonstrates a quite simple batch job implemented with JSR-352. This
 * simple use case shows a job in two steps:
 * <ul>
 * <li>Batchlet: most basic and simple step providing (generating) a test file
 * to be used for further processing in step 2.</li>
 * <li>Item reader/processor/writer: Step 2 consumes the test file provded by
 * the batchlet in step 1:
 * <ul>
 * <li>ItemReader: Reads employee records from provided CSV file.</li>
 * <li>ItemProcessor: Does some stuff with the employee record (in real life
 * e.g. you could apply validation here).</li>
 * <li>ItemWriter: A JAXB writer implementation writes each employee record as
 * XML into the new destination file.</li>
 * </ul>
 * </li>
 * </ul>
 * The job itself is handled via a simple service ({@link SimpleJob})
 * and defined in an XML file contained in META-INF/batch-jobs/simple.job.xml
 *
 * @author blazko
 * @since 2013-10-03
 */
@RunWith(Arquillian.class)
public class SimpleBatchJobTest {

    /**
     * Takes care of allocating a temporary folder and cleaning it up after test execution.
     * If you need to have a look at the data in this location, you need to§ set a breakpoint
     * and run as debug.
     */
    @Rule
    public TemporaryFolder testDataFolder = new TemporaryFolder();

    /**
     * Our simple job service that we are testing.
     */
    @Inject
    SimpleJob exec;

    /**
     * Bundle our test artifacts for the container, we use Arquillian here.
     *
     * @return JAR that Arquillian will deploy for testing into the JEE
     *         container configured.
     */
    @Deployment
    public static JavaArchive createJarToTest() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(SimpleJob.class)
                .addClass(ProvideDataBatchlet.class)
                .addClass(CsvFileReader.class)
                .addClass(ItemProcessor.class)
                .addClass(XmlFileWriter.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsManifestResource("META-INF/batch-jobs/simple.job.xml", "batch-jobs/simple.job.xml");
    }

    /**
     * Simple test: generates test data in CSV file in step 1. In step 2 a
     * reader parses the records, a processor applies some changes to the
     * records and a writer appends to a new XML file.
     *
     * @throws Exception BAM!
     */
    @Test
    public void simpleJob() throws Exception {
        StringBuilder jobInfo = new StringBuilder();

        final long jobExecutionId = exec.startJob(testDataFolder.getRoot().getAbsolutePath());
        System.out.println("Job Exec ID: " + jobExecutionId);
        JobExecution jx = exec.getJobExecutionDetails(jobExecutionId);

        // Job is executed asynchronously, se we have to wait for the result.
        while (jx.getBatchStatus() != BatchStatus.COMPLETED
                && jx.getBatchStatus() != BatchStatus.FAILED
                && jx.getBatchStatus() != BatchStatus.ABANDONED
                && jx.getBatchStatus() != BatchStatus.STOPPED) {
            Thread.sleep(500);
        }

        jobInfo.append("ID: ").append(jx.getExecutionId()).append('\n');
        jobInfo.append("Name: ").append(jx.getJobName()).append('\n');
        jobInfo.append("Parameters: ").append(jx.getJobParameters()).append('\n');
        jobInfo.append("Status: ").append(jx.getBatchStatus()).append('\n');
        jobInfo.append("Created: ").append(jx.getCreateTime()).append('\n');
        jobInfo.append("Ended: ").append(jx.getEndTime()).append('\n');
        jobInfo.append("Exit status: ").append(jx.getExitStatus()).append('\n');

        System.out.println(jobInfo);
    }
}
