package de.huepattl.playground.batch.complex;

import javax.batch.runtime.BatchStatus;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.batch.runtime.JobExecution;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static org.junit.Assert.*;

/**
 * Demonstrates a more complex job.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @see ComplexJob
 * @since 2013-11-06
 */
@RunWith(Arquillian.class)
public class ComplexBatchJobTest {

    private final Logger LOG = LogManager.getLogger(
            ComplexBatchJobTest.class.getName());

    /**
     * Our more complex job service that we are testing.
     */
    @Inject
    ComplexJob exec;

    /**
     * Bundle our test artifacts for the container, we use Arquillian here.
     *
     * @return JAR that Arquillian will deploy for testing into the JEE
     *         container configured.
     */
    @Deployment
    public static JavaArchive createJarToTest() {
        return ShrinkWrap.create(JavaArchive.class)
                .addPackage("de.huepattl.playground.batch")
                .addPackage("de.huepattl.playground.batch.complex")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsManifestResource("persistence-test.xml", "persistence.xml")
                .addAsManifestResource(
                        "META-INF/batch-jobs/justReadAndWrite.job.xml",
                        "batch-jobs/justReadAndWrite.job.xml")
                .addAsManifestResource("META-INF/batch-jobs/skipItems.job.xml",
                        "batch-jobs/skipItems.job.xml")
                .addAsManifestResource("META-INF/batch-jobs/retryItems.job.xml",
                        "batch-jobs/retryItems.job.xml")
                .addAsManifestResource(
                        "META-INF/batch-jobs/parallelSplit.job.xml",
                        "batch-jobs/parallelSplit.job.xml");
    }

    /**
     * Execute job as defined in justReadAndWrite.job.xml file.
     *
     * @throws Exception BAM!
     */
    private BatchStatus executeJob(final String name) throws Exception {

        /*
         Sleep to avoid "Caught exception executing step: java.lang.RuntimeException:
         java.lang.IllegalStateException: This web container has not yet been started"
         */
        final long jobExecutionId
                = exec.startJob(name);
        Thread.sleep(500);

        /*
         * Since the batch is executed in another thread, we have to wait for a job
         * result (failure or success) and sleep for that time. If we do not wait,
         * our JEE test container will be shut down during job execution...
         */
        JobExecution jx = exec.getJobExecutionDetails(jobExecutionId);
        while (jx.getBatchStatus() != BatchStatus.COMPLETED
                && jx.getBatchStatus() != BatchStatus.FAILED
                && jx.getBatchStatus() != BatchStatus.ABANDONED
                && jx.getBatchStatus() != BatchStatus.STOPPED) {
            Thread.sleep(500);
        }

        /*
         Job finished, print job information.
         */
        LOG.info("Job " + jx.getExecutionId() + " finished:");
        LOG.info("   - Name: " + jx.getJobName());
        LOG.info("   - Parameters: " + jx.getJobParameters());
        LOG.info("   - Created: " + jx.getCreateTime());
        LOG.info("   - Ended: " + jx.getEndTime());
        LOG.info("   - Status: " + jx.getBatchStatus());
        LOG.info("   - Exit status: " + jx.getExitStatus());

        return jx.getBatchStatus();
    }

    /**
     * Simple job run with no special handling.
     *
     * @throws Exception BAM!
     */
    @Test
    public void testJustReadAndWrite() throws Exception {
        final BatchStatus batchStatus = this.executeJob("justReadAndWrite"); // justReadAndWrite.job.xml
        assertEquals(BatchStatus.COMPLETED, batchStatus);
    }

    /**
     * This test shows how to skip items that case exceptions. They do not cause
     * the job to fail and we use listeners to react on such errors.
     * <p>
     * Uses skipItems.job.xml
     *
     * @throws Exception BAM!
     */
    @Test
    public void testSkipItemsDueException() throws Exception {
        final BatchStatus batchStatus = this.executeJob("skipItems");
        assertEquals(BatchStatus.COMPLETED, batchStatus);
    }

    /**
     * Demonstrate the retry-items mechanism when items fail.
     *
     * @throws Exception BAM!
     */
    @Test
    public void testRetryItemsDueException() throws Exception {
        final BatchStatus batchStatus = this.executeJob("retryItems");
        assertEquals(BatchStatus.COMPLETED, batchStatus);
    }

    /**
     * Parallel execution of related flows, no partitioning used here.
     *
     * @throws Exception BAM!
     */
    @Test
    public void testParallelSplit() throws Exception {
        final BatchStatus batchStatus = this.executeJob("parallelSplit");
        assertEquals(BatchStatus.COMPLETED, batchStatus);
    }

    /**
     * Parallel execution using partitioning of data.
     *
     * @throws Exception BAM!
     */
    @Test
    public void testParallelPartitions() throws Exception {
        final BatchStatus batchStatus = this.executeJob("parallelPartitions");
        assertEquals(BatchStatus.COMPLETED, batchStatus);
    }

    /**
     * Show how a decider works.
     *
     * @throws Exception BAM!
     */
    @Test
    public void testDecisions() throws Exception {
        final BatchStatus batchStatus = this.executeJob("decisions");
        assertEquals(BatchStatus.COMPLETED, batchStatus);
    }

}
