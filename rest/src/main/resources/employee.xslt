<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:template match="/">
        <html>
            <body>
                <h1>Employees</h1>
                <xsl:apply-templates select="employee" />
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="employee">
        <hr />
        ID:
        Name:
        Date of birth: <xsl:value-of select="date-of-birth" />
    </xsl:template> 
</xsl:stylesheet>