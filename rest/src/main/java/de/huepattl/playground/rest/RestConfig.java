package de.huepattl.playground.rest;

import javax.ws.rs.ApplicationPath;

/**
 * This is just a marker class enabling the WAR to also play REST using JAX-RS.
 * By annotating it with {@link ApplicationPath}, we specify the REST basis path
 * underneath the context root. If for example the WAR context root is
 * rest, the REST basis URL might be
 * <a href="http://localhost:8080/rest/resources"></a>, because we specified the
 * REST application path to be /rest.<p>
 * The REST services (or better: resources) declare themselves using
 * {@link Path}, see for example {@link SimpleRsource}.<p>
 * For playing around, you might use a web browser or even better REST client apps (or
 * REST browser plugins) or tools like wget.
 *
 * @author Timo Boewing (bjblazko@gmail.com)
 * @since 2014-07-10
 * @see BasicResource
 */
@ApplicationPath("/resources")
public class RestConfig extends javax.ws.rs.core.Application {

}
