package de.huepattl.playground.rest;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class is registered using the {@link Provider} annotation and by
 * extending the {@link ExceptionMapper} interface, it is registered to override
 * standard handling of the according exception type.<p>
 * In this example, we catch all {@link Throwable}s, but in real life you might
 * want to limit to several mappers for dedicated exceptions.<p>
 * So, what does it do? If there is an exception thrown, the REST implementation
 * would normally render a standard response (e.g. HTTP 500 with an HTML body)
 * which would not fit your needs. By implementing an own exception mapper, you
 * can define rendering a custom response, for example to return HTTP 406 (NOT
 * ACCEPTABLE) and having a JSON representation of the error details in the
 * response body.
 *
 * @author blazko
 * @since 2014-07-13
 */
@Provider
@ApplicationScoped
@Produces(MediaType.TEXT_PLAIN)
public class GeneralExceptionMapper implements javax.ws.rs.ext.ExceptionMapper<Throwable> {

    private static final Logger LOG = LogManager.getLogger(GeneralExceptionMapper.class);

    /**
     * Create a custom response. In this example, we define to return HTTP 406
     * (NOT ACCEPTABLE) and add the exception message as plain string to the
     * response body.<p>
     * Using {@code entity} below, you could also choose to define a JAXB
     * annotated class containing more error details which then will be returned
     * as XML or JASON object depending on the content-type.
     *
     * @param exception Exception thrown. We will return
     *                  {@link Throwable#getMessage()} as plain string in the
     *                  HTTP response body.
     *
     * @return
     */
    @Override
    public Response toResponse(Throwable exception) {
        LOG.error("Caught exception!", exception);

        /*
         * Make all exceptions HTTP 406 (NOT ACCEPTABLE) except for 404.
         */
        if (!(exception instanceof NotFoundException)) {
            return Response.status(Response.Status.NOT_ACCEPTABLE)
                    .entity(exception.getMessage()).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(exception.getMessage()).build();
        }
    }

}
