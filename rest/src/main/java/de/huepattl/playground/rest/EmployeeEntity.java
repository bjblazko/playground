package de.huepattl.playground.rest;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Simple entity that we are using for REST with JAX-RS and JSON or XML. Do not
 * leet you fool by the XML annotations - they are for JSON as well.
 *
 * @author blazko
 * @since 2014-07-15
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EmployeeEntity {

    private long id;
    private String name;
    private Date dateOfBirth;

    public EmployeeEntity() {
    }

    public EmployeeEntity(final long id, final String name, final Date dateOfBirth) {
        this.id = id;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

}
