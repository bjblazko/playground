package de.huepattl.playground.rest;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.CompletionCallback;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ExceptionMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This REST resource demonstrates synchronous (blocking) and asynchronous
 * requests - both on the server and/or on the client (see our JUnit test for
 * details).
 *
 * @author blazko
 * @since 2014-07-16
 */
@Path("async")
@Stateless
@Produces(MediaType.TEXT_PLAIN)
public class AsyncExamplesResource {

    private static final Logger LOG = LogManager.getLogger(AsyncExamplesResource.class);

    /**
     * Define artificial operation duration: 5 seconds.
     */
    private static final long WAIT_FOR = 5000;

    /**
     * This method is a common blocking execution, taking {@link #WAIT_FOR}
     * seconds. So when a client requests this operation, it takes that time
     * until it returns with our echo string.
     *
     * @param echo Literal to be included in the response.
     *
     * @return String response including our echo parameter to prove it
     *         understood our request.
     * @throws InterruptedException
     */
    @GET
    @Path("blocking-call")
    public String getSynchronous(@QueryParam("echo") final String echo) throws InterruptedException {
        LOG.info("Received request - will wait for " + WAIT_FOR + " msec...");
        Thread.sleep(WAIT_FOR);
        LOG.info("...done, sending response.");
        return "Synchronous (blocking) response: " + echo;
    }

    /**
     * This is an asynchronous (non-blocking) call which will fork into an own
     * thread (in JEE you should use a managed thread!)
     * .<p>
     * How the whole operation is handled, depends on the client:
     * <p>
     * <ul>
     * <li>If the client does a standard call, the server performs the operation
     * in a different thread while the client sees no difference and receives
     * the result when our spawned thread has finished.</li>
     * <li>If the client itself is asynchronously designed, its {@link Future}
     * will receive the operation result.</li>
     * </ul><p>
     * This example also shows how to deal with timeouts. You can define a
     * timeout and an handler in which you can do dedicated stuff if the
     * operation takes too long. In this case, JAX-RS throws HTTP error 503
     * (SERVICE UNAVAILABLE), which you could override with a dedicated
     * {@link ExceptionMapper}
     * .<p>
     * The whole magic utilises the help of {@link AsyncResponse}.
     *
     * @param asyncResponse Nothing coming from the client - provided by JAX-RS
     *                      runtime.
     * @param echo          Literal to be included in the response.
     * @param throwTimeout  When true, this operation simulates a timeout.
     *
     * @throws Exception
     */
    @GET
    @Path("non-blocking-call")
    public void getAsynchronous(@Suspended final AsyncResponse asyncResponse,
            @QueryParam("echo") final String echo,
            @QueryParam("cause-timeout") final boolean throwTimeout) throws Exception {

        /*
         * Optional: Register a callback handler.
         */
        asyncResponse.register((CompletionCallback) (Throwable throwable) -> {
            LOG.info("Completion callback invoked!");
        });

        /*
         * Optional: Register a timeout + handler.
         */
        if (throwTimeout) {
            // Causes timeout, cos our operation takes 5 seconds.
            LOG.info("I will cause a timeout!");
            asyncResponse.setTimeout(1, TimeUnit.SECONDS);
        } else {
            // No timeout.
            asyncResponse.setTimeout(30, TimeUnit.SECONDS);
        }
        asyncResponse.setTimeoutHandler((AsyncResponse asyncResponse1) -> {
            LOG.error("Aaaaaargh! It took too long!");
            asyncResponse.cancel();
        });

        /*
         * Do the grunt work. Looooooong taking operation here...
         */
        new Thread(() -> {
            try {
                Thread.sleep(WAIT_FOR);
                final String result = "Done: " + echo;
                /*
                 * 'result' can be plain type, JAXB entity or JAX-RS Response
                 */
                asyncResponse.resume(result);
            } catch (InterruptedException ex) {
                LOG.error("Ooops!", ex);
            }
        }).start();
    }
}
