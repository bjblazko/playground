package de.huepattl.playground.rest;

import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author blazko
 * @since 2014-07-13
 */
@Provider
public class LogFilter implements ContainerRequestFilter, ContainerResponseFilter {

    private static final Logger LOG = LogManager.getLogger(LogFilter.class);

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        // Nothing here, done in next method.
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        LOG.info(">>--> Handle  HTTP/REST request:\n   - Got: "
                + requestContext.getUriInfo().getRequestUri()
                + "\n   - Response: " + responseContext.getEntity());
    }

}
