package de.huepattl.playground.rest;

import java.math.BigDecimal;
import java.time.Instant;
import javax.ejb.Stateless;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * This class represents a REST resource. HTTP REST requests are directly mapped
 * to this class. The methods handling the request are chosen according their
 * annotations based on HTTP method (e.g. GET, POST, DETELE etc.) and content
 * types ({@link Consumes} and {@link Produces} for e.g. TEXT/PLAIN,
 * APPLICATION/XML or APPLICATION_JSON and so on)
 * .<p>
 * In this example, we will only demonstrate the very basic stuff. Handling XML,
 * JSON, asynchronous requests etc. is showed in other resources.<p>
 * The {@link Path} annotations used in the controller (plus the base path as
 * defined in {@link Application} form the URL the resource reacts on. Depending
 * on your cases, annotating the class methods with an additional {@link Path}
 * can be used to further structurise your code.
 * <p>
 * We also use Java Bean Validation annotations here to restrict our input data.
 * For example, the {@code say} parameter of method
 * {@link #echo(java.lang.String, java.lang.String)} may not be NULL and
 * requires at least three characters in length, enforced by {@link NotNull} and
 * {@link Size} annotations. This would throw
 * {@link ConstraintViolationException} that can then be handled in an
 * {@link ExceptionMapper} class that is responsible for creating an according
 * custom HTTP response.<p>
 * Please note that most annotations can be used on class-level and only in
 * exceptional cases you can anootate the method. For example, if all methods
 * consume or produce the same content-type, you can place the {@link Produces}
 * on class level. same applies to {@link Path} etc.
 *
 * @author blazko
 * @since 2014-07-13
 */
@Path("basic")
@Stateless
@Produces(MediaType.TEXT_PLAIN) // Feel responsible for TEXT/PLAIN
public class BasicExamplesResource {

    /**
     * Allows programmatic use of HTTP headers, see {@link #clientInfo()}
     * example. In most cases, however, you would like to use
     * {@link HeaderParam} etc. annotations for that, see {@link #accessHeader(java.lang.String) example.
     */
    @Context
    HttpHeaders httpHeaders;

    /**
     * Simple method that takes one or two strings as parameters of a HTTP GET
     * request. This method feels responsible for requests of type
     * text/plain.<p>
     * Client request URL: <a
     * href="/resources/basic/echo?say=hello&also=world">/resources/basic/echo?say=hello&also=world</a>
     *
     * @param say     First, mandatory string to echo. Additionally, it must be
     *                at least three characters long.
     * @param sayAlso Second, optional string to echo.
     *
     * @return Both parameters returned back as echo. If the first parameter is
     *         NULL or less than three characters long, HTTP 400 (BAD REQUEST)
     *         will be returned.
     * @see #echoHtml(java.lang.String, java.lang.String)
     */
    @GET // Feel responsible for HTTP GET requests
    @Path("echo") // react on /echo path (within parent path)
    public String echo(@NotNull @Size(min = 3) @QueryParam("say") final String say,
            @QueryParam("also") final String sayAlso) {
        return "Echo: " + say + " - " + sayAlso;
    }

    /**
     * Same as {@link #echo(java.lang.String, java.lang.String)}, but reacting
     * on HTML content type request based on the client. URL and params are
     * identical, so the runtime decides which methods to choose from based on
     * the annotations.<p>
     * Client request URL: <a
     * href="/resources/basic/echo?say=hello&also=world">/resources/basic/echo?say=hello&also=world</a>
     *
     * @param say  First echo.
     * @param also Second echo.
     *
     * @return HTML String. In real life, we would probably choose different
     *         stuff here, e.g. JAXB with XML to HTML transformation.
     * @see #echo(java.lang.String, java.lang.String)
     */
    @GET
    @Path("echo")
    @Produces(MediaType.TEXT_HTML) // Exception from standard: feel responsible for TEXT/HTML
    public String echoHtml(@NotNull @Size(min = 3) @QueryParam("say") final String say, @QueryParam("also") final String also) {
        return "<html><body><h1>HTML response</h1><p>" + say + " - " + also + "</p></body></html>";
    }

    /**
     * Let us see how we can handle different data types. This method takes
     * several ones:
     *
     * @param string     Special characters should be escaped/unescaped
     *                   correctly (test with ampersand etc.).
     * @param integer    Plain numbers.
     * @param bigDecimal Big decimal - I would recomment to transfer these as
     *                   string however and normalise to a common locale (e.g.
     *                   English) in order to avoid confusion between decimal
     *                   point and thousand separator.
     * @param bool       Boolean - I would recommend to use the string
     *                   (true/false) externally instead oz 0/1 etc. because
     *                   most converters ({@link String#valueOf(boolean)})
     *                   handle this better.
     * @param date       Using Java date types does not work. For REST, I
     *                   recommend using a String technically backed by Java 8
     *                   {@link Instant} time using ISO 8601 formatting
     *                   including timezone, e.g. 2014-07-15T18:18:57.632Z
     *
     * @return All parameters with a type prefix, delimited by comma.
     */
    @GET
    @Path("data-types")
    public String echoDataTypes(@QueryParam("string") final String string,
            @QueryParam("int") final int integer, @QueryParam("big-decimal") final BigDecimal bigDecimal,
            @QueryParam("boolean") final boolean bool, @QueryParam("date") final String date) {
        Instant instant = Instant.parse(date);
        return "String: " + string + ", Int: " + integer + ", Big decimal: " + bigDecimal
                + ", Boolean: " + bool + ", Date: " + instant.toString();
    }

    /**
     * Shows how to use HTTP POST. Inseat of query parameters (linke in
     * {@link #echo(java.lang.String, java.lang.String)}), we extract them from
     * the post body.<p>
     * Client request URL: <a
     * href="/resources/basic/echo">/resources/basic/echo</a>
     *
     * @param echo String chosen from the POST body.
     *
     * @return Echo string.
     */
    @POST // Feel responsible for HTTP POST
    @Path("echo")
    public String echoPost(@NotNull @FormParam("say") final String echo) {
        return "Echo: " + echo;
    }

    /**
     * Though I will not encourage to use Cookies for REST interfaces, you can
     * query cookies if you have to. This is done using the {@link CookieParam}
     * annotations.<p>
     * Client request URL: <a
     * href="/resources/basic/cookie">/resources/basic/cookie</a>
     *
     * @param userAgent Extracted from the cookie.
     *
     * @return Extracted value.
     * @see HttpHeaders
     * @see #clientInfo()
     */
    @GET
    @Path("cookie")
    public String accessCookie(@CookieParam("User-agent") final String userAgent) {
        return "User agent from cookie: " + userAgent;
    }

    /**
     * If you have to manually extract values from the HTTP header.<p>
     * Client request URL: <a
     * href="/resources/basic/header">/resources/basic/header</a>
     *
     * @param userAgent Extracted from the HTTP headers.
     *
     * @return Extracted value.
     * @see HttpHeaders
     * @see #clientInfo()
     */
    @GET
    @Path("header")
    public String accessHeader(@HeaderParam("User-agent") final String userAgent) {
        return "User agent from HTTP header: " + userAgent;
    }

    /**
     * Besides using {@link CookieParam} and {@link HeaderParam} you also also
     * access this information in a programmatic way. For this, we inject
     * {@link HttpHeaders} from the JAX-RS context using the {@link Context}
     * annotation.<p>
     * Client request URL: <a
     * href="/resources/basic/info">/resources/basic/info</a>
     *
     * @return Some information extracted from the HTTP headers.
     */
    @GET
    @Path("info")
    public String clientInfo() {
        final String NEWLINE = "\n";
        final String INDENT = "\t\t";
        StringBuilder response = new StringBuilder("Request information" + NEWLINE + NEWLINE);

        response.append("Request date: ").append(httpHeaders.getDate()).append(NEWLINE);
        response.append("Accepted languages: ").append(httpHeaders.getAcceptableLanguages()).append(NEWLINE);
        response.append("Language used: ").append(httpHeaders.getLanguage()).append(NEWLINE);
        response.append("Accepted content types: ").append(httpHeaders.getAcceptableMediaTypes()).append(NEWLINE);
        response.append("Content type used: ").append(httpHeaders.getMediaType()).append(NEWLINE);
        response.append("Cookies: ").append(httpHeaders.getCookies()).append(NEWLINE);

        response.append("Other entries:").append(NEWLINE);
        httpHeaders.getRequestHeaders().entrySet().stream().map((entries) -> {
            response.append(INDENT).append(entries.getKey()).append(":");
            return entries;
        }).forEach((entries) -> {
            response.append(INDENT).append(INDENT).append(entries.getValue()).append(NEWLINE);
        });

        return response.toString();
    }

}
