package de.huepattl.playground.rest;

import java.net.URL;
import java.util.concurrent.Future;
import javax.ws.rs.ServiceUnavailableException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.InvocationCallback;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ExceptionMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * This test demonstrates synchronous (blocking) and asynchronous (non-blocking,
 * with call back) REST calls against {@link AsyncExamplesResource}.
 *
 * @author blazko
 * @since 2014-07-16
 * @see AsyncExamplesResource
 * @see LogFilter
 * @see AllExceptionMapper
 */
@RunWith(Arquillian.class)
@RunAsClient
public class AsyncExamplesTest {

    private static final Logger LOG = LogManager.getLogger(AsyncExamplesTest.class);

    private static final String PARAM_NAME_TIMEOUT = "cause-timeout";
    private static final String PARAM_NAME = "echo";
    private static final String PARAM_VALUE = "Echo echo silently...";

    /**
     * This gives us the base URL of the container launched by Arquillian, e.g.
     * http://localhost:8081/mytestcontext
     */
    @ArquillianResource
    URL deployUrl;

    /**
     * URL part appended to {@link #deployUrl}.
     */
    private static final String RESOURCE_PATH = "resources/async";

    /**
     * Deploy our test JAR/WAR to test container.
     *
     * @return Archive deployed into test container.
     */
    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class).addPackage("de.huepattl.playground.rest")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
        return jar;
    }

    /**
     * This test performs a blocking call to a blocking server-side resource.
     * This means that both, client and server method are just normal blocking
     * operations.<p>
     * <ul>
     * <li>Client: synchronous (blocking)</li>
     * <li>Server: synchronous (blocking)</li>
     * </ul>
     *
     * @throws java.lang.Exception
     */
    @Test
    public void syncSync() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget resource = client.target(deployUrl + RESOURCE_PATH);

        LOG.info("Client: Making normal, blocking call. The server-side method is blocking as well.");

        // e.g. http://localhost:8081/tes/resources/async/blocking-call?echo=Hello
        final String response = resource.path("blocking-call")
                .queryParam(PARAM_NAME, PARAM_VALUE)
                .request(MediaType.TEXT_PLAIN).get(String.class);

        LOG.info("Client: Done.");

        assertNotNull(response);
        assertTrue(response.contains(PARAM_VALUE));
    }

    /**
     * This test performs a blocking call to a non-blocking server-side
     * resource. This means that the client call will block but the server
     * method is forking to a new thread, which in turn will then render the
     * response.<p>
     * <ul>
     * <li>Client: synchronous (blocking)</li>
     * <li>Server: asynchronous (non-blocking)</li>
     * </ul>
     *
     * @throws java.lang.Exception
     */
    @Test
    public void syncAsync() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget resource = client.target(deployUrl + RESOURCE_PATH);

        LOG.info("Client: Making normal, blocking call. The server-side method is asynchronous.");

        // e.g. http://localhost:8081/tes/resources/async/non-blocking-call?echo=Hello
        final String response = resource.path("non-blocking-call")
                .queryParam(PARAM_NAME, PARAM_VALUE)
                .request(MediaType.TEXT_PLAIN).get(String.class);

        LOG.info("Client: Done.");
        assertNotNull(response);
        assertTrue(response.contains(PARAM_VALUE));

    }

    /**
     * This test performs a blocking call to a non-blocking server-side
     * resource, just like {@link #syncAsync()} - with the exception that it
     * will cause a timeout on the server (due to an artificial request
     * parameter...). This means that the client call will block but the server
     * method is forking to a new thread, which in turn will end with a timeout
     * before rendering the response.<p>
     * By definition, timeouts will return HTTP 503 (SERVICE UNAVAILABLE) - you
     * can use an {@link ExceptionMapper} to render a custom response.<p>
     * <ul>
     * <li>Client: synchronous (blocking)</li>
     * <li>Server: asynchronous (non-blocking) - causes timeout!</li>
     * </ul>
     *
     * @throws java.lang.Exception
     */
    @Test(expected = ServiceUnavailableException.class) // HTTP 503
    public void syncAsyncWithTimeout() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget resource = client.target(deployUrl + RESOURCE_PATH);

        LOG.info("Client: Making normal, blocking call. The server-side method is asynchronous - BUT WILL CAUSE A TIMEOUT!");

        // e.g. http://localhost:8081/tes/resources/async/non-blocking-call?echo=Hello&cause-timeout=true
        resource.path("non-blocking-call")
                .queryParam(PARAM_NAME, PARAM_VALUE)
                .queryParam(PARAM_NAME_TIMEOUT, true)
                .request(MediaType.TEXT_PLAIN).get(String.class);
    }

    /**
     * This demonstrates the REST client being asynchronously. It invoked a call
     * against a blocking server-side resource and receives the result using a
     * {@link Future}
     * .<p>
     * <ul>
     * <li>Client: asynchronous (non-blocking)</li>
     * <li>Server: synchronous (blocking)</li>
     * </ul>
     *
     * @throws Exception
     */
    @Test
    public void asyncSync() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget resource = client.target(deployUrl + RESOURCE_PATH);

        LOG.info("Client: Making asynchronous (non-blocking) call. The server-side method is synchronous (blocking).");

        // e.g. http://localhost:8081/tes/resources/async/blocking-call?echo=Hello
        final Future<String> operationResult = resource.path("blocking-call")
                .queryParam(PARAM_NAME, PARAM_VALUE)
                .request(MediaType.TEXT_PLAIN).
                async().get(new InvocationCallback<String>() {

                    @Override
                    public void completed(String response) {
                        LOG.info("Whoooho! Received a Future: " + response);
                    }

                    @Override
                    public void failed(Throwable throwable) {
                        LOG.error("Ooooops, not intended to happen!", throwable);
                    }
                });

        final String response = operationResult.get();
        LOG.info("Received: " + response);
        assertNotNull(response);
        assertTrue(response.contains(PARAM_VALUE));
    }

    // TODO: Chunked (https://jersey.java.net/documentation/latest/async.html)
}
