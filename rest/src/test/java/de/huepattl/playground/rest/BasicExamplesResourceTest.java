package de.huepattl.playground.rest;

import java.math.BigDecimal;
import java.net.URL;
import java.time.Instant;
import javax.validation.constraints.NotNull;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

/**
 * Here we test our RESTful service {@link BasicResource} where we do simple
 * REST stuff with basic data types, testing the severla HTTP verbs (such as
 * GET, POST etc.) and accessing URL parameters and HTTP headers as well
 * excepion wrapping.<p>
 * Handling complex data with JSON or XML and doing asycnhronous calls is
 * subject for other tests.
 * <p>
 * The tests are not logging because a special filter does it for us, see
 * {@link LogFilter} while an exception mapper is responsible for writing an
 * according response when exceptions occurr ({@link AllExceptionMapper)
 * .<p>
 * We use the JAX-RS standard client API - so we have code similar as you would
 * implement in your applications for accessing REST resources. Thus, we avoid
 * using other HTTP clients (or JAX-RS implementations) such as RESTEasy, Jersey
 * or Apache HTTP Client directly.
 *
 * @author blazko @since 2014-07-12 @see Application @see BasicResource @see
 * LogFilter @see AllExceptionMapper
 */
@RunWith(Arquillian.class)
@RunAsClient
public class BasicExamplesResourceTest {

    /**
     * This gives us the base URL of the container launched by Arquillian, e.g.
     * http://localhost:8081/mytestcontext
     */
    @ArquillianResource
    URL deployUrl;

    /**
     * URL part appended to {@link #deployUrl}.
     */
    private static final String RESOURCE_PATH = "resources/basic";

    private final static String PARAM_MANDATORY_NAME = "say";
    private final static String PARAM_MANDATORY_VALUE = "Hello";
    private final static String PARAM_OPTIONAL_NAME = "also";
    private final static String PARAM_OPTIONAL_VALUE = "222";

    private final static String STRING_VALUE = "I am a string & have severl specialities = right? See!";
    private final static boolean BOOLEAN_VALUE = false;
    private final static int INTEGER_VALUE = 99;
    private final static BigDecimal BIGDEC_VALUE = new BigDecimal("100.99");
    private final static Instant DATE_VALUE = Instant.now();

    /**
     * Deploy our test JAR/WAR to test container.
     *
     * @return Archive deployed into test container.
     */
    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class).addPackage("de.huepattl.playground.rest")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
        return jar;
    }

    /**
     * Valid test of GET with two parameters and content-type TEXT/PLAIN as well
     * as TEXT/HTML.<p>
     * Client request URL: <a
     * href="/resources/basic/echo?say=hello&also=world">/resources/basic/echo?say=hello&also=world</a>
     *
     * @throws Exception
     * @see BasicResource#echo(java.lang.String, java.lang.String)
     * @see #httpGetWithError406()
     */
    @Test
    public void httpGet() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget resource = client.target(deployUrl + RESOURCE_PATH);
        String response;

        // 1st request with TEXT/PLAIN
        // e.g. http://localhost:8081/tes/resources/basic/echo?say=Hello&also=222
        {
            response = resource.path("echo")
                    .queryParam(PARAM_MANDATORY_NAME, PARAM_MANDATORY_VALUE)
                    .queryParam(PARAM_OPTIONAL_NAME, PARAM_OPTIONAL_VALUE).request(MediaType.TEXT_PLAIN).get(String.class);

            assertNotNull(response);
            assertTrue(response.contains(PARAM_MANDATORY_VALUE));
            assertTrue(response.contains(PARAM_OPTIONAL_VALUE));
        }

        // 2nd request with TEXT/HTML
        // e.g. http://localhost:8081/tes/resources/basic/echo?say=Hello&also=222
        {
            response = resource.path("echo")
                    .queryParam(PARAM_MANDATORY_NAME, PARAM_MANDATORY_VALUE)
                    .queryParam(PARAM_OPTIONAL_NAME, PARAM_OPTIONAL_VALUE).request(MediaType.TEXT_HTML).get(String.class);

            assertNotNull(response);
            assertTrue(response.contains(PARAM_MANDATORY_VALUE));
            assertTrue(response.contains("<html"));
            assertTrue(response.contains(PARAM_OPTIONAL_VALUE));
        }
    }

    /**
     * Contrary to {@link #httpGet()}, here we pass a NULL value to our
     * mandatory GET parameter. Our REST service has annotated that value with
     * {@link NotNull}, which causes our {@link AllExceptionMapper} to return
     * HTTP 406 (BAD REQUEST).
     *
     * @throws Exception Expecting: {@link BadRequestException}
     * @see BasicResource#echo(java.lang.String, java.lang.String)
     * @see #httpGet()
     */
    @Test(expected = BadRequestException.class)
    public void httpGetWithError406() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget resource = client.target(deployUrl + RESOURCE_PATH);

        // e.g. http://localhost:8081/tes/resources/basic/echo?say=&also=222
        resource.path("echo")
                .queryParam(PARAM_MANDATORY_NAME, (Object) null)
                .queryParam(PARAM_OPTIONAL_NAME, PARAM_OPTIONAL_VALUE).request(MediaType.TEXT_PLAIN).get(String.class);
    }

    /**
     * Here we test several different data types. Please read the
     * recommendations in the documentation of
     * {@link BasicResource#echoDataTypes(java.lang.String, int, java.math.BigDecimal, boolean, java.lang.String)}.
     *
     * @throws Exception
     * @see BasicResource#echoDataTypes(java.lang.String, int,
     * java.math.BigDecimal, boolean, java.lang.String)
     */
    @Test
    public void httpGetDataTypes() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget resource = client.target(deployUrl + RESOURCE_PATH);

        // e.g. http://localhost:8081/tes/resources/basic/echo?say=Hello&also=222
        final String response = resource.path("data-types")
                .queryParam("string", STRING_VALUE)
                .queryParam("int", INTEGER_VALUE)
                .queryParam("big-decimal", BIGDEC_VALUE)
                .queryParam("boolean", BOOLEAN_VALUE)
                .queryParam("date", DATE_VALUE.toString()) //ISO-8601 with time zone
                .request(MediaType.TEXT_PLAIN).get(String.class);

        assertNotNull(response);
        assertTrue(response.contains(STRING_VALUE));
        assertTrue(response.contains(String.valueOf(INTEGER_VALUE)));
        assertTrue(response.contains(String.valueOf(BIGDEC_VALUE)));
        assertTrue(response.contains(String.valueOf(BOOLEAN_VALUE)));
        assertTrue(response.contains(DATE_VALUE.toString()));
    }

    /**
     * Do a HTTP POST. We will add our parameters to a form instead of using URL
     * parameters like done in the {@link #httpGet()} example.
     *
     * @throws Exception
     * @see BasicResource#echoPost(java.lang.String)
     */
    @Test
    public void httpPost() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget resource = client.target(deployUrl + RESOURCE_PATH);

        /*
         * Add parameters as request body (form).
         */
        Form form = new Form();
        form.param(PARAM_MANDATORY_NAME, PARAM_MANDATORY_VALUE);

        // e.g. http://localhost:8081/tes/resources/basic/echo
        final String response = resource.path("/echo").request(MediaType.TEXT_PLAIN)
                .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class);

        assertNotNull(response);
        assertTrue(response.contains(PARAM_MANDATORY_VALUE));
    }
}
