package de.huepattl.playground.rest;

import java.net.URL;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

/**
 * Extended REST examples using marshalling/unmarshalling with XML and JSON.
 * This approach utilises POJOs with help of JAXB annotations. See
 * {@link EntityExamplesResource} for details.<p>
 * Contrary to this example, in the real world you would normally not publish
 * the POJOs from the service as part of the public interface but use an XML
 * Schema (XSD) or publish the JSON interface and let the client
 * applicationimplement its own way - especially because it might not use Java
 * at all.
 *
 * @author blazko
 * @since 2014-07-12
 * @see Application
 * @see EmployeeEntity
 * @see EmployeeResource
 * @see BasicResourceTest
 * @see LogFilter
 * @see AllExceptionMapper
 */
@RunWith(Arquillian.class)
@RunAsClient
public class JsonExamplesResourceTest {

    private static final Logger LOG = LogManager.getLogger(JsonExamplesResourceTest.class);

    /**
     * This gives us the base URL of the container launched by Arquillian, e.g.
     * http://localhost:8081/mytestcontext
     */
    @ArquillianResource
    URL deployUrl;

    /**
     * URL part appended to {@link #deployUrl}.
     */
    private static final String RESOURCE_PATH = "resources/employee";

    private static final String PARAM_NAME_EMPLOYEE_NAME = "name";
    private static final String PARAM_NAME_DATE_OF_BIRTH = "date-of-birth";

    private static final String EMPLOYEE_NAME = "Darth Vader";
    private static final LocalDate EMPLOYEE_DATE_OF_BIRTH = LocalDate.of(1977, Month.DECEMBER, 9);

    /**
     * Build our test JAR/WAR.
     *
     * @return Archive deployed into test container.
     */
    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class).addPackage("de.huepattl.playground.rest")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
        return jar;
    }

    /**
     * Request a new employee using HTTP POST. As a result, we expect a JSON
     * string created by the server from a POJO/entity.
     *
     * @throws Exception
     * @see JsonExamplesResource#create(java.lang.String, java.lang.String)
     */
    @Test
    public void createAndReturnJsonString() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget resource = client.target(deployUrl + RESOURCE_PATH);

        /*
         * Add parameters as request body (form).
         */
        Form form = new Form();
        form.param(PARAM_NAME_EMPLOYEE_NAME, EMPLOYEE_NAME);
        form.param(PARAM_NAME_DATE_OF_BIRTH, EMPLOYEE_DATE_OF_BIRTH.format(DateTimeFormatter.ISO_LOCAL_DATE));

        // e.g. http://localhost:8081/tes/resources/employee/create?name=Darth%20Vader
        final String stringResponse = resource.path("create").request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class);

        LOG.info("Received new entity as JSON string: " + stringResponse);
        assertTrue(stringResponse.contains(EMPLOYEE_NAME));
    }

    /**
     * Request a new employee entity using HTTP POST. As a result, we directly
     * get a POJO instance and do not have to parse the JSON sent by ourselves.
     * Note that for making this work we have to share the same annotated
     * {@link EmployeeEntity} (e.g. via Maven) or the client has to implement a
     * custom version with JAXB annotations.
     *
     * @throws Exception
     * @see JsonExamplesResource#create(java.lang.String, java.lang.String)
     */
    @Test
    public void createAndReturnJsonEntity() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget resource = client.target(deployUrl + RESOURCE_PATH);

        /*
         * Add parameters as request body (form).
         */
        Form form = new Form();
        form.param(PARAM_NAME_EMPLOYEE_NAME, EMPLOYEE_NAME);
        form.param(PARAM_NAME_DATE_OF_BIRTH, EMPLOYEE_DATE_OF_BIRTH.toString());

        // e.g. http://localhost:8081/tes/resources/employee/create?name=Darth%20Vader
        final EmployeeEntity employeeResponse = resource.path("create").request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), EmployeeEntity.class);

        LOG.info("Received new entity as unmarshalled entity object: "
                + employeeResponse.toString());
    }

    /**
     * Request a new employee using HTTP POST. Here, we do not unmarshall to a
     * POJO entity or even string but use the JEE {@link JsonObject} that
     * provides a polymorphic container (i.e. a better hash map), so that for
     * example the server can sent the same entity in different versions etc.
     *
     * @throws Exception
     * @see JsonExamplesResource#create(java.lang.String, java.lang.String)
     */
    @Test
    public void createAndReturnJsonObject() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget resource = client.target(deployUrl + RESOURCE_PATH);

        /*
         * Add parameters as request body (form).
         */
        Form form = new Form();
        form.param(PARAM_NAME_EMPLOYEE_NAME, EMPLOYEE_NAME);
        form.param(PARAM_NAME_DATE_OF_BIRTH, EMPLOYEE_DATE_OF_BIRTH.toString());

        // e.g. http://localhost:8081/tes/resources/employee/create?name=Darth%20Vader
        final JsonObject jsonObject = resource.path("create").request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), JsonObject.class);

        LOG.info("Received new entity as unmarshalled JSON (javax.json.JsonObject) object: "
                + jsonObject.toString());
    }

    /**
     * This just performs a get, showing how to construct a {@link JsonObject}
     * manually. See server-side implementation:
     * {@link JsonExamplesResource#get(long)}.
     *
     * @throws Exception
     * @see JsonExamplesResource#get(long)
     */
    @Test
    public void getJsonObject() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget resource = client.target(deployUrl + RESOURCE_PATH);

        // e.g. http://localhost:8081/tes/resources/employee/get?id=1701
        final JsonObject jsonObject = resource.path("get").queryParam("id", 1701)
                .request(MediaType.APPLICATION_JSON).get(JsonObject.class);

        LOG.info("Received new entity as unmarshalled JSON (javax.json.JsonObject) object: "
                + jsonObject.toString());
    }
}
