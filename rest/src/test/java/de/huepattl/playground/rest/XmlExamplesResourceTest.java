package de.huepattl.playground.rest;

import java.net.URL;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

/**
 *
 * @author blazko
 * @since 2014-07-18
 * @see Application
 * @see EmployeeEntity
 * @see XmlExamplesResource
 * @see LogFilter
 * @see AllExceptionMapper
 */
@RunWith(Arquillian.class)
@RunAsClient
public class XmlExamplesResourceTest {

    private static final Logger LOG = LogManager.getLogger(XmlExamplesResourceTest.class);

    /**
     * This gives us the base URL of the container launched by Arquillian, e.g.
     * http://localhost:8081/mytestcontext
     */
    @ArquillianResource
    URL deployUrl;

    /**
     * URL part appended to {@link #deployUrl}.
     */
    private static final String RESOURCE_PATH = "resources/employee";

    private static final String PARAM_NAME_EMPLOYEE_NAME = "name";

    private static final String EMPLOYEE_NAME = "Spot";

    /**
     * Build our test JAR/WAR.
     *
     * @return Archive deployed into test container.
     */
    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class).addPackage("de.huepattl.playground.rest")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
        return jar;
    }

    /**
     * Request a new employee using HTTP POST. We construct a Java Bean (POJO)
     * on the server just like we always do. However, this POJO contains JAXB
     * annotations and the JAX-RS runtime will use these to marshall the entity
     * to XML and send it over the wire. The client then, having also a POJO
     * (may be the same) with annotations will then unmarshall the XML back to a
     * bean.
     *
     * @throws Exception
     * @see JsonExamplesResource#create(java.lang.String, java.lang.String)
     */
    @Test
    public void getJaxb() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget resource = client.target(deployUrl + RESOURCE_PATH);

        // e.g. http://localhost:8081/tes/resources/employee/getJaxb?id=1701
        final EmployeeEntity employee = resource.path("getJaxb").queryParam("id", 1701)
                .request(MediaType.APPLICATION_XML)
                .get(EmployeeEntity.class);

        LOG.info("Received entity via XML: " + employee);
        assertEquals(EMPLOYEE_NAME, employee.getName());
    }

    @Test
    public void getJaxbAndTransform() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget resource = client.target(deployUrl + RESOURCE_PATH);

        // e.g. http://localhost:8081/tes/resources/employee/getJaxb?id=1701
        final String employeeString = resource.path("getJaxbAndTransform").queryParam("id", 1701)
                .request(MediaType.APPLICATION_XML)
                .get(String.class);

        LOG.info("Received entity via XML: " + employeeString);
        //assertEquals(EMPLOYEE_NAME, employee.getName());
    }

}
